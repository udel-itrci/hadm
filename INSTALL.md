# Installing HADM

There are many parts to HADM.  This document attempts to outline the procedures taken to install and implement it on DARWIN.

## DNS

The cluster's dnsmasq service is leveraged to tie the HADM database and API to the management node that's running the service:

```
$ cat /opt/local/warewulf/etc/hosts/dnsmasq.d/10-cnames.conf
   :
#
# Hermes Allocation Data Manager database:
#
cname=hadm-db,r0mgmt-master
cname=hadm-db.localdomain.hpc.udel.edu,r0mgmt-master.localdomain.hpc.udel.edu

#
# Hermes Allocation Data Manager REST API:
#
cname=hadm-api,r0mgmt-standby
cname=hadm-api.localdomain.hpc.udel.edu,r0mgmt-standby.localdomain.hpc.udel.edu

```

We colocated the HADM API service on the same node as the master `slurmctld` and the HADM database on the node servicing the shared ZFS+NFS file system.

## PostgreSQL database

A ZFS volume was created to hold the PostgreSQL database:

```
$ zfs create -V 4G beagle/postgresql
$ ls -ld /dev/zvol/beagle/postgresql 
lrwxrwxrwx 1 root root 9 Jun 17 14:02 /dev/zvol/beagle/postgresql -> ../../zd0
$ ls -ld /dev/zd0 
brw-rw---- 1 root disk 230, 0 Jun 17 14:02 /dev/zd0
$ mkfs.xfs /dev/zd0
$ mkdir -p /var/lib/postregsql-containers
```

The volume can be mounted on whichever DARWIN management server is servicing the ZFS file system:

```
$ cp <repository>/systemd/var-lib-postgresql\\x2dcontainers.mount /etc/systemd/system
$ systemctl daemon-reload
$ systemctl start var-lib-postgresql\\x2dcontainers.mount
```

A few files from this repository are copied to the mountpoint and then installed into systemd:

```
$ cp <repository>/systemd/darwin-postgresql-container@.service \
    <repository>/systemd/var-lib-postgresql\x2dcontainers.mount \
    <repository>/src/sbin/darwin-postgresql-container.sh \
    /var/lib/postgresql-containers
$ chmod +x /var/lib/postgresql-containers/darwin-postgresql-container.sh
$ cp /var/lib/postgresql-containers/darwin-postgresql-container@.service /etc/systemd/system
$ systemctl daemon-reload
```

We called the database `allocations` and created a directory to hold its components:

```
$ mkdir /var/lib/postgresql-containers/allocations
$ mkdir /var/lib/postgresql-containers/allocations/{data,run,root}
$ cp <repository>/docs/postgresql-container.def /var/lib/postgresql-containers/allocations/image.def
```

The Singularity container was created from the defintion file:

```
$ vpkg_require singularity/default
$ singularity build /var/lib/postgresql-containers/allocations/image.{sif,def}
```

The container was then started:

```
$ systemctl enable darwin-postgresql-container@allocations.service
$ systemctl start darwin-postgresql-container@allocations.service
$ systemctl status -l darwin-postgresql-container@allocations.service
● darwin-postgresql-container@allocations.service - Containerized PostgreSQL Server - allocations
   Loaded: loaded (/etc/systemd/system/darwin-postgresql-container@.service; disabled; vendor preset: disabled)
   Active: active (exited) since Thu 2021-08-05 14:21:22 EDT; 1 weeks 0 days ago
 Main PID: 114514 (code=exited, status=0/SUCCESS)

Aug 05 14:21:22 r0mgmt-master.localdomain.hpc.udel.edu systemd[1]: Starting Containerized PostgreSQL Server - allocations...
Aug 05 14:21:22 r0mgmt-master.localdomain.hpc.udel.edu systemd[1]: Started Containerized PostgreSQL Server - allocations.
```

### Importing the database schema

The `/var/lib/postgresql-containers/allocations/root` directory is bind-mounted as root's home directory inside the container and the `client` app starts with that as the working directory.  This makes it easy to import the schema:

```
$ cp <repository>/docs/hadm-db_schema.pgsql /var/lib/postgresql-containers/allocations/root
$ singularity run --app client instance://postgresql-allocations
psql (13.2 (Debian 13.2-1.pgdg100+1))
Type "help" for help.

allocations=# \i hadm-db_schema.pgsql
   :
allocations=# \q
```

### Firewalling

The database runs on the default PostgreSQL port (5432).  Existing firewall rules on the management servers:

- Block all ports on campus-facing interfaces, allowing only specific ingress (e.g. HTTP, SSH)
- Block nothing on cluster-facing (private) interfaces

So by construction the database is already not accessible from outside the cluster.  We could lock access even further by limiting to specific IPs (either in firewalld on cluster-facing interfaces or in the `pg_hba.conf` file) but password authentication is also in-place, so we felt that was adequate.

## REST API

The REST API is implemented in Flash (and thus in Python).  An Anaconda virtualenv is used to hold the infrastructure:

```
$ cd /opt/shared/slurm/add-ons/hadm
$ mkdir sbin
$ cp <repository>/sbin/hadm-api-run sbin
$ chmod +x sbin/hadm-api-run
$ mkdir rest-server
$ cp <repository>/rest-server/* rest-server/
$ cd rest-server
$ vpkg_require anaconda/2020.11:python3
$ . venv-create.sh
  :
```

The `hadm-api-run` script looks at its own realpath to locate the installation prefix, then adds `rest-server` and `logs` to generate its working directory and log file directory paths.  It then steps into that working directory and starts the gunicorn+Flask service.  A systemd service is installed on both management nodes:

```
$ cp <repository>/systemd/hadm-api.service /etc/systemd/system
$ systemctl daemon-reload
```

The service runs on the standby node (same node that runs `slurmctld`):

```
$ systemctl start hadm-api.service
$ systemctl status hadm-api.service
$ systemctl status hadm-api.service
● hadm-api.service - Hermes Allocation Data Manager - REST API
   Loaded: loaded (/etc/systemd/system/hadm-api.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-08-12 17:14:08 EDT; 5s ago
 Main PID: 77040 (hadm-api-run)
   CGroup: /system.slice/hadm-api.service
           ├─77040 /bin/bash /opt/shared/slurm/add-ons/hermes-allocations-datamgr/sbin/hadm-api-run
           ├─77080 /opt/shared/slurm/add-ons/hermes-allocations-datamgr/1.0.0/rest-server/venv/bin/python /opt/shared...
           ├─77082 /opt/shared/slurm/add-ons/hermes-allocations-datamgr/1.0.0/rest-server/venv/bin/python /opt/shared...
           └─77084 /opt/shared/slurm/add-ons/hermes-allocations-datamgr/1.0.0/rest-server/venv/bin/python /opt/shared...

Aug 12 17:14:08 r0mgmt-standby.localdomain.hpc.udel.edu systemd[1]: Started Hermes Allocation Data Manager - REST API.
Aug 12 17:14:08 r0mgmt-standby.localdomain.hpc.udel.edu hadm-api-run[77040]: Ensuring log directory /opt/shared/slu...ts
Aug 12 17:14:08 r0mgmt-standby.localdomain.hpc.udel.edu hadm-api-run[77040]: Loading Anaconda base environment
Aug 12 17:14:08 r0mgmt-standby.localdomain.hpc.udel.edu hadm-api-run[77040]: Activating web app virtualenv
Aug 12 17:14:08 r0mgmt-standby.localdomain.hpc.udel.edu hadm-api-run[77040]: Starting gunicorn WSGI:
```

## Slurm plugins

The Slurm plugins are C code that must be compiled and installed into Slurm's plugins directory.  Both plugins use the CMake build-system generator.

### Job submission

This component actually consists of two plugins:

- A CLI filter plugin:  run in the client context (`sbatch`, `salloc`, `srun`) to validate the Slurm flags provided in the job script and/or the command line arguments *before* `slurmctld` is presented with a job descriptor
- A job submission plugin:  runs inside `slurmctld` and validates a submitted job descriptor *before* it is turned into a job record

The CLI filter plugin allows us to catch common problems (user isn't running in a workgroup, a partition wasn't indicated) before the job has been handed-off to `slurmctld` via RPC; in theory, this reduces the processing load inside `slurmctld` and shifts it to the user-facing commands.  In reality, because Slurm allows the user to override the `slurm.conf` file location with an environment variable (SLURM_CONF), the user has the ability to disable client plugins by omitting them from an alternative `slurm.conf` file.  So most (if not all) of the checks in a CLI plugin must also be performed in the job submission plugin to guard against such subversion.

> We actually guard against the misuse of SLURM\_CONF by altering the Slurm source code prior to compilation.  All code that implements the SLURM\_CONF override is commented-out (though we debated wrapping it with a uid check to still allow us sysadmins to use it...maybe in the future).

The CMake configuration requires specification of three directories:

- The installation prefix of the target version of Slurm:  use the `SLURM_ROOT` environment variable or `-D` flag to `cmake`
- The source prefix of the target version of Slurm:  use the `SLURM_SRC_ROOT` environment variable or `-D` flag to `cmake`
- The build root of the target version of Slurm:  defaults to the source prefix + `build` but can be overridden using the `SLURM_BUILD_ROOT` environment variable or `-D` flag to `cmake`

There are two semi-vestigal CMake options:

- `SHOULD_ADD_GPU_TYPES`: a boolean, if `On` then the plugins should alter `--gpu` requests to ensure the appropriate sub-type for the selected partition (e.g. `tesla_v100:<N>` for the "gpu-v100" partition)
- `SHOULD_REQUIRE_GPU_TYPE`: a boolean, if `On` and `SHOULD_ADD_GPU_TYPES` is `Off`, then jobs are denied when the `--gpu` flag argument lacks a GPU sub-type

By default `SHOULD_ADD_GPU_TYPES` is `On` and `SHOULD_REQUIRE_GPU_TYPE` is `Off` and we aren't likely to ever alter that, hence the "semi-vestigal" label.

A build on our system looks like:

```
$ cd <repository>/src/hadm_jobsubmit
$ mkdir build-20.11.5
$ cd build-20.11.5
$ vpkg_require cmake/default
$ SLURM_ROOT=/opt/shared/slurm/20.11.5 SLURM_SRC_ROOT=SLURM_ROOT=/opt/shared/slurm/20.11.5/src \
    cmake ..
   :
$ grep ^SLURM_ CMakeCache.txt 
SLURM_BUILD_DIR:PATH=/opt/shared/slurm/20.11.5/src/build-20210421
SLURM_BUILD_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5/src/build-20210421
SLURM_HEADER:FILEPATH=/opt/shared/slurm/20.11.5/include/slurm/slurm.h
SLURM_LIBRARY:FILEPATH=/opt/shared/slurm/20.11.5/lib/libslurm.so
SLURM_MODULES_DIR:PATH=/opt/shared/slurm/20.11.5/lib/slurm
SLURM_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5
SLURM_SRC_DIR:PATH=/opt/shared/slurm/20.11.5/src
SLURM_SRC_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5/src

$ make
   :
$ make install
   :
```

### PrEp

The PrEp plugin makes use of several external libraries:

- pthreads:  the pre-debit transaction queue is processed by multiple threads
- cURL:  for communicating with the HADM REST API
- JSON-C:  for decoding the responses from the HADM REST API
- MUNGE:  for signing the URLs to produce the MungeUrlHash HTTP header

The three Slurm directories required for the job submission plugins are also required by this plugin (see above section for discussion).

There should not be any directory hints necessary for discovery of the pthreads library.  The plugin strongly prefers a library that implements the `pthread_timedjoin_np()` function (but should operate properly using `pthread_join()`).

Variables (environment or CMake define) used to provide installation hints for the other dependencies are as follows:

- cURL:  none available
- JSON-C:  `JSON_C_ROOT`
- MUNGE: `MUNGE_ROOT`

There are two build options present:

- `ENABLE_LOGGING_THREAD_DEBUG`: a boolean, when `On` the logging thread test program will emit tons of information to assist in debugging
- `ENABLE_LOGGING_THREAD_LOG_TO_JOB_STDERR`:  a boolean, when `On` if the job record inside Slurm has a `std_err` path present then attempt to write any error information to that file

The `ENABLE_LOGGING_THREAD_DEBUG` defaults to `On` because the purpose of the test program is to debug the function of the threaded queue...we're bound to want all that output!

The `ENABLE_LOGGING_THREAD_LOG_TO_JOB_STDERR` defaults to `Off` because its success is minimal:  if the file system on the `slurmctld` node is not the same as the execution and submission nodes (e.g. user home directories aren't present) then it's impossible for `slurmctld` to write to the file.  The same holds true if target directories are mounted but the `slurmctld` user cannot write to them.  In practice, we found this method for logging just didn't work, but left the option because it took a bit of work to implement.

A build on our system looks like:

```
$ cd <repository>/src/hadm_prep
$ mkdir build-20.11.5
$ cd build-20.11.5
$ vpkg_require cmake/default
$ SLURM_ROOT=/opt/shared/slurm/20.11.5 SLURM_SRC_ROOT=SLURM_ROOT=/opt/shared/slurm/20.11.5/src \
  MUNGE_ROOT=/opt/shared/munge \
    cmake ..
   :
$ egrep '^(CURL|JSON|MUNGE|SLURM)_' CMakeCache.txt 
CURL_INCLUDE_DIR:PATH=/usr/include
CURL_LIBRARY:FILEPATH=/usr/lib64/libcurl.so
JSON_C_HEADER:FILEPATH=/usr/include/json-c/json.h
JSON_C_LIBRARY:FILEPATH=/usr/lib64/libjson-c.so
JSON_C_ROOT:PATH=/usr
MUNGE_HEADER:FILEPATH=/opt/shared/munge/include/munge.h
MUNGE_LIBRARY:FILEPATH=/opt/shared/munge/lib/libmunge.so
MUNGE_ROOT:UNINITIALIZED=/opt/shared/munge
SLURM_BUILD_DIR:PATH=/opt/shared/slurm/20.11.5/src/build-20210421
SLURM_BUILD_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5/src/build-20210421
SLURM_HEADER:FILEPATH=/opt/shared/slurm/20.11.5/include/slurm/slurm.h
SLURM_LIBRARY:FILEPATH=/opt/shared/slurm/20.11.5/lib/libslurm.so
SLURM_MODULES_DIR:PATH=/opt/shared/slurm/20.11.5/lib/slurm
SLURM_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5
SLURM_SRC_DIR:PATH=/opt/shared/slurm/20.11.5/src
SLURM_SRC_ROOT:UNINITIALIZED=/opt/shared/slurm/20.11.5/src
CURL_INCLUDE_DIR-ADVANCED:INTERNAL=1
CURL_LIBRARY-ADVANCED:INTERNAL=1

$ make
   :
$ make install
   :
```

### Configuration

With the plugins built and installed, the `slurm.conf` looks like:

```
JobSubmitPlugins=hadm
CliFilterPlugins=hadm
PrEpPlugins=script,hadm
PrEpParameters="interval.maintenance=900 threads.max=13 threads.min=3"
```

### Adding partition-to-resource mappings

The plugins require knowledge of what billable RDR is associated with each partition for which an allocation should be charged.  At this time, there is an independent implementation of this functionality in each of the plugins.

The PrEp plugin has an array of `partition_to_rdr_t` data structures that associate a partition name with an RDR type (cpu or gpu).  Additional partitions can be added to the array.  Any partition not present in the array implies that TRES usage of a job is not billed.

The job submission plugins have a `partitions.c/.h` API that features a slightly more complex mapping structure that include GPU sub-types (for the code that forces `--gpu` requests to always include the appropriate sub-type).  Any partition not present produces fewer checks of the job at submission.

At some point it would be helpful to unite some of the common functionalities of these two plugins for greater consistency.  When a new billable partition is created on DARWIN, the plugin code must be modified to produce the appropriate accounting.
