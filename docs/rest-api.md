# Hermes Allocations Data Manager: REST API

Behind the scenes the HADM subsystem uses a PostreSQL database to manage all information regarding:

  - project:  a group of end-users
  - allocation:  a resource grant made to a project
  - activity:  various kinds of credits and debits against an allocation

## Project

The term *project* was chosen in alignment with the terminology of XSEDE.  A project represents a group of end-users who will be provided access to a system and will have one or more grants of service units (SUs) on the RDRs provided by that system.

In terms of the DARWIN system, a project is equivalent to a *workgroup*.  For this reason, the various identfiers associated with a workgroup are used to populate a project record in the system:

  - Project id (an integer)
  - Unix gid number
  - Unix gid name (a.k.a. gname)
  - Slurm account name
  
The `project_id` for each project is used inside the database to link allocation records to a project; but the `project_id` has scope outside the database.  The project id is assigned when a workgroup is created and exists in LDAP as the `udRCIProjId` attribute and is used as an orthogonal id for quota control on Lustre.

## Allocation

Projects have zero or more allocations associated with them.  Each allocation embodies:

  - Allocation id (an integer)
  - Parent project id
  - Category:  the type of allocation, **startup**, **education**, **research**
  - Resource:  the RDR:  **cpu**, **gpu**
  - Start date
  - End date

The `allocation_id` is automatically assigned by the database when tuples are added.  The start date and end date are timestamps that indicate the period during which an allocation can be used by the project.

A project can have AT MOST one allocation of each resource type present at any instant of time.  Attempting to add an allocation for a project that overlaps the start and end date of another (of the same resource type) is not allowed by the database.

## Activity

There are three types of activity represented in the database:

  - Credit: A positive integer that increments available SU on allocation
  - Debit: A negative integer that decrements available SU on allocation
  - Pre-debit:  A negative integer that temporarily decrements available SU on allocation

The balance for an allocation is the aggregate sum of all credits, debits, and pre-debits.

### Credit

A credit is a positive 64-bit integer.  When an allocation is first created, the granted SU is the first credit added to the allocation.  Refunds are another source of credits.

Credits use the `comments` field to describe the nature of the credit.  The `comments` field is text without any inherently-enforced usage:  syntax/grammar can be enforce by external clients/agents.

### Debit

A debit is a negative 64-bit integer.  Debits can have a Unix uid number associated with them in order to promote a per-user break-out of usage of the allocation.  A NULL uid number is permissible for negative adjustments (e.g. XSEDE transfers).

Debits can also make use of the `comments` field in the same capacity as described for credits.

### Pre-debits

When a job is submitted to the scheduler, the scheduler should check to ensure the projected usage for the job is available to the project.  If adequate credit exists, the job should temporarily debit from the allocation.  Once the job is completed (or cancelled) the usage should be updated to the actual value.

The described behavior implies that pre-debits must include additional fields:

  - Job id
  - Owner uid number

The job id is used to track the job's specific usage through its lifespan in the scheduler.  The owner uid number is used when resolving pre-debit jobs into the per-user debit tuples associated with an allocation.

## REST API

This section is organized primarily by route (URL path) and then by the HTTP method used for the request.  The REST API runs on TCP port 8000 on the host named `hadm-api.localdomain.hpc.udel.edu` (or just `hadm-api`) in the cluster's DNS.  Thus, the base URL is **http://hadm-api:8000/**.

There are three roles for access control in the API:

  - **sysadmin**:  root, \_slurmadm, frey, pdw
  - **admin**: **sysadmin** + anita, mkyle, jnhuffma
  - all other users

The REST API is only accessible from inside the cluster.

HTTP protocol errors will return non-200 HTTP responses, but any API errors will return a JSON object:

```
{
    "err_msg": "There was an error....",
    "err_code": -1
}
```

The `err_msg` key will always be present, but the `err_code` is optional.

Authentication of all HTTP requests is by means of the munge signing-authority that is a part of the Slurm infrastructure.  The requested URL is signed by munge, embedding the Unix credentials (uid and gid number) in the message.  This message is added as an HTTP header named **MungeUrlHash**.

### /project

#### GET

Returns a list of projects (as a JSON array of objects).  Each object resembles:

```
{
    "account": "it_nss",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "gid": 1001,
    "gname": "it_nss",
    "modification_date": null,
    "project_id": 1
}
```

The following CGI parameters are understood:

| Parameter   | Discussion                                                                                              | Access level |
| ----------- | ------------------------------------------------------------------------------------------------------- | ------------ |
| uid         | Comma-separated list of Unix uid numbers/names — only include projects to which these users are members | **admin**    |
| gid         | Comma-separated list of Unix gid numbers/names                                                          |              |
| account     | Comma-separated list of Slurm account names                                                             |              |
| project\_id | Comma-separated list of project ids                                                                     |              |

For users not in at least the **admin** role, the returned list of projects will be filtered to ONLY include those to which the user is a member.

#### POST

Create a new project. This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should be a JSON object containing the necessary fields:

```
{
    "account": "it_nss",
    "gid": 1001,
    "gname": "it_nss",
    "project_id": 1
}
```

The successful response will return the JSON object now present in the database:

```
{
    "account": "it_nss",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "gid": 1001,
    "gname": "it_nss",
    "modification_date": null,
    "project_id": 1
}
```

If unsuccessful an error object will be returned.

### /project/<int:project_id>

#### GET

The successful response will return the JSON object associated with the `project_id`:

```
{
    "account": "it_nss",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "gid": 1001,
    "gname": "it_nss",
    "modification_date": null,
    "project_id": 1
}
```

This route and method are only available to the **admin** role.

### /alloc

#### GET

Returns a list of allocations (as a JSON array of objects).  Each object resembles:

```
{
    "allocation_id": 1,
    "category": "startup",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "end_date": "Sat, 31 Jul 2021 04:00:00 GMT",
    "modification_date": null,
    "project_id": 1,
    "resource": "cpu",
    "start_date": "Mon, 03 May 2021 04:00:00 GMT"
}
```

The `project_id` links the allocation to its parent project.

The following CGI parameters are understood:

| Parameter      | Discussion                                                                                                         | Access level |
| -------------- | ------------------------------------------------------------------------------------------------------------------ | ------------ |
| uid            | Comma-separated list of Unix uid numbers/names — include allocations for projects in which these users are members | **admin**    |
| gid            | Comma-separated list of Unix gid numbers/names — include allocations for projects with these gids                  |              |
| account        | Comma-separated list of Slurm account names — include allocations for projects with these accounts                 |              |
| project\_id    | Comma-separated list of project ids — include allocations for these projects                                       |              |
| allocation\_id | Comma-separated list of allocation ids                                                                             |              |

For users not in at least the **admin** role, the returned list of allocations will be filtered to ONLY include those to which the user has access.

#### POST

Create a new allocation. This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should be a JSON object containing the necessary fields:

```
{
    "category": "startup",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "end_date": "Sat, 31 Jul 2021 04:00:00 GMT",
    "resource": "cpu",
    <project-identification>
}
```

where `<project-identification>` is one of the following (since each of them uniquely identifies the parent project):

  - `"project_id": 1`
  - `"gid": 1001`
  - `"gname": "it_nss"`
  - `"account": "it_nss"`

The successful response will return the JSON object now present in the database:

```
{
    "allocation_id": 1,
    "category": "startup",
    "creation_date": "Mon, 10 May 2021 22:02:39 GMT",
    "end_date": "Sat, 31 Jul 2021 04:00:00 GMT",
    "modification_date": null,
    "project_id": 1,
    "resource": "cpu",
    "start_date": "Mon, 03 May 2021 04:00:00 GMT"
}
```

If unsuccessful an error object will be returned.

### /alloc/<int:allocation_id>

#### GET

By default, or with the CGI paramter `report=aggregate`, the successful response will return a JSON object associated with the `allocation_id` with additional activity information:

```
{
    "allocation_id":1,
    "balance":40,
    "category":"startup",
    "creation_date":"Mon, 10 May 2021 22:02:39 GMT",
    "credit":1750,
    "debit":-1510,
    "end_date":"Sat, 31 Jul 2021 04:00:00 GMT",
    "modification_date":null,
    "predebit":-200,
    "project_id":1,
    "resource":"cpu",
    "start_date":"Mon, 03 May 2021 04:00:00 GMT"
}
```

Use of the CGI parameter `report=detail` will instead return the activity information as a list of objects:

```
[
    {
        "activity_kind":"debit",
        "allocation_id":1,
        "amount":-340,
        "comments":"",
        "creation_date":"Tue, 11 May 2021 13:13:47 GMT",
        "modification_date":null,
        "owner_uid":2267
    },
    {
        "activity_kind":"credit",
        "allocation_id":1,
        "amount":500,
        "comments":"renewal AST050009",
        "creation_date":"Tue, 11 May 2021 13:15:21 GMT",
        "modification_date":null,
        "owner_uid":null
    },
              :
    {
        "activity_kind":"predebit",
        "allocation_id":1,
        "amount":-200,
        "comments":"",
        "creation_date":"Thu, 01 Jul 2021 17:38:10 GMT",
        "modification_date":null,
        "owner_uid":1045
    }
]
```

For users not in at least the **admin** role, the user must have access to the allocation's parent project to be successful.

### /alloc/<int:allocation_id>/credit

#### GET

By default, or with the CGI paramter `report=aggregate`, the successful response will return an integer that is the current total credits to the allocation:

```
1750
```

Use of the CGI parameter `report=detail` will instead return the credits as a list of individual activity records:

```
[
    {
        "activity_kind":"credit",
        "allocation_id":1,
        "amount":500,
        "comments":"renewal AST050009",
        "creation_date":"Tue, 11 May 2021 13:15:21 GMT",
        "gid":1001,
        "modification_date":null,
        "owner_uid":null
    },
    {
        "activity_kind":"credit",
        "allocation_id":1,
        "amount":125,
        "comments":"internal refund",
        "creation_date":"Wed, 30 Jun 2021 19:13:27 GMT",
        "gid":1001,
        "modification_date":null,
        "owner_uid":null
    },
           :
]
```

For users not in at least the **admin** role, the user must have access to the allocation's parent project to be successful.

#### POST

Add a credit to the allocation. This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should be a bare integer:

```
1000
```

or a JSON object containing the necessary fields:

```
{
    "amount": 1000,
    "comments": "these guys need more time"
}
```

The successful response will return the JSON object now present in the database:

```
{
    "activity_id": 36,
    "allocation_id": 1,
    "amount": 1000,
    "comments": "these guys need more time",
    "creation_date": "Thu, 01 Jul 2021 20:54:04 GMT",
    "modification_date": null
}
```

If unsuccessful an error object will be returned.

##### Allocation Credit Comments for XSEDE Allocations

When adding credits for an XSEDE allocation the format should be:  
`"credit_type grant_number"`  
where credit_type corresponds to the types used in the AllocationType from AMIE:  

| Type | Description |
| ---- | ----------- |
| new | Award for a new project |
| renewal | Continuing award for existing project, or treated as a new award if the project does not already exist at the site |
| supplement | Add SUs to an existing allocation, or treated as a new award if the project does not already exist at the site |
| transfer | Treated the same as a supplement |
| advance | Award given in anticipation of a new or renewal allocation, treated as a supplement |
| adjustment | Modifies an existing allocation for other reasons (should be used in the case of a manual credit originating from the UD side) |

**NOTE**: XSEDE allocation credits that need to be reported back to XSEDE should have a comment that starts with `XSEDE:`.  This is used by the AMIE UD Backend API to detect manual credits/debits that should be reported as adjustment records to XSEDE's Usage API.  This only applies to manually added credits - all Slurm activity will get reported automatically.  
  
**VERY IMPORTANT**:  Starting a comment with "XSEDE:" should only be used for adjustments originating from the UD side.  If you do not start the comment with `XSEDE:` then it will not get reported to XSEDE, and the allocation balance on the XSEDE Portal will be out of sync with `sproject`.  It should **never** be used for credits that are added as the result of an AMIE packet.  This would double up the amount credited on the XSEDE Portal since XSEDE already accounts for credits/debits that it sends to us in an AMIE packet.

### /alloc/<int:allocation_id>/debit

#### GET

By default, or with the CGI paramter `report=aggregate`, the successful response will return an integer that is the current total debits against the allocation:

```
-1510
```

Use of the CGI parameter `report=detail` will instead return the debits as a list of individual activity records:

```
[
    {
        "activity_kind":"debit",
        "allocation_id":1,
        "amount":-340,
        "comments":"",
        "creation_date":"Tue, 11 May 2021 13:13:47 GMT",
        "gid":1001,
        "modification_date":null,
        "owner_uid":2267
    },
    {
        "activity_kind":"debit",
        "allocation_id":1,
        "amount":-1170,
        "comments":"",
        "creation_date":"Tue, 11 May 2021 13:13:47 GMT",
        "gid":1001,
        "modification_date":"Thu, 01 Jul 2021 17:33:37 GMT",
        "owner_uid":1001
    }
]
```

For users not in at least the **admin** role, the user must have access to the allocation's parent project to be successful.

#### POST

Add a debit to the allocation. This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should be a bare integer:

```
-250
```

of a JSON object containing the necessary fields:

```
{
    "amount": -250,
    "comments": "these guys are abusing the system"
}
```

The successful response will return the JSON object now present in the database:

```
{
    "activity_id":37,
    "allocation_id":1,
    "amount":-250,
    "comments":"these guys are abusing the system",
    "creation_date":"Thu, 01 Jul 2021 21:00:19 GMT",
    "modification_date":null,
    "owner_uid":null
}
```

If unsuccessful an error object will be returned.

##### Allocation Debit Comments for XSEDE Allocations

When adding debits for an XSEDE allocation the format should be:  
`"debit_type grant_number"`  
where debit_type corresponds to the types used in the AllocationType from AMIE:  

| Type | Description |
| ---- | ----------- |
| transfer | Transfer of SUs to another XSEDE resource |
| adjustment | Modifies an existing allocation for other reasons (should be used in the case of a manual debit originating from the UD side) |

**NOTE**: XSEDE allocation debits that need to be reported back to XSEDE should have a comment that starts with `XSEDE:`.  This is used by the AMIE UD Backend API to detect manual credits/debits that should be reported as adjustment records to XSEDE's Usage API.  This only applies to manually added credits - all Slurm activity will get reported automatically.  
  
**VERY IMPORTANT**:  Starting a comment with `XSEDE:` should only be used for adjustments originating from the UD side.  If you do not start the comment with `XSEDE:` then it will not get reported to XSEDE, and the allocation balance on the XSEDE Portal will be out of sync with `sproject`.  It should **never** be used for debits that are added as the result of an AMIE packet.  This would double up the amount debited on the XSEDE Portal since XSEDE already accounts for credits/debits that it sends to us in an AMIE packet.

### /job

#### GET

Returns jobs (pre-debit activities) as a list of objects:

```
[
    {
        "activity_id":27,
        "allocation_id":3,
        "amount":-120,
        "comments":null,
        "creation_date":"Thu, 27 May 2021 20:35:58 GMT",
        "job_id":90999,
        "modification_date":null,
        "owner_uid":1006,
        "status":"submitted"
    },
    {
        "activity_id":35,
        "allocation_id":1,
        "amount":-200,
        "comments":null,
        "creation_date":"Thu, 01 Jul 2021 17:38:10 GMT",
        "job_id":40456,
        "modification_date":null,
        "owner_uid":1045,
        "status":"submitted"
    }
]
```

The following CGI parameters are understood:

| Parameter      | Discussion                                                                         | Access level |
| -------------- | ---------------------------------------------------------------------------------- | ------------ |
| owner\_uid     | Comma-separated list of Unix uid numbers/names — only include jobs for these users | **admin**    |
| gid            | Comma-separated list of Unix gid numbers/names                                     |              |
| account        | Comma-separated list of Slurm account names                                        |              |
| project\_id    | Comma-separated list of project ids                                                |              |
| allocation\_id | Comma-separated list of allocation ids                                             |              |
| status         | Comma-separated list of statuses (submitted, completed)                            |              |

For users not in at least the **admin** role, the returned list of jobs will be filtered to ONLY include those to which the user is owner.

#### POST

Create a new job (pre-debit). This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should be a JSON object containing the necessary fields:

```
{
    "job_id": 32768,
    "owner_uid": 1001,
    "amount": 450,
    <allocation-identification>
}
```

where `<allocation-identification>` is one of the following (since each of them uniquely identifies the parent project):

  - `"allocation_id":1`
  - `"account":"it_nss"` and `"resource":"cpu"`

The successful response will return the JSON object now present in the database:

```
{
    "activity_id":38,
    "allocation_id":1,
    "amount":-450,
    "comments":null,
    "creation_date":"Thu, 01 Jul 2021 21:16:16 GMT",
    "job_id":32768,
    "modification_date":null,
    "owner_uid":1001,
    "status":"submitted"
}
```

If unsuccessful an error object will be returned.  For example, if the allocation does not have enough credit for the requested amount:

```
{
    "err_code":-1,
    "err_msg":"ERROR:  Requested allocation has insufficient balance: 340 < 4500000000 (HINT:  Decrease the wall time or requested resource levels of the job)"
}
```

### /job/<int:job_id>

#### GET

Returns the job (pre-debit) object with the given Slurm job id:

```
{
    "activity_id":38,
    "allocation_id":1,
    "amount":-450,
    "comments":null,
    "creation_date":"Thu, 01 Jul 2021 21:16:16 GMT",
    "job_id":32768,
    "modification_date":null,
    "owner_uid":1001,
    "status":"submitted"
}
```

For users not in at least the **admin** role, the user must own the job to be successful.

#### PUT

Update the amount and/or status of an existing job (pre-debit) record.  This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should contain a JSON object with the updated fields:

```
{
    "amount": -425,
    "status": "submitted"
}
```

The successful response will return the full updated JSON object:

```
{
    "activity_id":38,
    "allocation_id":1,
    "amount":-425,
    "comments":null,
    "creation_date":"Thu, 01 Jul 2021 21:16:16 GMT",
    "job_id":32768,
    "modification_date":"Thu, 01 Jul 2021 21:23:03 GMT",
    "owner_uid":1001,
    "status":"submitted"
}
```

If unsuccessful an error object will be returned.

### /job/activity-<int:activity_id>

This route is an alternative to the /job/<int:job_id> route (and methods) that references jobs (pre-debits) by activity id, not job id.

### /job/resolve

#### POST

Attempt to resolve a list of (pre-debit) allocation ids from the completed status.  Resolving an allocation\_id shifts its amount to a debit record for the owner\_uid.  This route and method are **only** available to the **sysadmin** role.

The body of the HTTP request should contain a JSON array of activity ids to resolve:

```
[1, 2, 3, 4, 5]
```

The response will be a JSON array of the activity ids that could not be resolved:

```
[1, 2, 3]
```

### /failure

#### GET

Returns jobs that failed to execute (e.g. due to lack of allocation credit) as a list of objects:

```
[
    {
        "activity_id":72,
        "allocation_id":1,
        "amount":2000000,
        "comments":"Requested allocation has insufficient balance: 95539 < 2000000 (HINT:  Decrease the wall time or requested resource levels of the job)",
        "creation_date":"Tue, 13 Jul 2021 05:08:39 GMT",
        "job_id":998877,
        "modification_date":null,
        "owner_uid":1001
    },{
        "activity_id":75,
        "allocation_id":3,
        "amount":7680,
        "comments":"Requested allocation has insufficient balance: 3641 < 7680 (HINT:  Decrease the wall time or requested resource levels of the job)",
        "creation_date":"Tue, 13 Jul 2021 12:57:10 GMT",
        "job_id":60472,
        "modification_date":null,
        "owner_uid":2117
    }
]
```

The following CGI parameters are understood:

| Parameter      | Discussion                                                                         | Access level |
| -------------- | ---------------------------------------------------------------------------------- | ------------ |
| owner\_uid     | Comma-separated list of Unix uid numbers/names — only include jobs for these users | **admin**    |
| gid            | Comma-separated list of Unix gid numbers/names                                     |              |
| account        | Comma-separated list of Slurm account names                                        |              |
| project\_id    | Comma-separated list of project ids                                                |              |
| allocation\_id | Comma-separated list of allocation ids                                             |              |
| job\_id        | Comma-separated list of job ids                                                    |              |

For users not in at least the **admin** role, the returned list of failures will be filtered to ONLY include those to which the user is owner.

## CLI Utility

A wrapper for the `curl` command is present which automatically adds the **MungeUrlHash** header to the request.  It recognizes the following CLI options:

| Option                | Discussion                                                          |
| --------------------- | ------------------------------------------------------------------- |
| -v/--verbose          | Increase the verbosity of the `curl` output                         |
| -X/--request <method> | The HTTP method to use (e.g. GET, PUT, POST)                        |
| --json                | The Content-type for a PUT or POST request will be application/json |
| --text                | The Content-type for a PUT or POST request will be plain/text       |
| --data-* <data>       | The data to send in a PUT or POST request                           |

Example commands follow.

### List jobs

```
$ bin/hadm-curl /job
[{"activity_id":27,"allocation_id":3,"amount":-120,"comments":null,"creation_date":"Thu, 27 May 2021 20:35:58 GMT","job_id":90999,"modification_date":null,"owner_uid":1006,"status":"submitted"},{"activity_id":35,"allocation_id":1,"amount":-200,"comments":null,"creation_date":"Thu, 01 Jul 2021 17:38:10 GMT","job_id":40456,"modification_date":null,"owner_uid":1045,"status":"submitted"},{"activity_id":38,"allocation_id":1,"amount":-425,"comments":null,"creation_date":"Thu, 01 Jul 2021 21:16:16 GMT","job_id":32768,"modification_date":"Thu, 01 Jul 2021 21:23:03 GMT","owner_uid":1001,"status":"submitted"}]
```

### Add a project

```
$ bin/hadm-curl -X POST --json --data-binary '{"account":"it_nss","gid":1001,"gname":"it_nss","project_id":1}' /project
{"account":"it_nss","creation_date":"Mon, 10 May 2021 22:02:39 GMT","gid":1001,"gname":"it_nss","modification_date":null,"project_id":1}
```
