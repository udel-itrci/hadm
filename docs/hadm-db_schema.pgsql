--
-- The allocation types that we recognize:
--
CREATE TYPE ALLOCATION_CATEGORY AS ENUM ('startup', 'education', 'research', 'explore', 'discover', 'accelerate');

--
-- The Resource Definition Record types that we recognize:
--
CREATE TYPE RDR AS ENUM ('cpu', 'gpu');

--
-- For pre-debits we need to diffentiate between a record added by the
-- prolog context of the PrEp plugin versus the resolved usage updated by the
-- epilog context of the PrEp plugin:
--
CREATE TYPE PREDEBIT_STATUS AS ENUM ('executing', 'completed');

--
-- Each local or XSEDE project (workgroup) gets added to this
-- table
--
CREATE TABLE projects (
    project_id          INTEGER PRIMARY KEY NOT NULL,
    gid                 INTEGER UNIQUE NOT NULL,
    gname               TEXT UNIQUE NOT NULL,
    account             TEXT UNIQUE NOT NULL,
    
    creation_date       TIMESTAMP WITH TIME ZONE DEFAULT now(),
    modification_date   TIMESTAMP WITH TIME ZONE
);

--
-- Projects can be granted zero or more allocations.  Each allocation is
-- of a specific category and against a specific resource type, and has a
-- start and end date during which it is usable.
--
-- The actual amoount of the allocation is not contained in this table.
-- Instead, additional tables are present for credits and the various debits
-- against the allocation.
--
CREATE TABLE allocations (
    allocation_id       BIGSERIAL PRIMARY KEY NOT NULL,
    project_id          INTEGER NOT NULL REFERENCES projects(project_id)
                        ON DELETE CASCADE,
    category            ALLOCATION_CATEGORY NOT NULL,
    resource            RDR NOT NULL,
    start_date          TIMESTAMP WITH TIME ZONE NOT NULL,
    end_date            TIMESTAMP WITH TIME ZONE NOT NULL,
    
    creation_date       TIMESTAMP WITH TIME ZONE DEFAULT now(),
    modification_date   TIMESTAMP WITH TIME ZONE
);
-- We use a trigger function to ensure per-project there are no overlapping allocations
-- in a temporal sense.
CREATE OR REPLACE FUNCTION allocation_is_valid_for_insert() RETURNS TRIGGER AS $$
    DECLARE
        aRow        RECORD;
    BEGIN
        SELECT COUNT(*) AS found INTO aRow FROM allocations WHERE project_id = NEW.project_id AND resource = NEW.resource AND ((start_date BETWEEN NEW.start_date AND NEW.end_date) OR (end_date BETWEEN NEW.start_date AND NEW.end_date));
        IF aRow.found > 0 THEN
            RAISE EXCEPTION 'Only one allocation per project may be active during a time period.';
        END IF;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER pre_check_allocation_on_insert BEFORE INSERT ON allocations
    FOR EACH ROW EXECUTE FUNCTION allocation_is_valid_for_insert();
-- We use a trigger function to ensure per-project there are no overlapping allocations
-- in a temporal sense.
CREATE OR REPLACE FUNCTION allocation_is_valid_for_update() RETURNS TRIGGER AS $$
    DECLARE
        aRow        RECORD;
    BEGIN
        IF OLD.allocation_id != NEW.allocation_id THEN
            RAISE EXCEPTION 'Allocation ids cannot be altered.';
        END IF;
        IF OLD.project_id != NEW.project_id THEN
            RAISE EXCEPTION 'Allocation project_ids cannot be altered.';
        END IF;
        SELECT COUNT(*) AS found INTO aRow FROM allocations WHERE project_id = NEW.project_id AND resource = NEW.resource AND category = NEW.category AND ((start_date BETWEEN NEW.start_date AND NEW.end_date) OR (end_date BETWEEN NEW.start_date AND NEW.end_date));
        IF aRow.found > 0 THEN
            RAISE EXCEPTION 'This allocation would overlap with another in the time period.';
        END IF;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER pre_check_allocation_on_update BEFORE UPDATE ON allocations
    FOR EACH ROW EXECUTE FUNCTION allocation_is_valid_for_update();


--
-- Base table for credit/debit activities on an allocation:
--
CREATE TABLE activity (
    activity_id         BIGSERIAL NOT NULL,
    allocation_id       BIGINT NOT NULL REFERENCES allocations(allocation_id)
                        ON DELETE CASCADE,
    amount              BIGINT NOT NULL DEFAULT 0,
    comments            TEXT,
    
    creation_date       TIMESTAMP WITH TIME ZONE DEFAULT now(),
    modification_date   TIMESTAMP WITH TIME ZONE
);
CREATE SEQUENCE activity_id_seq OWNED BY NONE;

--
-- Credits are SU increments
--
CREATE TABLE credits (
) INHERITS (activity);
ALTER TABLE credits ALTER COLUMN activity_id SET DEFAULT nextval('activity_id_seq');
CREATE UNIQUE INDEX credits_activity_id_idx ON credits (activity_id);

--
-- Pre-debits are job-oriented projected and completed SU decrements that
-- should be added when a job executes and updated once it completes
--
CREATE TABLE predebits (
    job_id              INTEGER UNIQUE NOT NULL,
    status              PREDEBIT_STATUS NOT NULL DEFAULT 'executing',
    owner_uid           INTEGER NOT NULL
) INHERITS (activity);
ALTER TABLE predebits ALTER COLUMN activity_id SET DEFAULT nextval('activity_id_seq');
CREATE UNIQUE INDEX predebits_activity_id_idx ON predebits (activity_id);

--
-- Failures are job-oriented projected SU decrements that exceeded the
-- credits available to the allocation.
--
CREATE TABLE failures (
    job_id              INTEGER UNIQUE NOT NULL,
    owner_uid           INTEGER NOT NULL,
) INHERITS (activity);
ALTER TABLE failures ALTER COLUMN activity_id SET DEFAULT nextval('activity_id_seq');
CREATE UNIQUE INDEX failures_activity_id_idx ON failures (activity_id);

--
-- Debits are resolved and aggregated pre-debits that have completed
-- status
--
CREATE TABLE debits (
    owner_uid           INTEGER
) INHERITS (activity);
ALTER TABLE debits ALTER COLUMN activity_id SET DEFAULT nextval('activity_id_seq');
CREATE UNIQUE INDEX debits_activity_id_idx ON debits (activity_id);
CREATE UNIQUE INDEX debits_unique_row_idx ON debits (allocation_id, owner_uid);


--
-- A view that aggregates projects and their allocations into a single
-- table.
--
CREATE VIEW projects_with_allocations AS
    SELECT P.project_id, P.gid, P.gname, P.account, A.allocation_id, A.category, A.resource, A.start_date, A.end_date, A.creation_date, A.modification_date FROM projects AS P
        INNER JOIN allocations AS A ON (A.project_id = P.project_id)
        ORDER BY P.project_id, A.allocation_id;


--
-- A view that aggregates debits and credits to produce the balance of each
-- allocation
--
CREATE VIEW balances AS
    SELECT A.allocation_id, C.credit, SUM(P.predebit) AS predebit, D.debit, C.credit+SUM(P.predebit)+D.debit AS balance FROM allocations AS A
        LEFT JOIN credits_collapsed AS C ON (C.allocation_id = A.allocation_id)
        LEFT JOIN predebits_collapsed AS P ON (P.allocation_id = A.allocation_id)
        LEFT JOIN debits_collapsed AS D ON (D.allocation_id = A.allocation_id)
        GROUP BY A.allocation_id, C.credit, D.debit;

--
-- A view that aggregates all credits; allocations with no credits will appear
-- as zero
--
CREATE VIEW credits_collapsed AS
    SELECT A.allocation_id, COALESCE(SUM(C.amount), 0) AS credit FROM allocations AS A
        LEFT JOIN credits AS C ON (C.allocation_id = A.allocation_id)
        GROUP BY A.allocation_id;

--
-- A view that aggregates all debits; allocations with no debits will appear
-- as zero
--
CREATE VIEW debits_collapsed AS
    SELECT A.allocation_id, COALESCE(SUM(D.amount), 0) AS debit FROM allocations AS A
        LEFT JOIN debits AS D ON (D.allocation_id = A.allocation_id)
        GROUP BY A.allocation_id;

--
-- A view that aggregates all pre-debits by job status and irrespective
-- of the job owner; allocations with no pre-debits will appear as zero
--
CREATE VIEW predebits_collapsed AS
    SELECT A.allocation_id, P.status, COALESCE(SUM(P.amount), 0) AS predebit FROM allocations AS A
        LEFT JOIN predebits AS P ON (P.allocation_id = A.allocation_id)
        GROUP BY A.allocation_id, P.status;

--
-- A view that aggregates debits by job owner
--
CREATE VIEW debits_by_uid AS
    SELECT allocation_id, owner_uid, SUM(amount) AS balance FROM (
        SELECT allocation_id, owner_uid, amount FROM predebits UNION (SELECT allocation_id, owner_uid, amount FROM debits)
    ) AS combined_debits
    GROUP BY allocation_id, owner_uid;

--
-- A view that combines the full allocation tuple with its resolved balance information
--
CREATE VIEW allocations_with_balances AS
    SELECT A.*, B.credit, B.predebit, B.debit, B.balance FROM balances AS B
        INNER JOIN allocations AS A ON (A.allocation_id = B.allocation_id);


CREATE VIEW allocation_details AS 
    SELECT allocation_id, amount, comments, creation_date, modification_date, 'credit' AS activity_kind, NULL as owner_uid FROM credits
    UNION (SELECT allocation_id, SUM(amount), '' AS comments, MAX(creation_date) AS creation_date, MAX(modification_date) AS modification_date, 'predebit' AS activity_kind, owner_uid FROM predebits GROUP BY allocation_id, activity_kind, owner_uid)
    UNION (SELECT allocation_id, SUM(amount), '' AS comments, MAX(creation_date) AS creation_date, MAX(modification_date) AS modification_date, 'debit' AS activity_kind, owner_uid FROM debits GROUP BY allocation_id, activity_kind, owner_uid);


--
-- Lookup project id:
--
CREATE OR REPLACE FUNCTION get_project_id_for_gid(in_gid INTEGER) RETURNS BIGINT AS $$
    SELECT project_id FROM projects WHERE gid = in_gid
$$ LANGUAGE SQL;
--
CREATE OR REPLACE FUNCTION get_project_id_for_gname(in_gname TEXT) RETURNS BIGINT AS $$
    SELECT project_id FROM projects WHERE gname = in_gname
$$ LANGUAGE SQL;
--
CREATE OR REPLACE FUNCTION get_project_id_for_account(in_account TEXT) RETURNS BIGINT AS $$
    SELECT project_id FROM projects WHERE account = in_account
$$ LANGUAGE SQL;


--
-- Get current allocation for a given project and resource:
--
CREATE OR REPLACE FUNCTION get_current_allocation(in_project_id INTEGER, in_resource RDR) RETURNS BIGINT AS $$
    SELECT allocation_id FROM allocations WHERE project_id = in_project_id AND resource = in_resource AND start_date <= now() AND end_date >= now()
$$ LANGUAGE SQL;
--
CREATE OR REPLACE FUNCTION get_current_allocation_for_gid(in_gid INTEGER, in_resource RDR) RETURNS BIGINT AS $$
    SELECT allocation_id FROM allocations WHERE project_id IN (SELECT project_id FROM projects WHERE gid = in_gid) AND resource = in_resource AND start_date <= now() AND end_date >= now()
$$ LANGUAGE SQL;
--
CREATE OR REPLACE FUNCTION get_current_allocation_for_gname(in_gname TEXT, in_resource RDR) RETURNS BIGINT AS $$
    SELECT allocation_id FROM allocations WHERE project_id IN (SELECT project_id FROM projects WHERE gname = in_gname) AND resource = in_resource AND start_date <= now() AND end_date >= now()
$$ LANGUAGE SQL;
--
CREATE OR REPLACE FUNCTION get_current_allocation_for_account(in_account TEXT, in_resource RDR) RETURNS BIGINT AS $$
    SELECT allocation_id FROM allocations WHERE project_id IN (SELECT project_id FROM projects WHERE account = in_account) AND resource = in_resource AND start_date <= now() AND end_date >= now()
$$ LANGUAGE SQL;


--
-- Resolve all completed pre-debit records into the debit table.  It's best to wrap
-- this in a transaction to avoid partial resolution of predebit records.
--
CREATE OR REPLACE FUNCTION resolve_completed_jobs() RETURNS INTEGER AS $$
    DECLARE
        aRow        RECORD;
        dummy       RECORD;
        resolved    INTEGER;
    BEGIN
        resolved := 0;
        FOR aRow IN SELECT * FROM predebits WHERE status = 'completed' LOOP
            SELECT pg_advisory_lock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
            INSERT INTO debits AS d (allocation_id, amount, owner_uid) VALUES (aRow.allocation_id, aRow.amount, aRow.owner_uid)
                ON CONFLICT (allocation_id, owner_uid) DO UPDATE SET amount = d.amount + EXCLUDED.amount, modification_date = now();
            DELETE FROM predebits WHERE activity_id = aRow.activity_id;
            SELECT pg_advisory_unlock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
            resolved := resolved + 1;
        END LOOP;
        RETURN resolved;
    END;
$$ LANGUAGE plpgsql;


--
-- Resolve a subset of completed pre-debit records into the debit table.  The
-- subset of activity_id values is passed as an array of 64-bit integers.
--
CREATE OR REPLACE FUNCTION resolve_array_of_completed_jobs(activity_ids BIGINT[]) RETURNS SETOF BIGINT AS $$
    DECLARE
        aRow        RECORD;
        dummy       RECORD;
        act_id      BIGINT;
    BEGIN
        FOREACH act_id IN ARRAY activity_ids LOOP
            SELECT * FROM predebits INTO aRow WHERE status = 'completed' AND activity_id = act_id;
            IF FOUND THEN
                SELECT pg_advisory_lock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
                INSERT INTO debits AS d (allocation_id, amount, owner_uid) VALUES (aRow.allocation_id, aRow.amount, aRow.owner_uid)
                    ON CONFLICT (allocation_id, owner_uid) DO UPDATE SET amount = d.amount + EXCLUDED.amount, modification_date = now();
                DELETE FROM predebits WHERE activity_id = act_id;
                SELECT pg_advisory_unlock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
                RETURN NEXT act_id;
            END IF;
        END LOOP;
        RETURN;
    END;
$$ LANGUAGE plpgsql;


--
-- Resolve a subset of completed pre-debit records into the debit table.  The
-- subset of activity_id values is drawn from the named table, which must be a single
-- column of BIGINTs.
--
-- The table of ids to resolve is 'resolve_completed_jobs_activity_ids' and is best
-- handled as a per-session temporary table.
--
CREATE OR REPLACE FUNCTION resolve_subset_of_completed_jobs() RETURNS INTEGER AS $$
    DECLARE
        aRow        RECORD;
        dummy       RECORD;
        resolved    INTEGER;
    BEGIN
        resolved := 0;
        FOR aRow IN SELECT * FROM predebits WHERE status = 'completed' AND activity_id IN (SELECT activity_id FROM resolve_completed_jobs_activity_ids) LOOP
            SELECT pg_advisory_lock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
            INSERT INTO debits AS d (allocation_id, amount, owner_uid) VALUES (aRow.allocation_id, aRow.amount, aRow.owner_uid)
                ON CONFLICT (allocation_id, owner_uid) DO UPDATE SET amount = d.amount + EXCLUDED.amount, modification_date = now();
            DELETE FROM predebits WHERE activity_id = aRow.activity_id;
            DELETE FROM resolve_completed_jobs_activity_ids WHERE activity_id = aRow.activity_id;
            SELECT pg_advisory_unlock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
            resolved := resolved + 1;
        END LOOP;
        RETURN resolved;
    END;
$$ LANGUAGE plpgsql;


--
-- The job submission plugin would in effect be calling THIS function
-- to check available SU against the allocation and make the predebit if
-- allowable.  Also handles an already-executing job_id that is attempting
-- to change its projected usage.
--
CREATE OR REPLACE FUNCTION submission_check_by_allocation(in_allocation BIGINT, in_job_id INTEGER, in_owner_uid INTEGER, in_amount BIGINT) RETURNS BIGINT AS $$
    DECLARE
        aRow            RECORD;
        dummy           RECORD;
        delta_amount    BIGINT;
        the_activity_id BIGINT;
    BEGIN
        -- Ensure incoming amount is positive for the sake of our checks:
        IF in_amount < 0 THEN
            in_amount := -in_amount;
        END IF;
        
        -- Does this job already exist and have 'executing' status?
        SELECT * INTO aRow FROM predebits WHERE job_id = in_job_id AND status = 'executing';
        IF FOUND THEN
            delta_amount = in_amount + aRow.amount;
            the_activity_id := aRow.activity_id;
        ELSE
            delta_amount = in_amount;
            the_activity_id := NULL;
        END IF;
        
        -- Serialize this critical section:
        SELECT pg_advisory_lock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
        
        -- Check for available balance:
        SELECT balance INTO aRow FROM balances WHERE allocation_id = in_allocation;
        IF FOUND AND aRow.balance < delta_amount THEN
            SELECT pg_advisory_unlock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
            RAISE EXCEPTION 'Requested allocation has insufficient balance: % < %', aRow.balance, delta_amount
                USING HINT = 'Decrease the wall time or requested resource levels of the job';
        END IF;
        
        IF the_activity_id IS NOT NULL THEN
            -- Update existing record in predebit table:
            UPDATE predebits SET amount=-in_amount, modification_date=now() WHERE activity_id = the_activity_id;
        ELSE
            -- Insert into the predebit table:
            INSERT INTO predebits (allocation_id, amount, job_id, owner_uid) VALUES (in_allocation, -in_amount, in_job_id, in_owner_uid) RETURNING activity_id INTO aRow;
            the_activity_id := aRow.activity_id;
        END IF;
        SELECT pg_advisory_unlock(x'6861646D6C6F636B'::BIGINT) INTO dummy;
        RETURN the_activity_id;
    END;
$$ LANGUAGE plpgsql;
--
CREATE OR REPLACE FUNCTION submission_check_by_account(in_account TEXT, in_resource RDR, in_job_id INTEGER, in_owner_uid INTEGER, in_amount BIGINT) RETURNS BIGINT AS $$
    DECLARE
        aRow            RECORD;
        the_allocation  BIGINT;
    BEGIN
        -- Resolve to allocation_id:
        the_allocation := get_current_allocation_for_account(in_account, in_resource);
        IF the_allocation IS NULL THEN
            RAISE EXCEPTION 'Requested account and resource have no active allocation';
        END IF;
        
        RETURN submission_check_by_allocation(the_allocation, in_job_id, in_owner_uid, in_amount);
    END;
$$ LANGUAGE plpgsql;


--
-- Demo:
--
INSERT INTO projects (project_id, gid, gname, account) VALUES
        (1, 1001, 'it_nss', 'it_nss');
INSERT INTO projects (project_id, gid, gname, account) VALUES
        (2, 1002, 'it_css', 'it_css');

INSERT INTO allocations (project_id, category, resource, start_date, end_date) VALUES
        (1, 'startup', 'cpu', '2021-05-03', '2021-07-31');
INSERT INTO allocations (project_id, category, resource, start_date, end_date) VALUES
        (1, 'startup', 'gpu', '2021-05-03', '2021-07-31');
INSERT INTO allocations (project_id, category, resource, start_date, end_date) VALUES
        (2, 'research', 'cpu', '2021-05-03', '2021-07-31');

-- Initial allocation credit:
INSERT INTO credits (allocation_id, amount, comments) VALUES
        (get_current_allocation(1, 'cpu'), 1000, 'new AST040002'),
        (get_current_allocation(1, 'gpu'), 2500, 'new AST040002'),
        (get_current_allocation(2, 'cpu'), 10000, 'new AST040045');

-- Add a couple pending/running jobs:
INSERT INTO predebits (allocation_id, job_id, amount, owner_uid) VALUES
        (get_current_allocation(1, 'cpu'), 12345, -150, 1001);
INSERT INTO predebits (allocation_id, job_id, amount, owner_uid) VALUES
        (get_current_allocation(1, 'cpu'), 12346, -150, 1001);
INSERT INTO predebits (allocation_id, job_id, amount, owner_uid) VALUES
        (get_current_allocation(1, 'cpu'), 12347, -425, 2267);

SELECT * FROM balances;
SELECT * FROM predebits_collapsed;
SELECT * FROM debits_by_uid;
SELECT * FROM allocations_with_balances;

-- Now we'll transition two jobs to being completed:
UPDATE predebits SET status='completed',amount=FLOOR(0.8*amount),modification_date=now() WHERE job_id IN (12345, 12347);

SELECT * FROM predebits_collapsed;

-- Let's reconcile completed jobs:
SELECT resolve_completed_jobs();

SELECT * FROM balances;
SELECT * FROM predebits_collapsed;
SELECT * FROM debits_by_uid;
SELECT * FROM allocations_with_balances;

-- Infuse a refund:
INSERT INTO credits (allocation_id, amount) VALUES
        (get_current_allocation(1, 'cpu'), 500);

SELECT * FROM balances;
SELECT * FROM predebits_collapsed;
SELECT * FROM debits_by_uid;
SELECT * FROM allocations_with_balances;


--
-- Docker environment for postgresql images:
--
--    POSTGRES_DB=allocations
--    POSTGRES_USER=allocations
--    POSTGRES_PASSWORD=++69-EXPERIENCE-CONSIDERABLE-understood-temperature-56++
--    POSTGRES_HOST_AUTH_METHOD=md5
--    POSTGRES_INITDB_ARGS=--auth-host=md5
--
-- These variables are defined in a file that Singularity can read when the container is started
-- (e.g. /var/lib/postgresql-containers/allocations.env).
--
-- The container stores the postgres database under /var/lib/postgresql/data, so a bind-mount
-- at that path is advisable.  E.g.
--
--    mkdir /var/lib/postgresql-containers/allocations{,/root,/data,/run}
--    singularity instance start --env-file /var/lib/postgresql-containers/allocations/environment --no-home \
--                  --bind /var/lib/postgresql-containers/allocations/data:/var/lib/postgresql/data:rw \
--                  --bind /var/lib/postgresql-containers/allocations/root:/root:rw \
--                  --bind /var/lib/postgresql-containers/allocations/run:/run:rw \
--                  /var/lib/postgresql-containers/allocations/container.sif postgresql-allocations
--

-- Special privileges granted to (root,_slurmadm,frey,pdw,anita,mkyle,jnhuffma).  Uid and gid are authenticated
-- by encoding the path portion of the request URL with munge.
--
-- Query parameters meant to filter results are always combined with logical OR.
--
--      PATH           DONE VERB                DISCUSSION
--      /job              ✓ GET                 list jobs matching search criteria; for regular users only
--                                              the munge-signed uid+gid are valid
--                                                  owner_uid=<owner-uid>{,<owner-uid>…}
--                                                  account=<workgroup>{,<workgroup>…}
--                                                  gid=<gid-number>{,<gid-number>…}
--                                                  project_id=<proj-id>{,<proj-id>…}
--                                                  allocation_id=<alloc-id>{,<alloc-id>…}
--                                                  status=(completed|executing){,(completed|executing)…}
--                        ✓ POST                add a job <job submission plugin>
--                          PUT                 n/a
--                          DELETE              n/a
--      /job/#            ✓ GET                 information for a specific job; regular users only have
--                                              access to their own jobs
--                          POST                n/a
--                        ✓ PUT                 update a job <job completion plugin>
--                          DELETE              n/a
--     /job/resolve         POST                request that a list of jobs be moved from completed pre-debit
--                                              status to per-user debit status.  The POSTed content should be
--                                              a JSON array of activity_id values.  The API returns a JSON array
--                                              of activity_id values that were not successfully resolved (an empty
--                                              array if fully successful)
--      /failure          ✓ GET                 list failures matching search criteria; for regular users only
--                                              the munge-signed uid+gid are valid
--                                                  owner_uid=<owner-uid>{,<owner-uid>…}
--                                                  account=<workgroup>{,<workgroup>…}
--                                                  gid=<gid-number>{,<gid-number>…}
--                                                  project_id=<proj-id>{,<proj-id>…}
--                                                  allocation_id=<alloc-id>{,<alloc-id>…}
--                                                  job_id=<job-id>{,<job-id>…}
--                          POST                n/a
--                          PUT                 n/a
--                          DELETE              n/a
--     /project           ✓ GET                 list projects matching search criteria; for regular users only
--                                              the munge-signed uid+gid are valid
--                                                  uid=<uid-number>{,<uid-number>…}
--                                                  account=<workgroup>{,<workgroup>…}
--                                                  gid=<gid-number>{,<gid-number>…}
--                                                  project_id=<proj-id>{,<proj-id>…}
--                        ✓ POST                create a new project: project_id, gid, gname, account
--                          PUT                 n/a
--                          DELETE              n/a
--     /project/#         ✓ GET                 information for a specific project, including allocations
--                          POST                n/a
--                          PUT                 n/a
--                          DELETE              n/a
--     /alloc             ✓ GET                 list allocations matching search criteria; for regular users only
--                                              the munge-signed uid+gid are value
--                                                  uid=<uid-number>{,<uid-number>…}
--                                                  account=<workgroup>{,<workgroup>…}
--                                                  gid=<gid-number>{,<gid-number>…}
--                                                  project_id=<proj-id>{,<proj-id>…}
--                                                  allocation_id=<alloc-id>{,<alloc-id>…}
--                        ✓ POST                create a new allocation for a project: project_id, category,
--                                              resource, start_date, end_date, credit
--                          PUT                 n/a
--                          DELETE              n/a
--     /alloc/#           ✓ GET                 information for a specific allocation, including balances
--                                                  report=(aggregate|detail)
--                          POST                n/a
--                          PUT                 n/a
--                          DELETE              n/a
--     /alloc/#/credit    ✓ GET                 total credits for allocation (response = bare integer)
--                        ✓ POST                add a credit to an allocation (payload = bare integer OR JSON)
--                          PUT                 n/a
--                          DELETE              n/a
--     /alloc/#/debit     ✓ GET                 total debits for allocation (response = bare integer)
--                        ✓ POST                add a debit to an allocation (payload = bare integer OR JSON)
--                          PUT                 n/a
--                          DELETE              n/a
--
-- job = {
--           "job_id": <job-id>,
--           "owner_uid": <uid-number>,
--           "status": "executing|completed",
--           "allocation_id": <allocation-id>,
--           "amount": <su-count>,
--           "creation_date": "<timestamp>",
--           "modification_date": "<timestamp>",
--     [optional, available for POST only iff "allocation_id" is absent]
--           "account": "<workgroup>",
--           "resource": "cpu|gpu",
--     }
--
-- project = {
--           "project_id": <project-id>,
--           "gid": <gid-number>,
--           "gname": "<gid-name>",
--           "account": "<workgroup>",
--           "creation_date": "<timestamp>",
--           "modification_date": "<timestamp>",
--     [optional, available on GET /project/# only]
--           "allocations": [ <allocation-id>, <allocation-id>, ... ],
--     }
-- allocation = {
--           "allocation_id": <allocation-id>,
--           "project_id": <project-id>,
--           "category": "startup|education|research|explore|discover|accelerate",
--           "resource": "cpu|gpu",
--           "start_date": "<timestamp>",
--           "end_date": "<timestamp>",
--           "credit": <su-count>,
--           "predebit": <su-count>,
--           "debit": <su-count>,
--           "balance": <su-count>,
--           "creation_date": "<timestamp>",
--           "modification_date": "<timestamp>",
--     }
--     
--
-- For the sake of /alloc/#/credit and /alloc/#/debit, JSON format:
--
-- activity = {
--           "amount": <su-count>,
--           "comments": "<text>",
--   }
--
