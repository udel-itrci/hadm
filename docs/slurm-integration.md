# Hermes Allocations Data Manager: Slurm Integration

The HADM system consists of a PostgreSQL database and a REST API that provides access to that database.  For Slurm to use HADM to keep track of jobs' consumption of allocations, plugins must be leveraged to alter Slurm's behavior.  

## Design 1: job submission and completion

The original design scheme looked like this:

  - cli\_filter
    - check for necessary paramters on the client side (e.g. one partition explicitly specified, submitted from known workgroup)
  - job\_submit
    - check for necessary paramters on the server side (can't trust the client side)
    - calculate projected SU usage for job
    - attempt to pre-debit the SU usage in HADM, notify user if balance insufficient
  - job\_completion
    - calculate actual SU usage for job
    - update pre-debited SU usage with real usage, mark job completed in HADM

The first issue encountered was that while the `job_descriptor` record that Slurm provides to a job\_submit plugin contains a `tres_req_cnt` field, it is not allocated and filled-in until *after* the job\_submit plugin has processed the record.  In fact, a number of fields of the `job_descriptor` are not touched until after the plugin has processed it.  A large amount of slurmctld internal code was copied and pasted into the job\_submit plugin to yield the `tres_req_cnt` array and adjust it according to partition and QOS overrides (our requirement of submitting against a single partition makes this feasible).

Once the `tres_req_cnt` array was produced and the partition-provided `tres_billing_weights` applied, the resulting billing value could be multiplied by the maximum wall time (in minutes) to produce a projected SU usage for the job.  It was this value that would be sent to HADM with the account, RDR type (cpu or gpu), job id, and owner uid to attempt to create a pre-debit.

There was a major problem:  one other field of the `job_descriptor` that is not filled-in when the job\_submit plugin executes is the `job_id`.  The internal `get_next_job_id()` function in slurmctld does have a `test_only` argument that (when `true`) yields a "look ahead" at the next job id in sequence, but without actually assigning a job id there could be a number of conditions that would see a different job's receiving that job id.  Producing a hash of the job parameters would be problematic since so many of the parameters will be alike between submissions; even the submission time, limited to a resolution of 1 second, has the possibility of not varying between two submissions.  And if Slurm altered any of the parameters — scheduling to the lower end of the node count range — the hash could be different once the job\_completion plugin attempts to update SU usage with HADM.

Without an immutable identifier for the job, it is impossible for a job\_submit plugin to add a pre-debit record to HADM.

## Design 2: prolog/epilog (PrEp)

The original design was altered:

  - cli\_filter
    - check for necessary paramters on the client side (e.g. one partition explicitly specified, submitted from known workgroup)
  - job\_submit
    - check for necessary paramters on the server side (can't trust the client side)
  - PrEp
    - prolog
      - if job uses an allocation-oriented partition
        - calculate actual SU usage for job
        - attempt to pre-debit the SU usage in HADM
    - epilog (for allocation-oriented partitions)
      - if job used an allocation-oriented partition
        - calculate actual SU usage for job
        - update pre-debited SU usage with real usage, mark job completed in HADM

Two major pros to this design are:

  - since the PrEp callbacks receive the final `job_record` data structure, the job id and (most importantly) `billable_tres` fields are present
  - since the PrEp callbacks execute *after* the job has been assigned to a partition and it is about to execute, submitting jobs against multiple partitions is permissible **if** the cli\_filter and job\_submit plugins allow it
    - as of this time, the plugins still require a single partition for the sake of enforcing GPU sub-types on the job (replace `--gpus=2` with `--gpus=tesla_v100:2` on partition `gpu-v100`)

There turned out to be two cons to this design.  First and foremost, the PrEp plugin has no way to communicate failure information back to the user via Slurm.  The error code communicated back to slurmctld by the PrEp layer is not retained.  Setting the `state_reason` string works *while the job record is visible via scontrol*, but once the job record shifts to the accounting database that information is lost.  The second major issue is that a PrEp prolog callback that returns an error to slurmctld does **not** cause the job to be cancelled:  the job is requeued and retried indefinitely.  This could easily lead to a denial-of-service condition:  imagine a workgroup that has exceeded its allocation continuing to submit hundreds of jobs.  The jobs remain in the queue and are continually rescheduled and have the PrEp prolog executed only to deny their execution.  As time goes by, the scheduling priority increases and the scheduler spends all its time attempting to run these jobs — to the detriment of jobs that *do* have enough allocation credits to execute.

After analyzing the slurmctld code, there is literally no way for a PrEp prolog callback to communicate back to slurmctld that the job should be cancelled and NOT requeued.  This seems like a major shortcoming to this particular plugin.  To prevent the proliferation of jobs that will not execute, the slurmctld code had to be modified:

  - the PrEp slurmctld prolog function signals an allocation deficiency by returning `ESLURM_ACCOUNTING_POLICY` to the PrEp framework
  - the `prep_prolog_slurmctld_callback()` function (`src/slurmctld/prep_slurmctld.c`) was modified to react to the `ESLURM_ACCOUNTING_POLICY` error by killing the job instead of requeing it

Ideally, the `prep_prolog_slurmctld_callback()` async callback in slurmctld should be modified to include a flag that indicates the job should not be requeued.  Even better would be if an error string could be returned and retained through the rest of the job's life cycle (into the accounting database and completion log).

This design in conjuction with the changes to slurmctld is working well in practice.  However, one enormous problem remains:  the end user receives no information as to why their job failed to execute.

### Logging failures

To address the lack of failure explanations returned by Slurm, the HADM pipeline itself is leveraged.  An additional activity of type `failure` was added to the database; when a pre-debit against the REST API is attempted and fails (due to insufficient credit), the REST API adds a tuple to the `failure` table.  The `sproject` utility can be used to explore the `failure` table, searching by specific job id if necessary.

## Configuration

The plugin is enabled **in addition to the default script plugin**.  If the `script` plugin is not included, no SPANK plugins will receive prolog/epilog callbacks (and that's how our tmpdir plugin works — we found out the hard way when we left `script` out of the config!).

```
PrEpPlugins=script,hadm
PrEpParameters="interval.maintenance=900 threads.max=13 threads.min=3"
```

The available plugin configuration parameters:

|Parameter|Discussion|
|---------|----------|
|timeout.connect|Timeout (in seconds) for initial CURL TCP connect for a API transaction|
|timeout.request|Timeout (in seconds) for the completion of a CURL API transaction (once connected)|
|threads.min|The minimum number of logging threads that should be present to process the job data queue|
|threads.max|The maximum number of logging threads that may be used to process the job data queue|
|lifespan.max\_requests|A logging thread should be terminated once it has processed this many job data records|
|lifespan.max\_time|A logging thread should be terminated after it has been online for this amount of time (in seconds)|
|interval.wait\_threshold|Once the statistical average wait time in the queue exceeds this value (in seconds) additional threads will be spawned (if possible)|
|interval.maintenance|The scheduled interval (in seconds) that the maintenance thread will wake and cleanup threads that have reached their lifespan and spawn new threads if necessary|
|rest.base_url|The base URL for HADM REST API requests|

Defaults are configured when the PrEp plugin is compiled via CMake options.