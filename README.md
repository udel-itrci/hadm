# Hermes Allocation Data Manager

The Hermes Allocation Data Manager (HADM) is a suite of tools used to implement a SU credit/debit accounting system external to but integrated with the Slurm job scheduler.

The HADM acronym is pronounced like "hate-em" — the tasks behind managing and tracking usage of allocations are really no fun so we tend to hate 'em.

## Allocations

An allocation-based computing environment identifies types of resources and the amount of those resources available in a given time period.  Consider a cluster of 10 nodes, each containing 128 GiB of RAM and 2 x 32C processors (memory and CPU sockets/cores are typically the resources of interest in cluster nodes).  In a one year period, the nodes provide (640 core)•(8760 hour) or 5606400 core•hour.  Over the same period, the nodes provide (1280 GiB)•(8760 hour) or 11212800 GiB•hour.  The amount of resource provided depends upon the choice of resource, and the resource identity is reflected in the unit.  For XSEDE, the amount is always presented with the **SU** unit, where **SU** is understood to stand-in for a *resource*•hour unit.

In a single *allocation period* the total SU for each resource can be distributed to one or more entities:  XSEDE calls these entities **projects** while on our systems we have used the term **workgroups**.  In this document the term **project** will be used interchangeably (they both are implemented as Unix groups and Slurm accounts).

1. In a given allocation period a project is allowed at most one allocation per resource (a **resource allocation**).
2. A resource allocation can have one or more SU **credits** associated with it.
3. The predicted SU amount for work creates a **pre-debit** against the resource allocation.  Work will be performed only if the resource allocation has sufficient credit versus the pre-debit amount.
4. When work is completed, the predicted pre-debit SU amount is corrected with the actual amount.
5. Completed work is periodically aggregated to a final **debit** against the resource allocation.

An online system to manage and track allocations must comprise the following components:

- Information storage/retrieval system to represent the projects, allocations, resource allocations, and transactions against resource allocation budget.
- User-facing mechanisms to query the information (e.g. most importantly, allocation balances).
- Sysadmin-facing mechanisms to manage the information.
- Hooks into job scheduling software to create (job submission or launch) and update (job completion) pre-debits against a resource allocation.

## Information storage

In its current incarnation, HADM uses a PostgreSQL database to store and retrieve information.  The [database schema](docs/hadm-db_schema.pgsql) makes use of enumerated types, triggers, and procedures to embed some of the logic within the database (versus having to implement it externally and repeatedly).

We chose to leverage an official PostgreSQL container (version 13.2 in this incarnation) running as a database server via Singularity with database storage bind-mounted into the container.  The Singularity container defintion looks like this:

```
Bootstrap: docker
From: postgres:13.2
Includecmd: no

%startscript
    /docker-entrypoint.sh postgres $INSTANCE_ARGS

%apprun client
    psql --username=allocations allocations

%apprun dumpall
    pg_dumpall --username=allocations

%apprun dump
    pg_dump --username=allocations allocations

%environment
    POSTGRES_DB="allocations"
    POSTGRES_USER="allocations"
    POSTGRES_PASSWORD="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    POSTGRES_HOST_AUTH_METHOD="md5"
    POSTGRES_INITDB_ARGS="--auth-host=md5"
    export POSTGRES_DB POSTGRES_USER POSTGRES_PASSWORD POSTGRES_HOST_AUTH_METHOD POSTGRES_INITDB_ARGS

```

Singularity 3.x introduced the notion of *applications* — aliases to canned commands that can be run inside a container similar to the way the runscript functions in bootstrapping a service inside a container.  We included applications to open a Postgres client to the database inside the container and to dump the database (for creating backups, etc.).  The environment section can be leveraged to alter the database listening port, though that can also be handled by creating a port-forwarding of an alternate host port to the standard Postgres port (5432) inside the container.

A ZFS volume was created on the cluster management node and mounted at `/var/lib/postgresql-containers` to hold the container image(s) and PostgreSQL data for each container.  For the HADM system, the `/var/lib/postgresql-containers/allocations` sub-directory was created:

```
drwx------ 19 root    root      4096 Jul 26 16:05 data
-rw-r--r--  1 root    root       632 May 10 13:14 image.def
drwxr-xr-x  2 root    root        48 May 10 12:56 root
drwxr-xr-x  3 root    root        24 May 10 11:57 run
```

The container was built therein (the [Singularity image definition file](docs/postgresql-container.def) was copied to `/var/lib/postgresql-containers/allocations/image.def`):

```
$ singularity build /var/lib/postgresql-containers/allocations/image.{sif,def}
$ ls /var/lib/postgresql-containers/allocations
drwx------ 19 root    root      4096 Jul 26 16:05 data
-rw-r--r--  1 root    root       632 May 10 13:14 image.def
-rwxr-xr-x  1 root    root 108969984 May 10 13:15 image.sif
drwxr-xr-x  2 root    root        48 May 10 12:56 root
drwxr-xr-x  3 root    root        24 May 10 11:57 run
```

1. The database schema was copied to the `root` subdirectory, which will appear as `/root` in the container.
2. The [service maintenance script](src/sbin/darwin-postgresql-container.sh) was copied to `/var/lib/postgresql-containers`.
3. The [templated database service](systemd/darwin-postgresql-container@.service) and [associated ZFS mount](systemd/var-lib-postgresql\x2dcontainers.mount) files were copied to `/etc/systemd/system`
4. A `systemctl daemon-reload` was effected.

At this point, the HADM database can be started (as root):

```
$ systemctl start darwin-postgresql-container@allocations.service
```

On success, the instance identifier will be `postgresql-allocations` and a client can be run inside the container:

```
$ singularity run --app client instance://postgresql-allocations
```

In the psql client, the schema can be loaded from the file in `/root` to provision the database.

At this point, the instance can be shutdown (`systemctl stop darwin-postgresql-container@allocations.service`) and the configuration files in `/var/lib/postgresql-containers/allocations/data` can be edited to add access restrictions, etc.  When the instance is restarted it will use the altered configuration — or exit in error if you made a mistake.

## Information retrieval

It would be possible to add users/roles to the database to allow the other software components to connect directly to the PostgreSQL database and only access specific functionalities:  the user-facing utilities would need a read-only role, for example.  One caveat would be in authorizing specific tuples in some tables to specific projects/users on the system:  project X should not be able to see the allocation balances and jobs associated with project Y.  This level of authorization is outside the scope of the PostgreSQL database.

The cluster already has a strong authentication mechanism in-use by Slurm:  munged.  In short, munged signs a chunk of data and includes the uid/gid used to request the signature in its product and hash by use of a preshared key.  Another agent can verify the identify of the product and decode the payload using that same key.  This establishes the Unix uid/gid identity of the requestor.

A REST API was constructed to provide access-controlled querying of the HADM database.  Each request must have an HTTP header whose value is the requested URL signed by munged.  As long as the decoded payload matches the requested URL, the request is considered authenticated and originated by the attached Unix uid/gid.  The uid/gid can then be used to perform authorization checks.

The API is [summarized elsewhere](docs/rest-api.md) and includes routes for

- Querying and managing projects present in the system
- Querying and managing resource allocations associated with those projects
- Querying and managing credits to a resource allocation
- Querying and managing pre-debit work (**jobs**) associated with a resource allocation
- Resolving pre-debit work to the full debit state

The API is implemented with Flask and runs in an Anaconda virtualenv (see the [server script](src/rest-server/rest-server.py) and [virtualenv creation script](src/rest-server/venv-create.sh)) on the same management node as the master `slurmctld` process to keep communications between those agents as fast as possible; the PostgreSQL database runs on a different management node at this time.

### Curl-wrapper for low-level API requests

The action of munged-signing the request URL and adding the HTTP header is wrapped around the `curl` command by the [hadm-curl script](src/bin/hadm-curl).  This command accepts API routes so use is contingent on knowledge of the structure of the API.  Primarily for expert interaction with the raw API.

## User-facing software

The [sproject command](src/bin/sproject) and the [script it calls](src/sproject.py) implement a textual user interface that queries and consumes data from the REST API.  The user's intentions are communicated with command line arguments and the script constructs the appropriate API routes.

```
$ sproject --help
usage: sproject.py [-h] [--rest-url <base-url>] [--format <output-format>]
                   [--numeric] [--exact]
                   {allocations,projects,jobs,failures} ...

Interface for querying DARWIN allocations.

optional arguments:
  -h, --help            show this help message and exit
  --rest-url <base-url>, -U <base-url>
                        Base URL of the REST API to be queried
  --format <output-format>, -f <output-format>
                        Controls the formatting of the program output (table,
                        csv, json, default is table)
  --numeric, -n         Do not resolve numeric values to textual forms (e.g.
                        user ids, allocation ids)
  --exact, -x           Show SU values in full-precision (minutes, not hours)

data type to query:
  {allocations,projects,jobs,failures}
    allocations         Allocation properties and balances
    projects            Project properties
    jobs                Submitted and running job properties
    failures            Properties of jobs that failed due to allocation
                        limits
```

By default all output is presented in tabular fashion (`--format=table`), but CSV and JSON output formats are also available:

The appropriate REST API URL is built into the script, so the `--rest-url` is not typically used.  The `--numeric` flag causes the utility to not map numerical user ids, allocation ids, etc., to their associated textual form (which can increase the overall performance if the textual forms are unnecessary).

The HADM system internally stores SU amounts in an integer number with an understoor *resource*•minute unit, whereas XSEDE specifically reports in hour-based units.  Using minute-based units increases the overall precision of the system and prevents extreme roundoff issues:  e.g. a 1 CPU job running for 01:05 might be billed as 2 CPU•hour in hour-based units, but would be billed as 65 CPU•minute.  By default, `sproject` displays SU amounts in hour-based units; the `--exact` flag uses minute-based units.

The nature of the data to be displayed is indicated by the sub-command to `sproject`.  Each sub-command has its own set of flags that can be used to filter the data.  For example, the `jobs` sub-command:

```
$ sproject jobs --help
usage: sproject.py jobs [-h]
                        [--sort {activity_id,allocation_id,creation_date,job_id,modification_date,status,owner_uid}]
                        [--descending] [--activity-id <activity-id>]
                        [--allocation-id <alloc-id>]
                        [--project-id <project-id>] [--project <project>]
                        [--uid <username|uid-number>]
                        [--gid <groupname|gid-number>]

optional arguments:
  -h, --help            show this help message and exit
  --sort {activity_id,allocation_id,creation_date,job_id,modification_date,status,owner_uid}
                        Sort the data by a specific column prior to display
  --descending          Sort the data in descending rather than ascending
                        order
  --activity-id <activity-id>
                        Select one or more specific debit/credit activities
  --allocation-id <alloc-id>
                        Select one or more specific allocation ids
  --project-id <project-id>
                        Select one or more specific project ids
  --project <project>, --workgroup <project>, -w <project>
                        Select one or more specific named projects/workgroups
                        in the query
  --uid <username|uid-number>, -u <username|uid-number>, --owner-uid <username|uid-number>
                        Select one or more specific users in the query
                        (requires additional privileges)
  --gid <groupname|gid-number>, -g <groupname|gid-number>
                        Select one or more specific Unix groups in the query
```

Note that the position of flags is significant in `sproject`; flags for the sub-command **must** appear after the sub-command.  Without any flags, the `jobs` sub-command displays pre-debit activities against allocations to which the user is a member (or, for the special user classes **superuser** and **admin**, the REST API will return all data):

```
$ sproject jobs
Activity id Alloc id Alloc descr  Job id Owner   Status    Amount Modification date         Creation date            
----------- -------- ------------ ------ ------- --------- ------ ------------------------- -------------------------
    3987088       94 xxxx::gpu     86426 xxxxx   executing     -1                           2021-08-06 15:31:20-04:00
    3987089       99 XXXXXXX::cpu  86427 xxxxxxx completed      0 2021-08-06 15:45:10-04:00 2021-08-06 15:45:08-04:00
    3987090       94 xxxx::gpu     86428 xxxxx   executing     -1                           2021-08-06 15:52:58-04:00
    3987063       94 xxxx::gpu     86410 xxxxx   executing   -260                           2021-08-06 11:27:45-04:00
```

or in the more-precise minute-based amounts:

```
$ sproject --exact jobs
Activity id Alloc id Alloc descr  Job id Owner   Status    Amount Modification date         Creation date            
----------- -------- ------------ ------ ------- --------- ------ ------------------------- -------------------------
    3987088       94 xxxx::gpu     86426 xxxxx   executing    -30                           2021-08-06 15:31:20-04:00
    3987089       99 xxxxxxx::cpu  86427 xxxxxxx completed     -2 2021-08-06 15:45:10-04:00 2021-08-06 15:45:08-04:00
    3987090       94 xxxx::gpu     86428 xxxxx   executing    -30                           2021-08-06 15:52:58-04:00
    3987063       94 xxxx::gpu     86410 xxxxx   executing -15600                           2021-08-06 11:27:45-04:00
```

Obtaining accounting information for an allocation is another important task:

```
$ sproject --exact allocations --allocation-id=94 --detail --by-user
Project id Alloc id Alloc descr Category RDR User  Transaction Amount Comments      
---------- -------- ----------- -------- --- ----- ----------- ------ --------------
        21       94 xxxx::gpu   startup  gpu xxxxx run+complt  -15660               
                 94 xxxx::gpu                xxxxx debit        -1516               
                 94 xxxx::gpu                      credit       24000 Initial credit
```

This "gpu" resource allocation has a single credit of 24000 GPU•minute and user "xxxxx" has 15660 GPU•minute worth of jobs that are currently running or have completed, and 1516 GPU•minute of jobs that have been logged and resolved to full debit status.  A more compact detail view without per-user data (and in GPU•hour units):

```
$ sproject allocations --allocation-id=94 --detail
Project id Alloc id Alloc descr Category RDR Credit Run+Cmplt Debit Balance
---------- -------- ----------- -------- --- ------ --------- ----- -------
        21       94 xxxx::gpu   startup  gpu    400      -261   -25     114
```

clearly shows the remaining credit on the resource allocation:  114 GPU•hour.

## Sysadmin-facing software

The aforementioned `hadm-curl` command can be used either directly or in scripts to manage the HADM data.  Typical tasks include:

- Creating new projects
- Creating new resource allocations for a project
- Funding (crediting) a resource allocation
- Adjusting a resource allocation (debits or credits)
- Resolving completed jobs

Each task requires a JSON payload to be sent to the REST API:

```
$ hadm-curl --json --request POST \
    --data '{"account":"it_nss","gid":1001,"gname":"it_nss","project_id":1}' \
    /project
```

A successful request will return the affected entity as a JSON object.  Errors generated by the REST API itself are returned as a JSON error object:

```
{"err_msg":"There was an error....","err_code": -1}
```

Additional details are [available in the API documentation](docs/rest-api.md).

### Resolving completed jobs

Each job added to the HADM data store exists as a unique tuple in the `predebits` table.  Over time, the number of tuples will increase.  There are two options:

1. Leave the `predebit` tuples in the database indefinitely, yielding lengthy job listings from `sproject` and larger database indices, etc.
2. *Resolve* completed jobs:  aggregate qualifying `predebit` records into the per-user `debit` records

The second option keeps the size of the database under control (and produces less output from `sproject` job commands).  For XSEDE projects the SP is responsible for providing allocation usage information to XSEDE (via AMIE) so that process nests nicely with resolving completed jobs in HADM:

- Completed jobs are eligible for communication to XSEDE via AMIE
- Job usage acknowledged as received by AMIE can be resolved in HADM
- Job usage not acknowledged as received by AMIE can be tried again later

For non-XSEDE projects (projects with accounts not prefixed by **xg-**) the procedure is even simpler:  completed jobs are resolved in HADM periodically.  The [DSI reaper](src/sbin/hadm-dsi-reaper) script and [systemd timer-based service](systemd/hadm-dsi-reaper.timer) is our implementation of that function for non-XSEDE projects on DARWIN.

## Job scheduling integration

The DARWIN cluster uses the Slurm job scheduler.  As detailed in the [Slurm design document](docs/slurm-integration.md), a PrEp (Prolog-Epilog) plugin is used to add a predebit (if sufficient credit exists) in the prolog and to update the predebit amount with actual usage and set the job as completed in the epilog.  The [hadm_prep](src/hadm_prep) plugin uses a sophisticated threaded work queue to process the necessary REST HTTP requests asynchronously (preventing deadlocks of `slurmctld`).  The base URL for the REST API is used to construct the routes, and since our Slurm already uses munged for authentication it is straightforward to construct the necessary authentication header for each request.  The Slurm user (e.g. root) needs to be granted the sysadmin role in the REST API, and (naturally) no firewall blocks must prevent access to the REST API from any `slurmctld` instance.  All PrEp queries will originate from the `slurmctld` nodes alone (no `slurmd` instances will use the `hadm_prep` plugin functionality).