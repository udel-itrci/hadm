if [ -z "$1" ]; then
    echo "ERROR:  please provide Slurm version to build against"
    return 1
fi

SLURM_BUILD="$(find /opt/shared/slurm/${1}/src -maxdepth 1 -type d -name build-\* | sort | head -1)"

cmake -DCMAKE_BUILD_TYPE=Release \
	-DMUNGE_ROOT=/opt/shared/munge \
	-DSLURM_ROOT=/opt/shared/slurm/"$1" \
	-DSLURM_SRC_ROOT="/opt/shared/slurm/${1}/src" \
	-DSLURM_BUILD_ROOT="$SLURM_BUILD" \
	..
