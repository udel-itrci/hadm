

#include "darwin_config.h"
#include "partitions.h"

/*
 * These variables are required by the generic plugin interface.  If they
 * are not found in the plugin, the plugin loader will ignore it.
 *
 * plugin_name - a string giving a human-readable description of the
 * plugin.  There is no maximum length, but the symbol must refer to
 * a valid string.
 *
 * plugin_type - a string suggesting the type of the plugin or its
 * applicability to a particular form of data or method of data handling.
 * If the low-level plugin API is used, the contents of this string are
 * unimportant and may be anything.  SLURM uses the higher-level plugin
 * interface which requires this string to be of the form
 *
 *      <application>/<method>
 *
 * where <application> is a description of the intended application of
 * the plugin (e.g., "auth" for SLURM authentication) and <method> is a
 * description of how this plugin satisfies that application.  SLURM will
 * only load authentication plugins if the plugin_type string has a prefix
 * of "auth/".
 *
 * plugin_version - an unsigned 32-bit integer containing the Slurm version
 * (major.minor.micro combined into a single number).
 */
const char plugin_name[]        = "HADM cli filter plugin";
const char plugin_type[]        = "cli_filter/hadm";
const uint32_t plugin_version   = SLURM_VERSION_NUMBER;

/**/

extern const char*
__darwin_cli_filter_get_option(
    slurm_opt_t     *opt,
    const char      *option_name,
    const char      *option_envvar
)
{
    const char      *out_value = NULL;
    
    if ( option_name ) {
        out_value = slurm_option_get(opt, option_name);
    }
    if ( ! out_value && option_envvar ) {
        out_value = getenv(option_envvar);
    }
    return out_value;
}

/**/

extern int
cli_filter_p_setup_defaults(
    slurm_opt_t *opt,
    bool        early
)
{
    int         rc = SLURM_SUCCESS;
    char        *current_groupname = NULL;
    gid_t       current_gidnumber = getgid();
    
    /*
     * 0. Current gid is a workgroup?
     */
    if ( current_gidnumber < DARWIN_GID_LOW || current_gidnumber > DARWIN_GID_HIGH ) {
        error("Please choose a workgroup before submitting a job");
        return SLURM_ERROR;
    }
    
    current_groupname = gid_to_string_or_null(current_gidnumber);
    if ( ! current_groupname ) {
        error("getgrid(%d) failed (errno = %d)", current_gidnumber, errno);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /*
     * 1. Default account on submission should match the workgroup.
     */
    rc = slurm_option_set(opt, "account", current_groupname, early);
    if ( rc != SLURM_SUCCESS ) {
        error("failed to set default Slurm account `%s` (rc = %d)", current_groupname, rc);
    } else {
        debug("set default --account=%s", current_groupname);
    }
    
early_exit:
    if ( current_groupname ) xfree(current_groupname);
    return rc;
}

extern int
cli_filter_p_pre_submit(
    slurm_opt_t *opt,
    int         offset
)
{
    static char*                gpu_flag_opt_list[] = { "gpus", "gpus-per-node", "gpus-per-socket", "gpus-per-task" };
    static char*                gpu_flag_env_list[] = { "SLURM_GPUS", "SLURM_GPUS_PER_NODE", "SLURM_GPUS_PER_SOCKET", "SLURM_GPUS_PER_TASK" };
    static int                  gpu_flag_list_len = sizeof(gpu_flag_opt_list) / sizeof(char*);

    int                         rc = SLURM_SUCCESS, i;
    const char                  *value;
    char                        *current_groupname = NULL;
    darwin_partition_info_t     *partition_info = NULL;
    darwin_partition_type_t     partition_type = darwin_partition_type_unknown;
    int                         partition_count = 0;
    gid_t                       current_gidnumber = getgid();
    bool                        has_gpu_request = false;
    
    /*
     * 0. Current gid is a workgroup?
     */
    if ( current_gidnumber < DARWIN_GID_LOW || current_gidnumber > DARWIN_GID_HIGH ) {
        error("Please choose a workgroup before submitting a job");
        return SLURM_ERROR;
    }
    
    current_groupname = gid_to_string_or_null(current_gidnumber);
    if ( ! current_groupname ) {
        error("getgrid(%d) failed (errno = %d)", current_gidnumber, errno);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /*
     * 1. --account provided?
     */
    value = __darwin_cli_filter_get_option(opt, "account", "SLURM_JOB_ACCOUNT");
    if ( ! value ) {
        /*
         * Unclear on how the default value could be discarded, but let's
         * guard against it anyway.
         */
    
        /* Set the account = current group name */
        rc = slurm_option_set(opt, "account", (value = current_groupname), false);
        
        if ( rc != SLURM_SUCCESS ) {
            error("failed to set Slurm account to current group `%s` (rc = %d)", current_groupname, rc);
            goto early_exit;
        } else {
            debug("set --account=%s", current_groupname);
        }
    }
    else if ( strcmp(value, current_groupname) != 0 ) {
        /*
         * The user provided a value that doesn't match!
         */
        error("You provided --account=%s and you are running as workgroup %s.  Please omit --account or ensure you are running in the appropriate workgroup.", value, current_groupname);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /*
     * 2. Now that we have an account name (pointed-to by 'value'), is it a
     *    short-term allocation?
     */
    if ( strncasecmp(value, "st_", 3) == 0 ) {
        /*
         * Force the partition and reservation for the job and be done with it:
         */
        rc = slurm_option_set(opt, "partition", value, false);
        if ( rc != SLURM_SUCCESS ) {
            error("failed to set Slurm partition `%s` (rc = %d)", value, rc);
            goto early_exit;
        } else {
            debug("set --partition=%s", value);
        }
        
        rc = slurm_option_set(opt, "reservation", value, false);
        if ( rc != SLURM_SUCCESS ) {
            error("failed to set Slurm reservation `%s` (rc = %d)", value, rc);
            goto early_exit;
        } else {
            debug("set --reservation=%s", value);
        }
        
        goto early_exit;
    }
    
    /*
     * 3. --partition provided?
     */
    value = __darwin_cli_filter_get_option(opt, "partition", "SLURM_JOB_PARTITION");
    if ( ! value ) {
        /*
         * Jobs MUST be submitted with an explicit partition requested
         */
        error("All jobs must explicitly request a partition");
        rc = SLURM_ERROR;
        goto early_exit;
    } else {
        /*
         * We need to iterate of all values in the list.  Make sure each value is
         * a known DARWIN partition and all types are the same.
         */
        const char          *partition_list_base = value,
                            *partition_list_comma =  NULL;
        
        while ( *partition_list_base ) {
            /* Get rid of leading comma(s): */
            while ( *partition_list_base == ',' ) partition_list_base++;
            if ( *partition_list_base ) {
                darwin_partition_type_t parsed_part_type;
                size_t                  part_name_len;
                
                /* Find the next comma or the NUL terminator: */
                partition_list_comma = strchrnul(partition_list_base, ',');
                /* Calculate length of the sub-string: */
                part_name_len = (partition_list_comma - partition_list_base);
                /* Parse the partition name => type: */
                partition_info = darwin_partition_info_for_name(partition_list_base, part_name_len);
                if ( partition_info ) {
                    /* Any unknown partitions will be skipped over: */
                    if ( partition_type == darwin_partition_type_unknown ) {
                        /* First pass, just note the type: */
                        partition_type = partition_info->partition_type;
                    } else if ( partition_info->partition_type != partition_type ) {
                        error("Jobs may not request multiple partition types (e.g. CPU and GPU)");
                        rc = SLURM_ERROR;
                        goto early_exit;
                    }
                }
                /* That's another partition in the list.... */
                partition_count++;
                partition_list_base = partition_list_comma;
            }
        }
    }
    
    if ( partition_count > 1 ) {
        error("Jobs may request a single partition only (%d requested by this job)", partition_count);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /* Was it a known partition? */
    if ( partition_info ) {
        /*
         * 4. --gres provided?
         */
        value = __darwin_cli_filter_get_option(opt, "gres", NULL);
        if ( value ) {
            /* Search for "gpu" in the list: */
            const char      *gres_list_base = value,
                            *gres_list_comma = NULL;
        
            while ( *gres_list_base ) {
                /* Get rid of leading comma(s): */
                while ( *gres_list_base == ',' ) gres_list_base++;
                if ( *gres_list_base ) {
                    size_t  gres_name_len;
                
                    /* Find the next comma or the NUL terminator: */
                    gres_list_comma = strchrnul(gres_list_base, ',');
                    /* Calculate the length of the sub-string: */
                    gres_name_len = (gres_list_comma - gres_list_base);
                    /* Is it a gpu request? */
                    if ( (gres_name_len >= 3) && (strncasecmp(gres_list_base, "gpu", 3) == 0) ) {
                        if ( (gres_name_len == 3) || (gres_list_base[3] == ':') ) {
                            error("GPU resource requests should use the --gpus or --gpus-per-<node,socket,task> option");
                            rc = SLURM_ERROR;
                            goto early_exit;
                        }
                        has_gpu_request = true;
                    }
                    gres_list_base = gres_list_comma;
                }
            }
        }
    
        /*
         * 5. For each GPU flag, check the format of the value matches <type>:<count> and
         *    if GPUs are needed, ensure the partition type is right
         */
        i = 0;
        while ( i < gpu_flag_list_len ) {
            value = __darwin_cli_filter_get_option(opt, gpu_flag_opt_list[i], gpu_flag_env_list[i]);
            if ( value ) {
                /* We require the <type>:<number> format; but first, make sure there's
                 * no comma -- we don't do multiple GPU types per job: */
                const char  *colon_ptr = strchrnul(value, ',');

                if ( colon_ptr && (*colon_ptr == ',') ) {
                    error("Only a single GPU type is allowed on DARWIN (multiple specified for --%s or %s)", gpu_flag_opt_list[i], gpu_flag_env_list[i]);
                    rc = SLURM_ERROR;
                    goto early_exit;
                }

                colon_ptr = strchrnul(value, ':');
                if ( *colon_ptr == '\0' ) {
                    /* It's a single value so by definition must be a count only */
#if defined(SHOULD_ADD_GPU_TYPES)
                    char            altered_gpu_flag[48 + (colon_ptr - value)];
            
                    if ( ! partition_info->gpu_type ) {
                        error("No GPU type could be inferred from the chosen partition");
                        rc = SLURM_ERROR;
                        goto early_exit;
                    }
                    if ( snprintf(altered_gpu_flag, sizeof(altered_gpu_flag), "%s:%s", partition_info->gpu_type, value) >= sizeof(altered_gpu_flag) ) {
                        error("Plugin error: DARWIN CLI filter: buffer overflow in typed-gpu-count template");
                        rc = SLURM_ERROR;
                        goto early_exit;
                    }
                    /* Override the flag value now: */
                    rc = slurm_option_set(opt, gpu_flag_opt_list[i], altered_gpu_flag, false);
                    if ( rc != SLURM_SUCCESS ) goto early_exit;
                    goto gpu_complete;
            
#elif defined(SHOULD_REQUIRE_GPU_TYPE)

                    error("The --%s option requires the '<type>:<count>' format:  no <type> specified", gpu_flag_list[i]);
                    rc = SLURM_ERROR;
                    goto early_exit;
            
#else
                
                    goto gpu_complete;
            
#endif
                }
                colon_ptr++;
                /* The rest of the string should be integer digits: */
                while ( isdigit(*colon_ptr) ) colon_ptr++;
                if ( *colon_ptr != '\0' ) {
                    error("The --%s option requires the '<type>:<count>' format: invalid count specified", gpu_flag_opt_list[i]);
                    rc = SLURM_ERROR;
                    goto early_exit;
                }
        
gpu_complete:
                has_gpu_request = true;
            
                /* We need GPUs, so is that our partition type? */
                if ( ! darwin_partition_type_is_gpu_capable(partition_type) ) {
                    error("Jobs requiring GPU resources must be submitted to a GPU-oriented partition");
                    rc = SLURM_ERROR;
                    goto early_exit;
                }
            }
            i++;
        }
    
        /*
         * If the partition is of GPU type, was a GPU requested?
         */
        if ( darwin_partition_type_is_gpu_capable(partition_type) && ! has_gpu_request ) {
            error("Jobs submitted to GPU-oriented partitions must request GPU resources.");
            rc = SLURM_ERROR;
            goto early_exit;
        }
        
        /*
         * 6. Make sure a QOS is set, default is 'allocation':
         */
        value = __darwin_cli_filter_get_option(opt, "qos", "SBATCH_QOS");
        if ( ! value || ! *value ) {
            rc = slurm_option_set(opt, "qos", "allocation", false);
            if ( rc != SLURM_SUCCESS ) goto early_exit;
        }
    }
    
    /* 
     * We don't want to set a minimum time limit here because we want slurmctld
     * to fill-in the default time limit before processing it.  Thus, we'll let
     * the job submission plug-in handle that.
     *
     * The same goes for minimum memory size -- just in case we end up doing a
     * setting in the QOS or something.  Let slurmctld fill it in for us first.
     */

early_exit:
    if ( current_groupname ) xfree(current_groupname);
    return rc;
}

extern int
cli_filter_p_post_submit(
    int         offset,
    uint32_t    jobid,
    uint32_t    stepid
)
{
    return SLURM_SUCCESS;
}

