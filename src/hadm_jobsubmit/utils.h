

#ifndef __UTILS_H__
#define __UTILS_H__

#include "darwin_config.h"

extern bool is_nonempty_str(const char *s);
extern bool str_in_list(const char *haystack, const char *needle, char separator, bool fold_case);
extern char* str_replace_in_list(const char *haystack, const char *needle, const char *replacement, char separator, bool fold_case);

#endif /* __UTILS_H__ */
