

#include "partitions.h"

/*!
 * @const __darwin_partition_info_map
 *
 * The list of all DARWIN partitions mapped to their type.  The
 * list must be sorted in ascending order by partition_name field
 * according to a strncasecmp() comparison operator -- basically
 * alphabetized.
 */
static darwin_partition_info_t __darwin_partition_info_map[] = {
        { .partition_name = "extended-mem", .partition_type = darwin_partition_type_cpu, .gpu_type = NULL, .rdr_type = darwin_rdr_cpu },
        { .partition_name = "gpu-mi100", .partition_type = darwin_partition_type_gpu, .gpu_type = "amd_mi100", .rdr_type = darwin_rdr_gpu },
        { .partition_name = "gpu-mi50", .partition_type = darwin_partition_type_gpu, .gpu_type = "amd_mi50", .rdr_type = darwin_rdr_gpu },
        { .partition_name = "gpu-t4", .partition_type = darwin_partition_type_gpu, .gpu_type = "tesla_t4", .rdr_type = darwin_rdr_gpu },
        { .partition_name = "gpu-v100", .partition_type = darwin_partition_type_gpu, .gpu_type = "tesla_v100", .rdr_type = darwin_rdr_gpu },
        { .partition_name = "large-mem", .partition_type = darwin_partition_type_cpu, .gpu_type = NULL, .rdr_type = darwin_rdr_cpu },
        { .partition_name = "standard", .partition_type = darwin_partition_type_cpu, .gpu_type = NULL, .rdr_type = darwin_rdr_cpu },
        { .partition_name = "xlarge-mem", .partition_type = darwin_partition_type_cpu, .gpu_type = NULL, .rdr_type = darwin_rdr_cpu }
    };

static darwin_partition_info_t __darwin_short_term_partition_info = {
        .partition_name = NULL, .partition_type = darwin_partition_type_shortterm, .gpu_type = NULL, .rdr_type = darwin_rdr_unknown
    };

/*!
 * @const __darwin_partition_info_map_len
 *
 * Number of records in the partition map list.
 */
static int __darwin_partition_info_map_len = sizeof(__darwin_partition_info_map) / sizeof(darwin_partition_info_t);

/*!
 * @typedef darwin_partition_info_map_bsearch_key_t
 *
 * The key passed to the bsearch() comparator includes the base pointer to
 * the name and the number of characters (since we're coming from a comma-separated
 * list of names and don't want to bother parsing or copying values).
 */
typedef struct {
    const char  *partition_name;
    size_t      partition_name_len;
} darwin_partition_info_map_bsearch_key_t;

/*!
 * @function __darwin_partition_info_map_bsearch_cmp
 *
 * The bsearch() comparator function for locating a partition name in the mapping
 * list.
 *
 * Per the API, returns 0 on a match, negative or positive integer value dependent
 * on the ordering of the key versus the map_record.
 */
static int
__darwin_partition_info_map_bsearch_cmp(
    const void  *key,
    const void  *map_record
)
{
    const darwin_partition_info_map_bsearch_key_t   *KEY = (darwin_partition_info_map_bsearch_key_t*)key;
    const darwin_partition_info_t                   *MAP_RECORD = (darwin_partition_info_t*)map_record;
    
    int     cmp_result = strncasecmp(KEY->partition_name, MAP_RECORD->partition_name, KEY->partition_name_len);
    
    if ( cmp_result == 0 ) cmp_result = MAP_RECORD->partition_name[KEY->partition_name_len];
    return cmp_result;
}

darwin_partition_info_t*
darwin_partition_info_for_name(
    const char  *partition_name,
    size_t      partition_name_len
)
{
    darwin_partition_info_map_bsearch_key_t key = { .partition_name = partition_name, .partition_name_len = (partition_name_len ? partition_name_len : strlen(partition_name)) };
    darwin_partition_info_t                 *map_record = NULL;
    
    map_record = (darwin_partition_info_t*)bsearch(&key, __darwin_partition_info_map, __darwin_partition_info_map_len, sizeof(darwin_partition_info_t), __darwin_partition_info_map_bsearch_cmp);
    
    if ( ! map_record && (partition_name_len > strlen("st_")) && (strncasecmp(partition_name, "st_", strlen("st_")) == 0) ) map_record = &__darwin_short_term_partition_info;

    return map_record;
}
