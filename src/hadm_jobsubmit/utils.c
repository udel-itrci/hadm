

#include "utils.h"

bool
is_nonempty_str(
  const char    *s
)
{
  if ( s != NULL ) {
    if ( *s != '\0' ) {
      while ( *s && isspace(*s) ) s++;
      if ( *s != '\0' ) return true;
    }
  }
  return false;
}

/**/

bool
str_in_list(
  const char    *haystack,
  const char    *needle,
  char          separator,
  bool          fold_case
)
{
  char*         (*search_fn)(const char *, const char *) = (fold_case ? xstrcasestr : xstrstr);
  const char    *p = haystack;
  size_t        needle_len = strlen(needle);
  
  while ( *p && (p = search_fn(p, needle)) ) {
    /* haystack starts with the needle */
    if ( (p == haystack) && ((p[needle_len] == separator) || (p[needle_len] == '\0')) ) return true;
    
    /* not at the start, so we must be preceded by a separator... */
    if ( p[-1] == separator ) {
      /* ...and end with a separator or NUL: */
      if ( (p[needle_len] == separator) || (p[needle_len] == '\0') ) return true;
    }
    p += needle_len;
    while ( *p && (*p != separator) ) p++;
    while ( *p == separator ) p++;
  }
  return false;
}

/**/

char*
str_replace_in_list(
  const char    *haystack,
  const char    *needle,
  const char    *replacement,
  char          separator,
  bool          fold_case
)
{
  char*         (*search_fn)(const char *, const char *) = (fold_case ? xstrcasestr : xstrstr);
  const char    *p = haystack;
  size_t        needle_len = strlen(needle);
  
  while ( *p && (p = search_fn(p, needle)) ) {
    /* haystack starts with the needle */
    if ( (p == haystack) && ((p[needle_len] == separator) || (p[needle_len] == '\0')) ) {
      char      *out_str = xstrdup(replacement);
      
      if ( p[needle_len] ) xstrcat(out_str, p + needle_len);
      return out_str;
    }
    
    /* not at the start, so we must be preceded by a comma... */
    if ( p[-1] == separator ) {
      /* ...and end with a comma or NUL: */
      if ( (p[needle_len] == separator) || (p[needle_len] == '\0') ) {
        char    *out_str = xstrndup(haystack, p - haystack);
        
        xstrcat(out_str, replacement);
        if ( p[needle_len] ) xstrcat(out_str, p + needle_len);
        return out_str;
      }
    }
    p += needle_len;
    while ( *p && (*p != separator) ) p++;
    while ( *p == separator ) p++;
  }
  return NULL;
}