FIND_FILE(
        SLURM_HEADER
        NAMES slurm/slurm.h
        PATHS ${SLURM_ROOT} ENV SLURM_ROOT
        PATH_SUFFIXES include
        DOC "The Slurm public header file"
    )
IF(SLURM_HEADER)
    # The base include path is two levels back:
    GET_FILENAME_COMPONENT(SLURM_INCLUDE_DIR ${SLURM_HEADER} DIRECTORY)
    GET_FILENAME_COMPONENT(SLURM_INCLUDE_DIR ${SLURM_INCLUDE_DIR} DIRECTORY)
    
    FIND_LIBRARY(
            SLURM_LIBRARY
            NAMES slurm
            PATHS ${SLURM_ROOT} ENV SLURM_ROOT
            PATH_SUFFIXES lib lib64
            DOC "The Slurm runtime library"
        )
    IF(SLURM_LIBRARY)
        GET_FILENAME_COMPONENT(SLURM_LIBRARY_DIR ${SLURM_LIBRARY} DIRECTORY)
        FIND_PATH(
                SLURM_MODULES_DIR
                NAMES libslurmfull.so
                PATHS ${SLURM_LIBRARY_DIR}/slurm
                DOC "Directory containing SLURM extensions."
            )
    ENDIF()
ENDIF()

# handle the QUIETLY and REQUIRED arguments and set SLURM_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
        SLURM
        REQUIRED_VARS SLURM_LIBRARY SLURM_INCLUDE_DIR SLURM_MODULES_DIR
    )

IF(SLURM_FOUND)
    SET(SLURM_LIBRARIES ${SLURM_LIBRARY})
    SET(SLURM_INCLUDE_DIRS ${SLURM_INCLUDE_DIR})
ENDIF()

#
# If a SLURM_SRC_ROOT is present, then make sure it looks like
# one and is added to the header paths, too:
#
FIND_PATH(
        SLURM_SRC_DIR
        NAMES slurm.spec slurm/slurm.h.in
        PATHS ${SLURM_SRC_ROOT} ENV SLURM_SRC_ROOT
        DOC "Directory containing Slurm source code"
    )
IF(SLURM_SRC_DIR)
    SET(SLURM_SRC_DIR_FOUND 1)
    LIST(APPEND SLURM_INCLUDE_DIRS ${SLURM_SRC_DIR})
    IF(NOT SLURM_BUILD_ROOT)
        SET(SLURM_BUILD_ROOT ${SLURM_SRC_ROOT})
    ENDIF()
ENDIF()

#
# If a SLURM_BUILD_ROOT is present, then make sure it looks like
# one and is added to the header paths, too:
#
FIND_PATH(
        SLURM_BUILD_DIR
        NAMES config.h slurm/slurm.h
        PATHS ${SLURM_BUILD_ROOT} ENV SLURM_BUILD_ROOT
        DOC "Directory containing Slurm build"
    )
IF(SLURM_BUILD_DIR)
    SET(SLURM_BUILD_DIR_FOUND 1)
    LIST(APPEND SLURM_INCLUDE_DIRS ${SLURM_BUILD_DIR})
ENDIF()
