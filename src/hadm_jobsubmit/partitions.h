
#ifndef __PARTITIONS_H__
#define __PARTITIONS_H__

#include "darwin_config.h"

/*!
 * @typedef  darwin_partition_type_t
 *
 * Define the different types of partitions in DARWIN.
 */
typedef enum {
    darwin_partition_type_unknown,
    darwin_partition_type_cpu,
    darwin_partition_type_gpu,
    darwin_partition_type_shortterm,
    darwin_partition_type_invalid
} darwin_partition_type_t;

/*!
 * @defined darwin_partition_type_is_gpu_capable
 *
 * Returns boolean true of a darwin_partition_type_t value, T, is capable of supporting
 * GPU jobs.
 */
#define darwin_partition_type_is_gpu_capable(T) (((T) == darwin_partition_type_gpu) || ((T) == darwin_partition_type_shortterm))

/*!
 * @typedef darwin_rdr_t
 *
 * Enumeration of the RDR types present on DARWIN.
 */
typedef enum {
    darwin_rdr_unknown = -1,
    darwin_rdr_cpu = 0,
    darwin_rdr_gpu,
    darwin_rdr_max
} darwin_rdr_t;

/*!
 * @typedef darwin_partition_info_t
 *
 * Data structure containing the disparate bits of info about partitions versus
 * resources in the partition.
 */
typedef struct {
    darwin_partition_type_t partition_type;
    const char              *partition_name;
    const char              *gpu_type;
    darwin_rdr_t            rdr_type;
} darwin_partition_info_t;

/*!
 * @function darwin_partition_info_for_name
 *
 * Lookup the partition info record for the given partition name.
 *
 * Short-term partitions MAY return a common record that lacks a partition name
 * but has the partition_type set appropriately.  If specific short-term partitions
 * are present in this APIs internal list, a full record may be returned.
 *
 * NULL is returned if the partition is completely unknown to this API.
 */
extern darwin_partition_info_t* darwin_partition_info_for_name(const char *partition_name, size_t partition_name_len);

#endif /* __PARTITIONS_H__ */
