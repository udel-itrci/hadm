

#include "darwin_config.h"
#include "partitions.h"
#include "utils.h"

#include "src/common/assoc_mgr.h"

/*
 * Plugin identity and versioning info:
 */
const char plugin_name[]        = "HADM job submit plugin";
const char plugin_type[]        = "job_submit/hadm";
const uint32_t plugin_version   = SLURM_VERSION_NUMBER;

/**/

int
__validate_job_descriptor(
    struct job_descriptor       *job_desc,
    uint32_t                    submit_uid,
    char                        **err_msg
)
{
    static char*                tres_list_list[] = {
                                        "tres_per_job",
                                        "tres_per_node",
                                        "tres_per_socket",
                                        "tres_per_socket"
                                    };
    char**                      tres_list_ptr[] = {
                                        &job_desc->tres_per_job,
                                        &job_desc->tres_per_node,
                                        &job_desc->tres_per_socket,
                                        &job_desc->tres_per_socket
                                    };
    static int                  tres_list_list_len = sizeof(tres_list_list) / sizeof(char*);

    int                         rc = SLURM_SUCCESS, i;
    const char                  *value;
    char                        *current_groupname = NULL;
    darwin_partition_info_t     *partition_info = NULL;
    darwin_partition_type_t     partition_type = darwin_partition_type_unknown;
    int                         partition_count = 0;
    bool                        has_gpu_request = false;
    
    /*
     * 0. Current gid is a workgroup?
     */
    if ( job_desc->group_id < DARWIN_GID_LOW || job_desc->group_id > DARWIN_GID_HIGH ) {
        *err_msg = xstrdup("Please choose a workgroup before submitting a job");
        return SLURM_ERROR;
    }
    
    current_groupname = gid_to_string_or_null(job_desc->group_id);
    if ( ! current_groupname ) {
        *err_msg = xstrdup_printf("getgrid(%d) failed (errno = %d)", job_desc->group_id, errno);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /*
     * 1. --account provided?
     */
    value = job_desc->account;
    if ( ! is_nonempty_str(value) ) {
        /* Set the account = current group name */
        if ( ! (job_desc->account = xstrdup(current_groupname)) ) {
            *err_msg = xstrdup_printf("failed to set Slurm account to current group `%s` (errno = %d)", current_groupname, errno);
            rc = SLURM_ERROR;
            goto early_exit;
        } else {
            debug("set --account=%s", current_groupname);
        }
    }
    else if ( strcmp(value, current_groupname) != 0 ) {
        /*
         * The user provided a value that doesn't match!
         */
        *err_msg = xstrdup_printf("You provided --account=%s and you are running as workgroup %s.  Please omit --account or ensure you are running in the appropriate workgroup.", value, current_groupname);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /*
     * 2. Now that we have an account name (pointed-to by 'value'), is it a
     *    short-term allocation?
     */
    if ( strncasecmp(job_desc->account, "st_", 3) == 0 ) {
        /*
         * Force the partition and reservation for the job and be done with it:
         */
        if ( ! job_desc->partition || (strcmp(job_desc->partition, job_desc->account) != 0) ) {
            if ( job_desc->partition ) xfree(job_desc->partition);
            job_desc->partition = xstrdup(job_desc->account);
        }
        if ( ! job_desc->reservation || (strcmp(job_desc->reservation, job_desc->account) != 0) ) {
            if ( job_desc->reservation ) xfree(job_desc->reservation);
            job_desc->reservation = xstrdup(job_desc->account);
        }
        rc = SLURM_SUCCESS;
        goto early_exit;
    }
    
    /*
     * 3. --partition provided?
     */
    value = job_desc->partition;
    if ( ! is_nonempty_str(value) ) {
        /*
         * Jobs MUST be submitted with an explicit partition requested
         */
        *err_msg = xstrdup("All jobs must explicitly request a partition");
        rc = SLURM_ERROR;
        goto early_exit;
    } else {
        /*
         * We need to iterate of all values in the list.  Make sure each value is
         * a known DARWIN partition and all types are the same.
         */
        const char          *partition_list_base = value,
                            *partition_list_comma =  NULL;
        
        while ( *partition_list_base ) {
            /* Get rid of leading comma(s): */
            while ( *partition_list_base == ',' ) partition_list_base++;
            if ( *partition_list_base ) {
                darwin_partition_type_t parsed_part_type;
                size_t                  part_name_len;
                
                /* Find the next comma or the NUL terminator: */
                partition_list_comma = strchrnul(partition_list_base, ',');
                /* Calculate length of the sub-string: */
                part_name_len = (partition_list_comma - partition_list_base);
                /* Parse the partition name => type: */
                partition_info = darwin_partition_info_for_name(partition_list_base, part_name_len);
                if ( partition_info ) {
                    /* Any unknown partitions will just be skipped. */
                    if ( partition_type == darwin_partition_type_unknown ) {
                        /* First pass, just note the type: */
                        partition_type = partition_info->partition_type;
                    } else if ( partition_type != partition_info->partition_type ) {
                        *err_msg = xstrdup("Jobs may not request multiple partition types (e.g. CPU and GPU)");
                        rc = SLURM_ERROR;
                        goto early_exit;
                    }
                }
                /* That's another partition in the list.... */
                partition_count++;
                partition_list_base = partition_list_comma;
            }
        }
    }
    
    if ( partition_count > 1 ) {
        *err_msg = xstrdup_printf("Jobs may request a single partition only (%d requested by this job)", partition_count);
        rc = SLURM_ERROR;
        goto early_exit;
    }
    
    /* The partition_info now points to the partition info for the selected single partition
     * of a known type, or is NULL for all other partition name(s):
     */
    if ( partition_info ) {
        /*
         * 4. The whole --gres thing (and --gpus*) will be in the tres_* fields of the
         *    job descriptor.  For each TRES field, check the format of any GPU value
         *    matches <type>:<count> and if GPUs are needed, ensure the partition type
         *    is right
         */
        i = 0;
        while ( i < tres_list_list_len ) {
            value = *(tres_list_ptr[i]);
            if ( is_nonempty_str(value) ) {
                const char  *gres_gpu_ptr = value;
                size_t      value_len = strlen(value);
        
                /*
                 * Search for gpu{:<type>}{:<#>} strings in the list:
                 */
                while ( (gres_gpu_ptr = strstr(gres_gpu_ptr, "gpu")) ) {
                    /* Either this is the start of the string or the preceding character
                     * was a comma: */
                    if ( (gres_gpu_ptr == value) || (gres_gpu_ptr[-1] == ';') ) {
                        /*
                         * Okay, so we found gpu.  What's after it?
                         */
                        const char      *start = gres_gpu_ptr + strlen("gpu");
                    
                        if ( *start == ':' ) {
                            bool        has_sub_type = false, is_proper_format = false;
                            long        gpu_count = -1;
                            char        *end_ptr;
                        
                            /*
                             * Good, there's a colon; what follows is either the GPU
                             * sub-type or the count:
                             */
                            end_ptr = NULL;
                            gpu_count = strtol(++start, &end_ptr, 0);
                            if ( (end_ptr == start) || ((*end_ptr != '\0') && (*end_ptr != ';')) ) {
                                /*
                                 * Subtype present, skip ahead to a colon, comma, or end
                                 * of string:
                                 */
                                has_sub_type = true;
                                start = end_ptr;
                                while ( (*start != '\0') && (*start != ':') && (*start != ';') ) start++;
                                if ( *start == ':' ) {
                                    end_ptr = NULL;
                                    gpu_count = strtol(++start, &end_ptr, 0);
                                    if ( (*end_ptr == '\0') || (*end_ptr == ';') ) is_proper_format = true;
                                }
                            } else {
                                is_proper_format = true;
                            }
                        
                            if ( ! is_proper_format ) {
                                *err_msg = xstrdup_printf("Invalid TRES GPU in %s from `%s`", tres_list_list[i], gres_gpu_ptr);
                                rc = SLURM_ERROR;
                                goto early_exit;
                            }
                        
                            /*
                             * Was a sub-type present?
                             */
                            if ( ! has_sub_type ) {
#if defined(SHOULD_ADD_GPU_TYPES)
                                char        *new_value;
                                size_t      replacement_length;
                                size_t      offset, new_length;
                    
                                if ( ! partition_info->gpu_type ) {
                                    *err_msg = xstrdup("No GPU type could be inferred from the chosen partition");
                                    rc = SLURM_ERROR;
                                    goto early_exit;
                                }
                    
                                /* Gotta fill-in the GPU <type> for the TRES.  At this point
                                 * gres_gpu_ptr is pointing to the start of the gpu substring
                                 * and end_ptr to the character following its full run -- giving
                                 * us the entire replacement range.
                                 */
                                replacement_length = snprintf(NULL, 0, "gpu:%s:%ld", partition_info->gpu_type, gpu_count);
                                offset = gres_gpu_ptr - value;
                                new_length = offset + replacement_length + strlen(end_ptr) + 1;
                                new_value = (char*)xmalloc(new_length);
                                if ( offset ) memcpy(new_value, value, offset);
                                snprintf(new_value + offset, replacement_length + 1, "gpu:%s:%ld", partition_info->gpu_type, gpu_count);
                                if ( *end_ptr ) strcpy(new_value + offset + replacement_length, end_ptr);
                    
                                /* Now replace the existing job_descriptor value and get our scan
                                 * pointers reset in the new buffer: */
                                *(tres_list_ptr[i]) = new_value;
                                info("Corrected %s value of %s to be %s", tres_list_list[i], value, new_value);
                                xfree(value);
                                value = new_value;
                                gres_gpu_ptr = value + offset + replacement_length + 1;

#elif defined(SHOULD_REQUIRE_GPU_TYPE)

                                *err_msg = xstrdup_printf("No GPU <type> specified in %s at `%s`", tres_list_list[i], gres_gpu_ptr);
                                rc = SLURM_ERROR;
                                goto early_exit;

#else

                                goto gpu_complete;

#endif
                            } else {
                                gres_gpu_ptr = end_ptr;
                                goto gpu_complete;
                            }
gpu_complete:
                            has_gpu_request = true;
                            /* We need GPUs, so is that our partition type? */
                            if ( ! darwin_partition_type_is_gpu_capable(partition_type) ) {
                                *err_msg = xstrdup("Jobs requiring GPU resources must be submitted to a GPU-oriented partition");
                                rc = SLURM_ERROR;
                                goto early_exit;
                            }
                        } else {
                            gres_gpu_ptr = start;
                        }
                    } else {
                        /* Skip ahead */
                        gres_gpu_ptr++;
                    }
                }
            }
            i++;
        }
    
        /*
         * If the partition is of GPU type, was a GPU requested?
         */
        if ( darwin_partition_type_is_gpu_capable(partition_type) && ! has_gpu_request ) {
            info("%s: GPU job w/o GPU resource request; forcing 1 GPU-per-node", plugin_type);
            if ( job_desc->tres_per_node ) {
                char        *updated_tres = xstrdup_printf("%s;gpu:%s:1", job_desc->tres_per_node, partition_info->gpu_type);
                xfree(job_desc->tres_per_node);
                job_desc->tres_per_node = updated_tres;
            } else {
                job_desc->tres_per_node = xstrdup_printf("gpu:%s:1", partition_info->gpu_type);
            }
        }
        
        /*
         * 5. If no QOS was specified, then default to 'allocation'
         */
        if ( ! job_desc->qos || ! *(job_desc->qos) ) {
            if ( job_desc->qos ) xfree(job_desc->qos);
            job_desc->qos = xstrdup("allocation");
        }
    }
    
    /* Ensure that an empty time-min is set to time-limit: */
    if ( job_desc->time_min == NO_VAL ) {
        job_desc->time_min = job_desc->time_limit;
        info("%s: time_min is empty, setting to time_limit", plugin_type);
    }
    
    /* Memory limit _must_ be set: */
    if ( job_desc->pn_min_memory == NO_VAL64 ) {
        job_desc->pn_min_memory = DARWIN_MINIMUM_MEM_MB | MEM_PER_CPU;
        info("%s: setting default memory limit (%lu MiB per CPU)", plugin_type, DARWIN_MINIMUM_MEM_MB);
    }
    
early_exit:
    if ( current_groupname ) xfree(current_groupname);
    return rc;

}

extern int
job_submit(
    struct job_descriptor   *job_desc,
    uint32_t                submit_uid,
    char                    **err_msg
)
{
    return __validate_job_descriptor(job_desc, submit_uid, err_msg);
}

extern int
job_modify(
  struct job_descriptor     *job_desc,
  struct job_record         *job_ptr,
  uint32_t                  submit_uid
)
{
    char                    *err_msg = NULL;
    int                     rc = __validate_job_descriptor(job_desc, submit_uid, &err_msg);

    if ( err_msg ) {
        error(err_msg);
        xfree(err_msg);
    }
    return rc;
}
