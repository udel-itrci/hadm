#!/bin/bash
#
# Service start/restart/stop script for PostgreSQL database servers.
#

# Pop the method and instance arguments off the command line; the rest
# of the arguments will be passed to any singularity instance-starting
# commands.
if [ $# -lt 2 ]; then
    echo "ERROR:  $0 requires <action> and <instance> arguments"
    exit 22
fi
METHOD="$1"; shift
INSTANCE="$1"; shift
DATABASE_PREFIX="/var/lib/postgresql-containers/${INSTANCE}"
SINGULARITY_INSTANCE="postgresql-${INSTANCE}"

# Check for the container image:
if [ ! -f "${DATABASE_PREFIX}/image.sif" ]; then
    echo "ERROR:  no container image file found: ${DATABASE_PREFIX}/image.sif"
    exit 1
fi

# Check for the root homedir:
if [ ! -d "${DATABASE_PREFIX}/root" ]; then
    mkdir -p --mode=0700 "${DATABASE_PREFIX}/root" >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "ERROR:  unable to create external /root homedir: ${DATABASE_PREFIX}/root"
        exit 1
    fi
fi

# Check for the database datadir:
if [ ! -d "${DATABASE_PREFIX}/data" ]; then
    mkdir -p --mode=0700 "${DATABASE_PREFIX}/data" >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "ERROR:  unable to create external database datadir: ${DATABASE_PREFIX}/data"
        exit 1
    fi
fi

# Check for the /run dir:
if [ ! -d "${DATABASE_PREFIX}/run" ]; then
    mkdir -p --mode=0755 "${DATABASE_PREFIX}/run" >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "ERROR:  unable to create external /run dir: ${DATABASE_PREFIX}/run"
        exit 1
    fi
fi

# Get singularity loaded into the env:
source /etc/profile.d/valet.sh
vpkg_require singularity/default >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "ERROR:  unable to load Singularity into runtime environment"
    exit 1
fi

function instance_start
{
    singularity instance start --no-home \
        --bind "${DATABASE_PREFIX}/data:/var/lib/postgresql/data:rw" \
        --bind "${DATABASE_PREFIX}/root:/root:rw" \
        --bind "${DATABASE_PREFIX}/run:/run:rw" \
        --env INSTANCE_ARGS="-c wal_recycle=off" \
        "$@" \
        "${DATABASE_PREFIX}/image.sif" "$SINGULARITY_INSTANCE"
    return $?
}

function instance_stop
{
    singularity instance stop "$SINGULARITY_INSTANCE"
    return $?
}

# Check to see if we're already running:
IS_RUNNING="$(singularity instance list | egrep "^${SINGULARITY_INSTANCE}\\s")"
RC=0
case "$METHOD" in

    start)
        if [ -n "$IS_RUNNING" ]; then
            echo "WARNING:  instance ${SINGULARITY_INSTANCE} already running"
        else
            instance_start "$@"
            RC=$?
        fi
        ;;
    
    status)
        if [ -z "$IS_RUNNING" ]; then
            echo "instance ${SINGULARITY_INSTANCE} is not running"
        elif [[ $IS_RUNNING =~ ^${SINGULARITY_INSTANCE}[[:space:]]+([0-9]+) ]]; then
            echo "instance ${SINGULARITY_INSTANCE} is running with PID ${BASH_REMATCH[1]}"
        else
            echo "instance ${SINGULARITY_INSTANCE} is running"
        fi
        ;;
    
    restart|reload)
        if [ -n "$IS_RUNNING" ]; then
            instance_stop
            RC=$?
        fi
        if [ $RC -eq 0 ]; then
            instance_start "$@"
            RC=$?
        fi
        ;;
    
    stop)
        if [ -z "$IS_RUNNING" ]; then
            echo "WARNING:  instance ${SINGULARITY_INSTANCE} is not running"
        else
            instance_stop
            RC=$?
        fi
        ;;

    *)
        echo "ERROR:  unknown action '$METHOD'"
        RC=1
        ;;

esac
exit $RC
