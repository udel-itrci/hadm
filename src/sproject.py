#!/usr/bin/env python
#
# Querying of the hermes allocation database.
#

import sys
import os

from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE,SIG_DFL) 

#
# The REST API returns dates in UTC format; let's get a timezone helper setup to convert all dates
# to the local time zone for output:
#
import dateutil.parser
from datetime import *
import pytz
local_timezone = pytz.timezone(os.environ.get('TZ', 'America/New_York'))

#
# Additional modules that we'll make use of:
#
import pymunge
import argparse
import json
import csv

#
# Default base URL for the REST API:
#
DEFAULT_REST_API_URL = 'http://hadm-api:8000'

global_additional_headers = {}

class HermesException(Exception):
    """Exception that wraps an error response from the REST API."""
    def __init__(self, json_data):
        self.err_code = int(json_data.get('err_code', '-1'))
        self.err_msg = json_data.get('err_msg', 'ERROR:  An unknown error occurred')
        super().__init__('{:s} (code={:d})'.format(self.err_msg, self.err_code))

import pwd

def uname_with_uid_number(uid_number):
    """Convenience function that turns a uid number into a user name if possible, otherwise returning the uid number."""
    try:
        uinfo = pwd.getpwuid(uid_number)
        return uinfo.pw_name
    except:
        return uid_number

#
# This is a Python 2.x script, so we need to use the older urllib
# APIs
#
from urllib import quote_plus
from urllib2 import Request, urlopen, HTTPError, URLError

def build_rest_url(base, path='/', query_args={}):
    """Returns a string containing a composed URL under the REST API."""
    query_str = '&'.join([k + '=' + quote_plus(v) for (k,v) in query_args.items()])
    return base + ('' if path.startswith('/') else '/' ) + path + ('?' if len(query_str) > 0 else '') + query_str

def fetch_url(the_url, additional_headers=None):
    """Given a URL associated with the REST API, fetch the content and attempt to parse as JSON data.  Return the JSON object if successful.

HTTP 40x errors are displayed and the program exits immediately.  Likewise, for problems like connection timeout the error is displayed and the program exits immediately."""
    # Use MUNGE to sign the request URL:
    the_url_signature = pymunge.encode(the_url.encode('ASCII'))
    http_headers = { 'MungeUrlHash': the_url_signature }
    
    # Add any other headers:
    if global_additional_headers is not None and len(global_additional_headers):
        http_headers.update(global_additional_headers)
    if additional_headers is not None and len(additional_headers):
        http_headers.update(additional_headers)
    
    # Create the GET request with the MUNGE-signed header:
    the_request = Request(the_url, headers=http_headers)
    
    try:
        # Create the request for the URL:
        the_response = urlopen(the_request)
        
        # Request made, read the response:
        the_data = json.loads(the_response.read())
        
        # If we got an error JSON object, raise an exception:
        if 'err_msg' in the_data or 'err_code' in the_data:
            raise HermesException(the_data)
        
        # Otherwise, return the data:
        return the_data
    except HTTPError as E:
        sys.stderr.write('ERROR:  failed request to allocations server: ({:d}) {:s}\n'.format(E.code, E.reason))
        sys.exit(1)
    except URLError as E:
        sys.stderr.write('ERROR:  could not contact allocations server: {:s}\n'.format(E.reason))
        sys.exit(1)


class column_formatter(object):
    """Instances of the column_formatter base class are used to parse and format a single column of a data from a table (list of dictionaries or lists).

The datum for the column is assumed to be a string.  Subclasses exist that treat the datum as an integer or a date/timestamp."""
    def __init__(self, key, title=None, alignment=None):
        """The mandatory key attribute represents the column's key/index in each row of data.  The title is an alternate string that will be displayed in CSV and tabular forms; if None (the default) the key will be used as the column header.  The alignment is an alignment string from the Python string format() (e.g. ">" to right-align).""" 
        self.key = key
        self.title = str(title) if title is not None else key
        self.alignment = '' if alignment is None else alignment
    
    def string_to_value(self, string):
        """Convert and return an external string (probably from a JSON reply) to the native data type for this column."""
        return None if string is None else str(string)
    
    def value_to_string(self, value):
        """Convert a value (of the native data type for this column) to a string."""
        return '' if value is None else str(value)
    
    def value_to_json(self, value):
        """Convert a value (of the native data type for this column) to the appropriate type for JSON export."""
        return None if value is None else str(value)
    
    def format_str(self, width=0):
        """Return a format specification for this column's values."""
        fmt = '{:s}{:d}'.format(self.alignment, width)
        return ('{:' + fmt + 's}')

class int_column_formatter(column_formatter):
    """column_formatter subclass that treats the datum as an integer."""

    def __init__(self, key, title=None, alignment='>', scalar=None):
        super(int_column_formatter, self).__init__(key, title=title, alignment=alignment)
        self._scalar = scalar
    
    def string_to_value(self, string):
        try:
            v = int(string)
            if self._scalar is not None:
                v = int(round(self._scalar(v)))
            return v
        except:
            return None
    
    def value_to_string(self, value):
        return '' if value is None else str(value)
    
    def value_to_json(self, value):
        return None if value is None else int(value)

class date_column_formatter(column_formatter):
    """column_formatter subclass that treats the datum as a date/timestamp."""

    def string_to_value(self, string):
        try:
            return dateutil.parser.parse(string).astimezone(local_timezone)
        except:
            return ''


class data_array(object):
    """A data_array instance formats a list of dictionaries or lists for export/display."""

    OUTPUT_FORMATS = ('table', 'csv', 'json')
    DEFAULT_OUTPUT_FORMAT = 'table'

    SHOULD_KEEP_EMPTY_COLUMNS = 'should_keep_empty_columns'

    def __init__(self, column_defs, dataset, flags=[]):
        self._column_defs = [ v for v in column_defs ]
        self._widths = [ len(v.title) for v in column_defs ]
        
        # Ensure the dataset is a list:
        if not isinstance(dataset, list):
            dataset = [ dataset ]
        
        # Process the dataset:
        max_widths = [0]*len(self._widths)
        self._dataset = []
        for row in dataset:
            row_values = [ c.string_to_value(row[c.key]) for c in column_defs ]
            row_widths = [ len(c.value_to_string(v)) for (c,v) in zip(column_defs, row_values) ]
            max_widths = [ max(v) for v in zip(max_widths, row_widths) ]
            self._dataset.append(row_values)
            
        # If any columns are zero-width there's no data, and we can drop them if
        # allowed:
        if data_array.SHOULD_KEEP_EMPTY_COLUMNS in flags:
            self._should_hide = [False]*len(self._widths)
        else:
            self._should_hide = [(v == 0) for v in max_widths]
        
        # Merge row-based widths with title lengths for true max:
        self._widths = [ max(v) for v in zip(self._widths, max_widths) ]
        
        # Generate the format string:
        self._format_str = ' '.join([ '' if h else c.format_str(w) for (c,w,h) in zip(self._column_defs, self._widths, self._should_hide) ])
    
    def print_array(self, output_format='table'):
        if output_format == 'table':
            print(self._format_str.format(*[ c.title for c,h in zip(self._column_defs, self._should_hide) if not h ]))
            print(self._format_str.format(*[ '-'*w for w,h in zip(self._widths, self._should_hide) if not h ]))
            for row in self._dataset:
                print(self._format_str.format(*[ c.value_to_string(v) for c,v,h in zip(self._column_defs, row, self._should_hide) if not h ]))
        elif output_format == 'csv':
            csv_writer = csv.writer(sys.stdout)
            csv_writer.writerow([ c.title for c,h in zip(self._column_defs, self._should_hide) if not h ])
            for row in self._dataset:
                csv_writer.writerow([ c.value_to_string(v) for c,v,h in zip(self._column_defs, row, self._should_hide) if not h ])
        elif output_format == 'json':
            output_data = []
            for row in self._dataset:
                output_data.append({ c.key:c.value_to_json(v) for c,v in zip(self._column_defs, row)})
            print(json.dumps(output_data, indent=4))


class textual_mappings(object):

    def __init__(self, base_rest_url):
        self.__allocation_map = {
                        r['allocation_id']: {'category':r['category'], 'project_id':r['project_id'], 'resource':r['resource']} for r in fetch_url(build_rest_url(base=base_rest_url, path='/alloc'))
                    }
        self.__project_map = {
                        r['project_id']: {'account':r['account'], 'gid':r['gid'], 'account':r['account']} for r in fetch_url(build_rest_url(base=base_rest_url, path='/project'))
                    }

    def allocation_id_to_projrdr(self, allocation_id):
        alloc_info = self.__allocation_map[allocation_id]
        proj_info = self.__project_map[alloc_info['project_id']]
        return proj_info['account'] + '::' + alloc_info['resource']
            

#
# Initial argument parse to get the datum of interest:
#
cli_parser = argparse.ArgumentParser(description='Interface for querying DARWIN allocations.')
cli_parser.add_argument('--rest-url', '-U', metavar='<base-url>',
        dest='base_rest_url',
        default=DEFAULT_REST_API_URL,
        help='Base URL of the REST API to be queried'
    )
cli_parser.add_argument('--run-as',
        dest='run_as',
        help=argparse.SUPPRESS
    )
cli_parser.add_argument('--format', '-f', metavar='<output-format>',
        dest='output_format',
        default=data_array.DEFAULT_OUTPUT_FORMAT,
        choices=data_array.OUTPUT_FORMATS,
        help='Controls the formatting of the program output ({:s}, default is {:s})'.format(', '.join(data_array.OUTPUT_FORMATS), data_array.DEFAULT_OUTPUT_FORMAT)
    )
cli_parser.add_argument('--numeric', '-n',
        dest='should_show_numeric',
        default=False,
        action='store_true',
        help='Do not resolve numeric values to textual forms (e.g. user ids, allocation ids)'
    )
cli_parser.add_argument('--exact', '-x',
        dest='should_show_exact_usage',
        default=False,
        action='store_true',
        help='Show SU values in full-precision (minutes, not hours)'
    )
datum_subparser = cli_parser.add_subparsers(title='data type to query',
        dest='datum'
    )

#
# CLI arguments associated with the "alloations" subcommand:
#
allocations_parser = datum_subparser.add_parser('allocations',
        help='Allocation properties and balances'
    )
allocations_parser.add_argument('--detail',
        dest='should_show_detail',
        default=False,
        action='store_true',
        help='Show detailed information rather than a simple list'
    )
allocations_parser.add_argument('--by-user',
        dest='should_show_by_user',
        default=False,
        action='store_true',
        help='Break-out detailed view into individual usage by project user (implies --detail)'
    )
allocations_parser.add_argument('--current-only', '--active-only',
        dest='should_show_current_only',
        default=False,
        action='store_true',
        help='Only show allocations that are active now (when this command is executed)'
    )
allocations_parser.add_argument('--allocation-id', metavar='<alloc-id>',
        dest='allocation_ids',
        action='append',
        type=int,
        help='Select one or more specific allocation ids'
    )
allocations_parser.add_argument('--project-id', metavar='<project-id>',
        dest='project_ids',
        action='append',
        type=int,
        help='Select one or more specific project ids'
    )
allocations_parser.add_argument('--project', '--workgroup', '-w', metavar='<project>',
        dest='projects',
        action='append',
        help='Select one or more specific projects/workgroups in the query'
    )
allocations_parser.add_argument('--uid', '-u', metavar='<username|uid-number>',
        dest='uids',
        action='append',
        help='Select one or more specific users in the query (requires additional privileges)'
    )
allocations_parser.add_argument('--gid', '-g', metavar='<groupname|gid-number>',
        dest='gids',
        action='append',
        help='Select one or more specific Unix groups in the query'
    )

#
# CLI arguments associated with the "projects" subcommand:
#
projects_parser = datum_subparser.add_parser('projects',
        help='Project properties'
    )
projects_parser.add_argument('--detail',
        dest='should_show_detail',
        default=False,
        action='store_true',
        help='Show detailed information rather than a simple list'
    )
projects_parser.add_argument('--project-id', metavar='<project-id>',
        dest='project_ids',
        action='append',
        type=int,
        help='Select one or more specific project ids'
    )
projects_parser.add_argument('--project', '--workgroup', '-w', metavar='<project>',
        dest='projects',
        action='append',
        help='Select one or more specific projects/workgroups in the query'
    )
projects_parser.add_argument('--uid', '-u', metavar='<username|uid-number>',
        dest='uids',
        action='append',
        help='Select one or more specific users in the query (requires additional privileges)'
    )
projects_parser.add_argument('--gid', '-g', metavar='<groupname|gid-number>',
        dest='gids',
        action='append',
        help='Select one or more specific Unix groups in the query'
    )

#
# CLI arguments associated with the "jobs" subcommand:
#
jobs_parser = datum_subparser.add_parser('jobs',
        help='Submitted and running job properties'
    )
jobs_parser.add_argument('--sort',
        dest='sort_by',
        choices=['activity_id', 'allocation_id', 'creation_date', 'job_id', 'modification_date', 'status', 'owner_uid'],
        help='Sort the data by a specific column prior to display'
    )
jobs_parser.add_argument('--descending',
        dest='should_sort_descending',
        default=False,
        action='store_true',
        help='Sort the data in descending rather than ascending order'
    )
jobs_parser.add_argument('--activity-id', metavar='<activity-id>',
        dest='activity_ids',
        action='append',
        type=int,
        help='Select one or more specific debit/credit activities'
    )
jobs_parser.add_argument('--allocation-id', metavar='<alloc-id>',
        dest='allocation_ids',
        action='append',
        type=int,
        help='Select one or more specific allocation ids'
    )
jobs_parser.add_argument('--project-id', metavar='<project-id>',
        dest='project_ids',
        action='append',
        type=int,
        help='Select one or more specific project ids'
    )
jobs_parser.add_argument('--project', '--workgroup', '-w', metavar='<project>',
        dest='projects',
        action='append',
        help='Select one or more specific named projects/workgroups in the query'
    )
jobs_parser.add_argument('--uid', '-u', '--owner-uid', metavar='<username|uid-number>',
        dest='owner_uids',
        action='append',
        help='Select one or more specific users in the query (requires additional privileges)'
    )
jobs_parser.add_argument('--gid', '-g', metavar='<groupname|gid-number>',
        dest='gids',
        action='append',
        help='Select one or more specific Unix groups in the query'
    )

#
# CLI arguments associated with the "failures" subcommand:
#
failures_parser = datum_subparser.add_parser('failures',
        help='Properties of jobs that failed due to allocation limits'
    )
failures_parser.add_argument('--detail',
        dest='should_show_detail',
        default=False,
        action='store_true',
        help='Show additional details rather than a compact list'
    )
failures_parser.add_argument('--sort',
        dest='sort_by',
        choices=['activity_id', 'allocation_id', 'amount', 'creation_date', 'job_id', 'modification_date', 'owner_uid'],
        help='Sort the data by a specific column prior to display'
    )
failures_parser.add_argument('--descending',
        dest='should_sort_descending',
        default=False,
        action='store_true',
        help='Sort the data in descending rather than ascending order'
    )
failures_parser.add_argument('--activity-id', metavar='<activity-id>',
        dest='activity_ids',
        action='append',
        type=int,
        help='Select one or more specific debit/credit activities'
    )
failures_parser.add_argument('--allocation-id', metavar='<alloc-id>',
        dest='allocation_ids',
        action='append',
        type=int,
        help='Select one or more specific allocation ids'
    )
failures_parser.add_argument('--project-id', metavar='<project-id>',
        dest='project_ids',
        action='append',
        type=int,
        help='Select one or more specific project ids'
    )
failures_parser.add_argument('--project', '--workgroup', '-w', metavar='<project>',
        dest='projects',
        action='append',
        help='Select one or more specific named projects/workgroups in the query'
    )
failures_parser.add_argument('--uid', '-u', '--owner-uid', metavar='<username|uid-number>',
        dest='owner_uids',
        action='append',
        help='Select one or more specific users in the query (requires additional privileges)'
    )
failures_parser.add_argument('--gid', '-g', metavar='<groupname|gid-number>',
        dest='gids',
        action='append',
        help='Select one or more specific Unix groups in the query'
    )
failures_parser.add_argument('--job-id', '-j', metavar='<job-id>',
        dest='job_ids',
        action='append',
        help='Select one or more specific Slurm job ids'
    )

#
# Ready:  parse all command line arguments:
#
cli_args = cli_parser.parse_args()

# Add the run-as header if required:
if cli_args.run_as:
    global_additional_headers['X-HADM-RunAs'] = cli_args.run_as

# Make sure the name-mapping class can access the REST API:        
if not cli_args.should_show_numeric:
    alloc_map = textual_mappings(cli_args.base_rest_url)

su_unit_divider = None if cli_args.should_show_exact_usage else lambda v:v/60.0

if cli_args.datum == 'allocations':
    query_args = {}
    if cli_args.project_ids is not None:
        query_args['project_id'] = ','.join([str(v) for v in cli_args.project_ids])
    if cli_args.projects is not None:
        query_args['account'] = ','.join(cli_args.projects)
    if cli_args.gids is not None:
        query_args['gid'] = ','.join([str(v) for v in cli_args.gids])
    if cli_args.uids is not None:
        query_args['uid'] = ','.join([str(v) for v in cli_args.uids])
    if cli_args.allocation_ids is not None:
        query_args['allocation_id'] = ','.join([str(v) for v in cli_args.allocation_ids])
    query_path = '/alloc'
    
    # --by-user implies detail:
    if cli_args.should_show_by_user:
        cli_args.should_show_detail = True
    
    # if --current-only, then get a string to compare against:
    if cli_args.should_show_current_only:
        date_and_time_now = datetime.now(local_timezone)
    
    try:
        # Generate a list of allocations:
        query_url = build_rest_url(base=cli_args.base_rest_url, path=query_path, query_args=query_args)
        json_data = fetch_url(query_url)
        
        # Filter to current allocations only?
        if cli_args.should_show_current_only:
            json_data = [a for a in json_data if (dateutil.parser.parse(a['start_date']).astimezone(local_timezone) < date_and_time_now and dateutil.parser.parse(a['end_date']).astimezone(local_timezone) > date_and_time_now)]
        
        # Override should_show_detail if --by-user was used:
        if cli_args.should_show_by_user:
           cli_args.should_show_detail = True 
        
        # If we're not looking for detail, just print the list:
        if cli_args.should_show_detail:
            if cli_args.should_show_by_user:
                query_args = {'report': 'detail'}
                allocations = []
                
                for row in json_data:
                    data_to_add = {'project_id': row['project_id'], 'category': row['category'], 'resource': row['resource'] }
                    if cli_args.output_format in ('csv', 'json'):
                        for user_row in fetch_url(build_rest_url(base=cli_args.base_rest_url, path='/alloc/{:d}'.format(row['allocation_id']), query_args=query_args)):
                            current_row = dict(data_to_add)
                            current_row.update(user_row)
                            if not cli_args.should_show_numeric:
                                if current_row['owner_uid'] is not None:
                                    current_row['owner_uid'] = uname_with_uid_number(int(current_row['owner_uid']))
                                current_row['allocation'] = alloc_map.allocation_id_to_projrdr(current_row['allocation_id'])
                            if current_row['activity_kind'] == 'predebit':
                                current_row['activity_kind'] = 'run+complt'
                            allocations.append(current_row)
                    else:
                        for user_row in fetch_url(build_rest_url(base=cli_args.base_rest_url, path='/alloc/{:d}'.format(row['allocation_id']), query_args=query_args)):
                            data_to_add.update(user_row)
                            if not cli_args.should_show_numeric:
                                if data_to_add['owner_uid'] is not None:
                                    data_to_add['owner_uid'] = uname_with_uid_number(int(data_to_add['owner_uid']))
                                data_to_add['allocation'] = alloc_map.allocation_id_to_projrdr(data_to_add['allocation_id'])
                            if data_to_add['activity_kind'] == 'predebit':
                                data_to_add['activity_kind'] = 'run+complt'
                            allocations.append(data_to_add)
                            data_to_add = {'project_id': '', 'category': '', 'resource': '' }
                
                # Build the data array:
                if not cli_args.should_show_numeric:
                    da = data_array(
                                [
                                    int_column_formatter('project_id', title='Project id'),
                                    int_column_formatter('allocation_id', title='Alloc id'),
                                    column_formatter('allocation', title='Alloc descr'),
                                    column_formatter('category', title='Category'),
                                    column_formatter('resource', title='RDR'),
                                    int_column_formatter('owner_uid', title='User') if cli_args.should_show_numeric else column_formatter('owner_uid', title='User'),
                                    column_formatter('activity_kind', title='Transaction'),
                                    int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                                    column_formatter('comments', title='Comments'),
                                ],
                                allocations
                            )

                else:
                    da = data_array(
                                [
                                    int_column_formatter('project_id', title='Project id'),
                                    int_column_formatter('allocation_id', title='Allocation id'),
                                    column_formatter('category', title='Category'),
                                    column_formatter('resource', title='RDR'),
                                    int_column_formatter('owner_uid', title='User') if cli_args.should_show_numeric else column_formatter('owner_uid', title='User'),
                                    column_formatter('activity_kind', title='Transaction'),
                                    int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                                    column_formatter('comments', title='Comments'),
                                ],
                                allocations
                            )
                # then display it:
                da.print_array(cli_args.output_format)
            else:
                query_args = {'report': 'aggregate'}
                allocations = [fetch_url(build_rest_url(base=cli_args.base_rest_url, path='/alloc/{:d}'.format(a['allocation_id']), query_args=query_args)) for a in json_data]
                
                # Do we need to add allocation descriptions?
                if not cli_args.should_show_numeric:
                    # Add the project+RDR column:
                    alt_data = []
                    for row in allocations:
                        new_row = row.copy()
                        new_row['allocation'] = alloc_map.allocation_id_to_projrdr(row['allocation_id'])
                        alt_data.append(new_row)
                    
                    # Build the data array:
                    da = data_array(
                                [
                                    int_column_formatter('project_id', title='Project id'),
                                    int_column_formatter('allocation_id', title='Alloc id'),
                                    column_formatter('allocation', title='Alloc descr'),
                                    column_formatter('category', title='Category'),
                                    column_formatter('resource', title='RDR'),
                                    int_column_formatter('credit', title='Credit', scalar=su_unit_divider),
                                    int_column_formatter('predebit', title='Run+Cmplt', scalar=su_unit_divider),
                                    int_column_formatter('debit', title='Debit', scalar=su_unit_divider),
                                    int_column_formatter('balance', title='Balance', scalar=su_unit_divider),
                                ],
                                alt_data
                            )
                else:
                    # Build the data array:
                    da = data_array(
                                [
                                    int_column_formatter('project_id', title='Project id'),
                                    int_column_formatter('allocation_id', title='Allocation id'),
                                    column_formatter('category', title='Category'),
                                    column_formatter('resource', title='RDR'),
                                    int_column_formatter('credit', title='Credit', scalar=su_unit_divider),
                                    int_column_formatter('predebit', title='Run+Cmplt', scalar=su_unit_divider),
                                    int_column_formatter('debit', title='Debit', scalar=su_unit_divider),
                                    int_column_formatter('balance', title='Balance', scalar=su_unit_divider),
                                ],
                                allocations
                            )
                # then display it:
                da.print_array(cli_args.output_format)
        else:
            # Do we need to add allocation descriptions?
            if not cli_args.should_show_numeric:
                # Add the project+RDR column:
                alt_data = []
                for row in json_data:
                    new_row = row.copy()
                    new_row['allocation'] = alloc_map.allocation_id_to_projrdr(row['allocation_id'])
                    alt_data.append(new_row)
                    
                # Build the data array:
                da = data_array(
                            [
                                int_column_formatter('project_id', title='Project id'),
                                int_column_formatter('allocation_id', title='Alloc id'),
                                column_formatter('allocation', title='Alloc descr'),
                                column_formatter('category', title='Category'),
                                column_formatter('resource', title='RDR'),
                                date_column_formatter('start_date', title='Start date'),
                                date_column_formatter('end_date', title='End date'),
                            ],
                            alt_data
                        )
            else:
                # Build the data array:
                da = data_array(
                            [
                                int_column_formatter('project_id', title='Project id'),
                                int_column_formatter('allocation_id', title='Allocation id'),
                                int_column_formatter('category', title='Category'),
                                column_formatter('resource', title='RDR'),
                                date_column_formatter('start_date', title='Start date'),
                                date_column_formatter('end_date', title='End date'),
                            ],
                            json_data
                        )
            # then display it:
            da.print_array(cli_args.output_format)

    except HermesException as E:
        print(str(E))
        sys.exit(1)


elif cli_args.datum == 'projects':
    query_args = {}
    if cli_args.project_ids is not None:
        query_args['project_id'] = ','.join([str(v) for v in cli_args.project_ids])
    if cli_args.projects is not None:
        query_args['account'] = ','.join(cli_args.projects)
    if cli_args.gids is not None:
        query_args['gid'] = ','.join([str(v) for v in cli_args.gids])
    if cli_args.uids is not None:
        query_args['uid'] = ','.join([str(v) for v in cli_args.uids])
    query_path = '/project'
    
    try:
        # Generate a list of allocations:
        query_url = build_rest_url(base=cli_args.base_rest_url, path=query_path, query_args=query_args)
        json_data = fetch_url(query_url)
        
        # If we're not looking for detail, just print the list:
        if cli_args.should_show_detail:
            projects = []
            for row in json_data:
                if cli_args.output_format in ('csv', 'json'):
                    projects.extend(fetch_url(build_rest_url(base=cli_args.base_rest_url, path='/project/{:d}'.format(row['project_id']))))
                else:
                    row_num = 0
                    for alloc_row in fetch_url(build_rest_url(base=cli_args.base_rest_url, path='/project/{:d}'.format(row['project_id']))):
                        if row_num > 0:
                            alloc_row['project_id'] = None
                            alloc_row['gid'] = None
                            alloc_row['gname'] = None
                            alloc_row['account'] = None
                        projects.append(alloc_row)
                        row_num = row_num + 1
            
            # Build the data array:
            da = data_array(
                        [
                            int_column_formatter('project_id', title='Project id'),
                            column_formatter('account', title='Account'),
                            int_column_formatter('gid', title='Group id'),
                            column_formatter('gname', title='Group name'),
                            int_column_formatter('allocation_id', title='Allocation id'),
                            column_formatter('category', title='Category'),
                            column_formatter('resource', title='RDR'),
                            date_column_formatter('start_date', title='Start date'),
                            date_column_formatter('end_date', title='End date'),
                            date_column_formatter('modification_date', title='Modification date'),
                            date_column_formatter('creation_date', title='Creation date'),
                        ],
                        projects
                    )
            # then display it:
            da.print_array(cli_args.output_format)
        else:
            # Build the data array:
            da = data_array(
                        [
                            int_column_formatter('project_id', title='Project id'),
                            column_formatter('account', title='Account'),
                            int_column_formatter('gid', title='Group id'),
                            column_formatter('gname', title='Group name'),
                            date_column_formatter('modification_date', title='Modification date'),
                            date_column_formatter('creation_date', title='Creation date'),
                        ],
                        json_data
                    )
            # then display it:
            da.print_array(cli_args.output_format)

    except HermesException as E:
        print(str(E))
        sys.exit(1)


elif cli_args.datum == 'jobs':
    query_args = {}
    if cli_args.project_ids is not None:
        query_args['project_id'] = ','.join([str(v) for v in cli_args.project_ids])
    if cli_args.projects is not None:
        query_args['account'] = ','.join(cli_args.projects)
    if cli_args.gids is not None:
        query_args['gid'] = ','.join([str(v) for v in cli_args.gids])
    if cli_args.owner_uids is not None:
        query_args['owner_uid'] = ','.join([str(v) for v in cli_args.owner_uids])
    if cli_args.allocation_ids is not None:
        query_args['allocation_id'] = ','.join([str(v) for v in cli_args.allocation_ids])
    query_path = '/job'
    
    try:
        # Generate a list of jobs:
        query_url = build_rest_url(base=cli_args.base_rest_url, path=query_path, query_args=query_args)
        json_data = fetch_url(query_url)
        
        # Fill-in user ids and allocation descriptions?
        if not cli_args.should_show_numeric:
            json_data = [ { k:(uname_with_uid_number(int(v)) if (v is not None and k == 'owner_uid') else v) for k,v in r.items() } for r in json_data ]
        
            # Add the project+RDR column:
            alt_data = []
            for row in json_data:
                new_row = row.copy()
                new_row['allocation'] = alloc_map.allocation_id_to_projrdr(row['allocation_id'])
                alt_data.append(new_row)
            json_data = alt_data
        
        # Sort?
        if cli_args.sort_by is not None:
            json_data = sorted(json_data, key=lambda k: k[cli_args.sort_by], reverse=cli_args.should_sort_descending) 
        
        # Build the data array:
        if not cli_args.should_show_numeric:
            da = data_array(
                        [
                            int_column_formatter('activity_id', title='Activity id'),
                            int_column_formatter('allocation_id', title='Alloc id'),
                            column_formatter('allocation', title='Alloc descr'),
                            int_column_formatter('job_id', title='Job id'),
                            int_column_formatter('owner_uid', title='Owner') if cli_args.should_show_numeric else column_formatter('owner_uid', title='Owner'),
                            column_formatter('status', title='Status'),
                            int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                            date_column_formatter('modification_date', title='Modification date'),
                            date_column_formatter('creation_date', title='Creation date'),
                        ],
                        json_data
                    )
        else:
            da = data_array(
                        [
                            int_column_formatter('activity_id', title='Activity id'),
                            int_column_formatter('allocation_id', title='Allocation id'),
                            int_column_formatter('job_id', title='Job id'),
                            int_column_formatter('owner_uid', title='Owner') if cli_args.should_show_numeric else column_formatter('owner_uid', title='Owner'),
                            column_formatter('status', title='Status'),
                            int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                            date_column_formatter('modification_date', title='Modification date'),
                            date_column_formatter('creation_date', title='Creation date'),
                        ],
                        json_data
                    )
        # then display it:
        da.print_array(cli_args.output_format)

    except HermesException as E:
        print(str(E))
        sys.exit(1)


elif cli_args.datum == 'failures':
    query_args = {}
    if cli_args.activity_ids is not None:
        query_args['activity_id'] = ','.join([str(v) for v in cli_args.activity_ids])
    if cli_args.project_ids is not None:
        query_args['project_id'] = ','.join([str(v) for v in cli_args.project_ids])
    if cli_args.projects is not None:
        query_args['account'] = ','.join(cli_args.projects)
    if cli_args.gids is not None:
        query_args['gid'] = ','.join([str(v) for v in cli_args.gids])
    if cli_args.owner_uids is not None:
        query_args['owner_uid'] = ','.join([str(v) for v in cli_args.owner_uids])
    if cli_args.allocation_ids is not None:
        query_args['allocation_id'] = ','.join([str(v) for v in cli_args.allocation_ids])
    if cli_args.job_ids is not None:
        query_args['job_id'] = ','.join([str(v) for v in cli_args.job_ids])
    query_path = '/failure'
    
    try:
        # Generate a list of jobs:
        query_url = build_rest_url(base=cli_args.base_rest_url, path=query_path, query_args=query_args)
        json_data = fetch_url(query_url)
        
        # Fill-in user ids?
        if not cli_args.should_show_numeric:
            json_data = [ { k:(uname_with_uid_number(int(v)) if (v is not None and k == 'owner_uid') else v) for k,v in r.items() } for r in json_data ]
        
            # Add the project+RDR column:
            alt_data = []
            for row in json_data:
                new_row = row.copy()
                new_row['allocation'] = alloc_map.allocation_id_to_projrdr(row['allocation_id'])
                alt_data.append(new_row)
            json_data = alt_data
        
        # Sort?
        if cli_args.sort_by is not None:
            json_data = sorted(json_data, key=lambda k: k[cli_args.sort_by], reverse=cli_args.should_sort_descending) 

        
        # Build the data array:
        if cli_args.should_show_detail:
            if not cli_args.should_show_numeric:
                columns = [
                    int_column_formatter('activity_id', title='Activity id'),
                    int_column_formatter('allocation_id', title='Alloc id'),
                    column_formatter('allocation', title='Alloc descr'),
                    int_column_formatter('job_id', title='Job id'),
                    int_column_formatter('owner_uid', title='Owner') if cli_args.should_show_numeric else column_formatter('owner_uid', title='Owner'),
                    int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                    column_formatter('comments', title='Error message'),
                    date_column_formatter('modification_date', title='Modification date'),
                    date_column_formatter('creation_date', title='Creation date'),
                ]

            else:
                columns = [
                    int_column_formatter('activity_id', title='Activity id'),
                    int_column_formatter('allocation_id', title='Allocation id'),
                    int_column_formatter('job_id', title='Job id'),
                    int_column_formatter('owner_uid', title='Owner') if cli_args.should_show_numeric else column_formatter('owner_uid', title='Owner'),
                    int_column_formatter('amount', title='Amount', scalar=su_unit_divider),
                    column_formatter('comments', title='Error message'),
                    date_column_formatter('modification_date', title='Modification date'),
                    date_column_formatter('creation_date', title='Creation date'),
                ]
        else:
            columns = [
                int_column_formatter('job_id', title='Job id'),
                column_formatter('comments', title='Error message'),
            ]
        da = data_array(columns, json_data)
        # then display it:
        da.print_array(cli_args.output_format)

    except HermesException as E:
        print(str(E))
        sys.exit(1)
