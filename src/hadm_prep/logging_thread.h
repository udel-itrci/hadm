/*
 * logging_thread
 * State information associated with each job data logging thread.
 */

#ifndef __LOGGING_THREAD_H__
#define __LOGGING_THREAD_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

// We make use of CURL for the HTTP requests:
#include <curl/curl.h>

// MUNGE is necessary for encoding the REST API URLs as authnz:
#include <munge.h>

// Incrementally-updated statistics are used to make automated
// performance-altering decisions.
#include "incremental_stats.h"

#ifndef LOGGING_THREAD_CONNECT_TIMEOUT
/*!
 * @defined LOGGING_THREAD_CONNECT_TIMEOUT
 * @discussion
 *     The default timeout (in seconds) for CURL to open a TCP connection to
 *     the web server.
 */
#define LOGGING_THREAD_CONNECT_TIMEOUT           300
#endif /* LOGGING_THREAD_CONNECT_TIMEOUT */

#ifndef LOGGING_THREAD_REQUEST_TIMEOUT
/*!
 * @defined LOGGING_THREAD_REQUEST_TIMEOUT
 * @discussion
 *     The default timeout (in seconds) for CURL to complete an HTTP request
 *     on an open TCP connection to the web server.
 */
#define LOGGING_THREAD_REQUEST_TIMEOUT           900
#endif /* LOGGING_THREAD_REQUEST_TIMEOUT */

#ifndef LOGGING_THREAD_QUEUE_WAIT_THRESHOLD
/*!
 * @defined LOGGING_THREAD_QUEUE_WAIT_THRESHOLD
 * @discussion
 *     The default time (in seconds) above which queue wait time will trigger
 *     the spawning of additional logging threads.
 */
#define LOGGING_THREAD_QUEUE_WAIT_THRESHOLD      300
#endif /* LOGGING_THREAD_QUEUE_WAIT_THRESHOLD */

#ifndef LOGGING_THREAD_MIN_THREADS
/*!
 * @defined LOGGING_THREAD_MIN_THREADS
 * @discussion
 *     The default minimum number of queue-processing logging threads that
 *     a controller should spawn.
 */
#define LOGGING_THREAD_MIN_THREADS                 1
#endif /* LOGGING_THREAD_MIN_THREADS */

#ifndef LOGGING_THREAD_MAX_THREADS
/*!
 * @defined LOGGING_THREAD_MAX_THREADS
 * @discussion
 *     The default maximum number of queue-processing logging threads that
 *     a controller is allowed to spawn.
 */
#define LOGGING_THREAD_MAX_THREADS                10
#endif /* LOGGING_THREAD_MAX_THREADS */

#ifndef LOGGING_THREAD_MAX_REQUESTS_PER_THREAD
/*!
 * @defined LOGGING_THREAD_MAX_REQUESTS_PER_THREAD
 * @discussion
 *     The default number of requests a logging thread should process before
 *     being terminated (and possibly replaced by a new thread).
 *
 *     The value 0 implies no limit.
 */
#define LOGGING_THREAD_MAX_REQUESTS_PER_THREAD  1000
#endif /* LOGGING_THREAD_MAX_REQUESTS_PER_THREAD */

#ifndef LOGGING_THREAD_MAX_TIME_PER_THREAD
/*!
 * @defined LOGGING_THREAD_MAX_TIME_PER_THREAD
 * @discussion
 *     The default time interval (in seconds) a logging thread should process
 *     queue records before being terminated (and possibly replaced by a new
 *     thread).
 *
 *     The value 0 implies no limit.
 */
#define LOGGING_THREAD_MAX_TIME_PER_THREAD         0
#endif /* LOGGING_THREAD_MAX_TIME_PER_THREAD */

#ifndef LOGGING_THREAD_MAINTENANCE_INTERVAL
/*!
 * @defined LOGGING_THREAD_MAINTENANCE_INTERVAL
 * @discussion
 *     The default time interval (in seconds) between automatic checks for
 *     threads that have exceeded their lifespan, spawning of new threads,
 *     etc.
 */
#define LOGGING_THREAD_MAINTENANCE_INTERVAL      120
#endif /* LOGGING_THREAD_MAINTENANCE_INTERVAL */

#ifndef LOGGING_THREAD_TIMEDJOIN_TIMEOUT
/*!
 * @defined LOGGING_THREAD_TIMEDJOIN_TIMEOUT
 * @discussion
 *     The default time interval (in seconds) to wait for a pthread_timedjoin_np()
 *     to notice the thread's having exited.
 */
#define LOGGING_THREAD_TIMEDJOIN_TIMEOUT              135
#endif /* LOGGING_THREAD_TIMEDJOIN_TIMEOUT */

#ifndef LOGGING_THREAD_TIMEDJOIN_RETRY
/*!
 * @defined LOGGING_THREAD_TIMEDJOIN_RETRY
 * @discussion
 *     The number of times to defer cancellation of a logging thread for
 *     exceeding its join timeout.
 */
#define LOGGING_THREAD_TIMEDJOIN_RETRY                  3
#endif /* LOGGING_THREAD_TIMEDJOIN_RETRY */

#ifndef LOGGING_THREAD_REST_API_BASE_URL
/*!
 * @defined LOGGING_THREAD_REST_API_BASE_URL
 * @discussion
 *     The default REST API URL for job data logging requests.
 */
#define LOGGING_THREAD_REST_API_BASE_URL        "http://hadm-api:8000/job"
#endif /* LOGGING_THREAD_REST_API_BASE_URL */


/*!
 * @typedef logging_thread_t
 * @discussion
 *     Data structure holding all information associated with a job data
 *     logging thread.
 *
 *     The thread has an initial start_time timestamp that can be used to
 *     determine the age of the thread, and a last_request_time timestamp
 *     that gets updated each time the thread attempts to handle a job data
 *     record.
 *
 *     Each thread also maintains the number of requests it has handled.
 *
 *     The control thread can look at the lifetime or request count for a thread
 *     as criteria for terminating it.
 *
 *     A CURL simple request handle is used to send HTTP requests to the API
 *     server.
 *
 *     The thread_mutex is used to serialize state changes to the job data
 *     logging thread -- e.g. setting it to exit.
 */
typedef struct logging_thread {
    struct logging_thread_control   *parent;
    struct logging_thread           *link;
    
    pthread_mutex_t                 thread_mutex;
    pthread_t                       the_thread;
    bool                            should_exit;
    int                             join_defer_count;
    
    CURL*                           curl_handle;
    munge_ctx_t                     munge_context;
    
    time_t                          start_time;
    time_t                          last_request_time;
    unsigned long long              n_requests;
} logging_thread_t;

/*!
 * @defined logging_thread_request_rate
 * @discussion
 *     Calculate the rate of requests handled by logging_thread, T.  Value
 *     is in units of requests per second.
 */
#define logging_thread_request_rate(T) \
            (((T)->last_request_time > (T)->start_time) ? ((double)(T)->n_requests / ((T)->last_request_time - (T)->start_time)) : 0.0)

/*!
 * @defined logging_thread_is_lifespan_surpassed
 * @discussion
 *     Determine whether a logging_thread, T, has exceeded a lifespan of
 *     requests, MN, or elapsed time, MT.
 */
#define logging_thread_is_lifespan_surpassed(T, MT, MN) \
            ( (((MN) > 0) && ((T)->n_requests >= (MN))) || (((MT) > 0.0) && ((time(NULL) - (T)->start_time) > (MT))) )


/*!
 * @typedef logging_type_prep_callback_type_t
 * @discussion
 *     Enumeration of the slurmctld PREP callbacks that must be
 *     used to complete async PREP events.
 */
typedef enum {
    logging_thread_prep_callback_type_prolog = 0,
    logging_thread_prep_callback_type_epilog,
    logging_thread_prep_callback_type_max
} logging_type_prep_callback_type_t;
typedef void (*logging_thread_prep_callback)(int rc, uint32_t job_id);

/*!
 * @function logging_thread_register_prep_callback
 * @discussion
 *     Register a slurmctld PREP callback function pointer with this
 *     API.  Should be called by the PREP plugin at startup to register
 *     all of the callback functions.
 */
void logging_thread_register_prep_callback(logging_type_prep_callback_type_t callback_type, logging_thread_prep_callback callback_ptr);


#ifndef JOB_DATA_RECORD_ACCOUNT_CAPACITY
/*!
 * @defined JOB_DATA_RECORD_ACCOUNT_CAPACITY
 * @discussion
 *     The maximum length of Slurm account names in the job_data_t
 *     struct.
 */
#define JOB_DATA_RECORD_ACCOUNT_CAPACITY  32
#endif

/*!
 * @typedef job_data_variant_t
 * @discussion
 *     The type of logging to be done:
 * @constant job_data_variant_predebit
 *     A pre-debit of the job's projected SU usage
 * @constant job_data_variant_completion
 *     Completion of a pre-debited job with real SU usage
 * @constant job_data_variant_max
 *     Maximum value in enumeration; used as terminal index in loops, etc.
 */
typedef enum {
    job_data_variant_predebit = 0,
    job_data_variant_completion,
    job_data_variant_max
} job_data_variant_t;

/*!
 * @typedef job_rdr_t
 * @discussion
 *     Enumeration of the resource types that job's can debit.
 * @constant job_rdr_cpu
 *     CPU-centric resources
 * @constant job_rdr_gpu
 *     GPU-centric resources
 */
typedef enum {
    job_rdr_unknown = -1,
    job_rdr_cpu = 0,
    job_rdr_gpu,
    job_rdr_max
} job_rdr_t;

/*!
 * @typedef job_data_t
 * @discussion
 *     A fixed-size data structure containing the attributes of a job.
 *
 *     The billed_su is not assumed to have any unit associated with it; it
 *     is up to the caller to properly-scale the value before passing a
 *     job_data_t to a thread_controller.
 */
typedef struct job_data {
    job_data_variant_t          job_data_variant;
    uint32_t                    job_id;
    char                        account[JOB_DATA_RECORD_ACCOUNT_CAPACITY];
    uid_t                       owner_uid;
    uint64_t                    billed_su;
    job_rdr_t                   rdr_type;
    // For logging failures:
    uint8_t                     open_mode;
    const char                  *std_err;
} job_data_t;

/*!
 * @typedef job_data_record_t
 * @discussion
 *     Wrapper around job_data_t structure for the sake of queueing
 *     records.
 *
 *     Incoming job data is always copied into the job_data
 *     field.
 *
 *     The queued_time and assigned_time are used to determine the queue wait
 *     time on a record and the request-processing time.
 */
typedef struct job_data_record {
    struct job_data_record      *link;
    
    logging_thread_t            *assigned_thread;
    time_t                      queued_time;
    time_t                      assigned_time;
    job_data_t                  job_data;
} job_data_record_t;

/*!
 * @typedef logging_thread_control_t
 * @discussion
 *     An instance of logging_thread_control_t embodies a queue of job data
 *     records; a set of threads that can process the queued job data records;
 *     a maintenance thread responsible for spawning/terminating data logging
 *     threads; and maintaining statistics w.r.t. the entire process to determine
 *     when/if additional threads should be spawned, etc.
 *
 *     The timeout, min/max thread count, and lifespan parameters are set to the
 *     default values cited above, possibly overridden by an options string passed
 *     to the initialization function.
 *
 *     Alterations to the linked list of logging threads as well as the maintenance
 *     thread is controlled by the thread_mutex.  The maintenance_wakeup is used to
 *     break the maintenance thread out of its sleep interval early (e.g. on shutdown).
 *
 *     Alterations to the data queue and the timing stats are controlled by
 *     the queue_mutex.  The queue_cond is used to wakeup logging threads when new
 *     data records are added to the queue.
 */
typedef struct logging_thread_control {
    time_t                          start_time;
    long                            connection_timeout;
    long                            request_timeout;
    long                            queue_wait_threshold;
    unsigned int                    min_threads, max_threads;
    struct {
        unsigned long long          max_requests;
        time_t                      max_time;
    } thread_lifespan;
    long                            timedjoin_timeout;
    long                            timedjoin_retry;
    const char                      *rest_api_base_url;
    size_t                          rest_api_base_url_len;
    
    pthread_mutex_t                 threads_mutex;
    unsigned int                    n_threads;
    logging_thread_t                *threads;
    long                            maintenance_interval;
    bool                            should_maintenance_exit;
    pthread_t                       maintenance_thread;
    pthread_cond_t                  maintenance_wakeup;
    
    pthread_mutex_t                 queue_mutex;
    pthread_cond_t                  queue_cond;
    bool                            is_queue_disabled;
    unsigned int                    n_queued, n_in_progress, n_bulk;
    job_data_record_t               *queued_records, *queued_tail;
    job_data_record_t               *in_progress_records;
    job_data_record_t               *unused_job_data_records;
    incremental_stat_t              queue_timing_stats;
    incremental_stat_t              request_timing_stats;
} logging_thread_control_t;

/*!
 * @function logging_thread_control_init
 * @discussion
 *     Allocate and initialize a new logging_thread_control_t instance.
 *
 *     The options string can be NULL; if not NULL, it should contain a sequence
 *     of name=value options separated by whitespace:
 *
 *          "lifespan.max_requests=0 lifespan.max_time=0 interval.maintenance=10"
 * @result
 *     Returns NULL if the new controller could not be allocated and successfully
 *     initialized.
 */
logging_thread_control_t* logging_thread_control_init(const char *options);

/*!
 * @function logging_thread_control_fini
 * @discussion
 *     Shutdown the maintenance and logging threads associated with thread_controller.
 *     The logging_thread_t instances are finalized, and all job data records associated
 *     with thread_controller are free'd.  Finally, thread_controller itself is free'd.
 * @result
 *     Returns bool false if there were issues shutting-down and deallocating the
 *     thread_controller.
 */
bool logging_thread_control_fini(logging_thread_control_t *thread_controller);

/*!
 * @function logging_thread_control_push_job_data
 * @discussion
 *    Add the job data in job_data to the queue associated with
 *    thread_contoller.  If successful, the logging threads are signalled that there is
 *    data waiting (via queue_cond) and bool true is returned.
 * @result
 *    Returns bool false if the data could not be pushed into the queue. 
 */
bool logging_thread_control_push_job_data(logging_thread_control_t *thread_controller, job_data_t *job_data);

/*!
 * @function logging_thread_control_push_job_data_bulk
 * @discussion
 *    Add the job data in job_data to the queue associated with
 *    thread_contoller.  If successful and is_last_record is true, the logging threads
 *    are signalled that there is data waiting (via queue_cond).
 * @result
 *    Returns bool false if the data could not be pushed into the queue, bool true
 *    otherwise.
 */
bool logging_thread_control_push_job_data_bulk(logging_thread_control_t *thread_controller, job_data_t *job_data, bool is_last_record);

/*!
 * @function logging_thread_control_check_threads
 * @discussion
 *    Check thread_controller for logging threads that have exceeded their lifespan and
 *    terminate them.  Spawn new logging threads if necessary.
 * @result
 *     Returns bool true if no issues occurred.
 */
bool logging_thread_control_check_threads(logging_thread_control_t *thread_controller);

/*!
 * @function logging_thread_control_unserialize_queue_from_file
 * @discussion
 *     Attempt to open src_file_path and read-in serialized job data records, adding them into
 *     the queue associated with thread_controller.
 *
 *     Used to restore saved-state after a restart of slurmctld, for example.
 * @result
 *     Returns bool true if no issues occurred.
 */
bool logging_thread_control_unserialize_queue_from_file(logging_thread_control_t *thread_controller, const char *src_file_path);

/*!
 * @function logging_thread_control_serialize_queue_to_file
 * @discussion
 *    Wait for all in-progress job data record transactions to complete, then write any remaining
 *    queue records to dst_file_path.
 *
 *    If any error is encountered while opening/writing the file, the records will be dumped to the
 *    error output stream (e.g. in the Slurm plugin to the Slurm error() function and log file).
 *
 *     Used to save uncompleted state when slurmctld is exiting, for example.
 * @result
 *     Returns bool true if no issues occurred.
 */
bool logging_thread_control_serialize_queue_to_file(logging_thread_control_t *thread_controller, const char *dst_file_path);

#endif /* __LOGGING_THREAD_H__ */
