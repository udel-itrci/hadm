/*
 * logging_thread
 * State information associated with each job data logging thread.
 */
 
#include "logging_thread.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <json-c/json.h>

#ifdef LOGGING_THREAD_TESTING
#   include <limits.h>
#   ifdef LOGGING_THREAD_DEBUG
#       define DEBUG(FMT, ...) { char ts[32]; time_t t = time(NULL); struct tm tss; localtime_r(&t, &tss); strftime(ts, sizeof(ts), "%Y%d%mT%H%M%S", &tss); fprintf(stderr, "%s DEBUG(%s:%d) " FMT "\n", ts, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stderr); }
#   else
#       define DEBUG(FMT, ...)
#   endif /* LOGGING_THREAD_DEBUG */
#   define ERROR(FMT, ...) { char ts[32]; time_t t = time(NULL); struct tm tss; localtime_r(&t, &tss); strftime(ts, sizeof(ts), "%Y%d%mT%H%M%S", &tss); fprintf(stderr, "%s ERROR(%s:%d) " FMT "\n", ts, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stderr); }
#   define INFO(FMT, ...) { char ts[32]; time_t t = time(NULL); struct tm tss; localtime_r(&t, &tss); strftime(ts, sizeof(ts), "%Y%d%mT%H%M%S", &tss); fprintf(stderr, "%s INFO(%s:%d)  " FMT "\n", ts, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stderr); }
#else
#   include "slurm/slurm.h"
#   include "slurm/slurm_errno.h"
#   include "src/common/log.h"
#   include "src/common/slurm_xlator.h"
#   define DEBUG debug3
#   define ERROR error
#   define INFO  info
#endif

//

enum {
    logging_thread_join_success = 0,
    logging_thread_join_defer,
    logging_thread_join_failure
};

//

void __logging_thread_control_reset_job_data_record(logging_thread_control_t *thread_controller, job_data_record_t *record_to_zero, bool should_return_to_unused);

//

enum {
    rest_api_err_code_generic               = -1,
    rest_api_err_database                   = -2,
    
    rest_api_err_code_invalid_auth          = -100,
    rest_api_err_code_missing_auth          = -101,
    rest_api_err_code_not_authorized        = -102,
    
    rest_api_err_code_invalid_data          = -200,
    rest_api_err_code_incomplete_data       = -201,
    rest_api_err_code_unknown_job_id        = -202,
    rest_api_err_code_no_allocation_avail   = -203
} rest_api_err_code_t;

//

static logging_thread_prep_callback     __logging_thread_prep_callbacks[logging_thread_prep_callback_type_max] = {
                                                NULL,
                                                NULL
                                            };

void
logging_thread_register_prep_callback(
    logging_type_prep_callback_type_t   callback_type,
    logging_thread_prep_callback        callback_ptr
)
{
    __logging_thread_prep_callbacks[callback_type] = callback_ptr;
}

//

const char logging_thread_rest_munge_header_name[] = "MungeUrlHash";
const size_t logging_thread_rest_munge_header_name_len = sizeof("MungeUrlHash") - 1;

#define LOGGING_THREAD_CONTROL_DECL_COMPLETION_URL_VAR(VAR_NAME, THREAD_CONTROL_PTR, JOB_ID) \
        size_t      VAR_NAME##_len = (THREAD_CONTROL_PTR)->rest_api_base_url_len + 1 + 10 + 1; \
        const char  VAR_NAME[VAR_NAME##_len]; \
        snprintf((char*)VAR_NAME, VAR_NAME##_len, "%s/%u", (THREAD_CONTROL_PTR)->rest_api_base_url, (JOB_ID));

#define LOGGING_THREAD_CONTROL_DECL_PREDEBIT_URL_VAR(VAR_NAME, THREAD_CONTROL_PTR, JOB_ID) \
        size_t      VAR_NAME##_len = (THREAD_CONTROL_PTR)->rest_api_base_url_len + 1; \
        const char  VAR_NAME[VAR_NAME##_len]; \
        strncpy((char*)VAR_NAME, (THREAD_CONTROL_PTR)->rest_api_base_url, VAR_NAME##_len);


/*!
 * @function __logging_thread_control_pop_job_data_data
 *
 * Shift the next job data record in the thread_controller queue to the
 * in-progress state.  The record's time-in-queue is calculated and added to
 * the queue-wait statistics.
 *
 * The fields in the job_data_data_t pointer returned should NOT be modified
 * by the caller.  The assigned_thread field is set to calling_thread, so only
 * the calling_thread can later finalize the record.
 *
 * Pre-condition:  the queue_mutex is locked by the caller.
 */
job_data_t*
__logging_thread_control_pop_job_data(
    logging_thread_control_t    *thread_controller,
    logging_thread_t            *calling_thread
)
{
    // On entry the queue_mutex is held by a thread, so we DO NOT attempt to do anything
    // with locking inside this function!
    if ( ! thread_controller->is_queue_disabled && thread_controller->queued_records ) {
        job_data_record_t     *next_record = thread_controller->queued_records;
        time_t                  now = time(NULL);
        double                  dt;
        
        // Pop this record off the queue:
        thread_controller->queued_records = next_record->link;
        if ( ! thread_controller->queued_records ) thread_controller->queued_tail = NULL;
        
        // Associate this record with the calling thread:
        next_record->assigned_thread = calling_thread;
        
        // Push onto the in-progress list:
        next_record->link = thread_controller->in_progress_records;
        thread_controller->in_progress_records = next_record;
        
        // Update the counts:
        thread_controller->n_queued--;
        thread_controller->n_in_progress++;
        
        DEBUG("THREAD %p:  UPDATED QUEUE ON POP:  n_queued = %u, n_in_progress = %u, n_bulk = %u", calling_thread->the_thread, thread_controller->n_queued, thread_controller->n_in_progress, thread_controller->n_bulk);
        
        // Update the time-in-queue stats as well as the assignement
        // timestamp on the record:
        next_record->assigned_time = now;
        dt = now - next_record->queued_time;
        DEBUG("THREAD %p:  %ld - %ld = ∆t(queue) = %lg", calling_thread->the_thread, now, next_record->queued_time, dt);
        incremental_stat_append(thread_controller->queue_timing_stats, dt);
        
        // Hand the data back to the caller:
        return &next_record->job_data;
    }
    return NULL;
}

/*!
 * @function __logging_thread_control_requeue_job_data
 *
 * Shift the job_data record in the thread_controller queue out of the
 * in-progress state back to the queued state.  This would happen if the assigned
 * thread fails to complete the data logging.
 *
 * The queued and assigned times are reset.
 *
 * Pre-condition:  the queue_mutex is locked by the caller.
 */
void
__logging_thread_control_requeue_job_data(
    logging_thread_control_t    *thread_controller,
    logging_thread_t            *calling_thread,
    job_data_t                  *job_data
)
{
    // On entry the queue_mutex is held by a thread, so we DO NOT attempt to do anything
    // with locking inside this function!
    job_data_record_t           *in_progress_list = thread_controller->in_progress_records;
    job_data_record_t           *last_record = NULL;
    
    while ( in_progress_list ) {
        if ( (calling_thread == in_progress_list->assigned_thread) && (job_data == &in_progress_list->job_data) ) break;
        last_record = in_progress_list;
        in_progress_list = in_progress_list->link;
    }
    // Found it?
    if ( in_progress_list ) {
        // Unlink the record and push back to the queued list:
        if ( last_record ) {
            last_record->link = in_progress_list->link;
        } else {
            thread_controller->in_progress_records = in_progress_list->link;
        }
        in_progress_list->assigned_thread = NULL;
        in_progress_list->queued_time = time(NULL);
        in_progress_list->assigned_time = 0;
        in_progress_list->link = NULL;
        
        if ( thread_controller->queued_tail ) {
            thread_controller->queued_tail->link = in_progress_list;
            thread_controller->queued_tail = in_progress_list;
        } else {
            thread_controller->queued_records = thread_controller->queued_tail = in_progress_list;
        }
        
        // Update counts:
        thread_controller->n_queued++;
        thread_controller->n_in_progress--;
        DEBUG("THREAD %p:  UPDATED QUEUE ON REQUEUE:  n_queued = %u, n_in_progress = %u, n_bulk = %u", calling_thread->the_thread, thread_controller->n_queued, thread_controller->n_in_progress, thread_controller->n_bulk);
    } else {
        DEBUG("THREAD %p:  JOB DATA RECORD NOT FOUND?!?!", calling_thread->the_thread);
    }
}

/*!
 * @function __logging_thread_control_fini_job_data
 *
 * Shift the job_data record in the thread_controller queue out of the
 * in-progress state.  The record is scrubbed and returned to the allocation
 * pool.
 *
 * The total request processing time (from the time the record was assigned to
 * a thread until now) is added to the request time statistics.
 *
 * Pre-condition:  the queue_mutex is locked by the caller.
 */
void
__logging_thread_control_fini_job_data(
    logging_thread_control_t    *thread_controller,
    logging_thread_t            *calling_thread,
    job_data_t                  *job_data
)
{
    // On entry the queue_mutex is held by a thread, so we DO NOT attempt to do anything
    // with locking inside this function!
    job_data_record_t         *in_progress_list = thread_controller->in_progress_records;
    job_data_record_t         *last_record = NULL;
    
    while ( in_progress_list ) {
        if ( (calling_thread == in_progress_list->assigned_thread) && (job_data == &in_progress_list->job_data) ) break;
        last_record = in_progress_list;
        in_progress_list = in_progress_list->link;
    }
    // Found it?
    if ( in_progress_list ) {
        time_t                  now = time(NULL);
        double                  dt;
        
        DEBUG("THREAD %p:  FOUND JOB DATA RECORD TO REMOVE", calling_thread->the_thread);
        
        // Update the request time stats:
        dt = now - in_progress_list->assigned_time;
        DEBUG("THREAD %p:  %ld - %ld = ∆t(request) = %lg", calling_thread->the_thread, now, in_progress_list->assigned_time, dt);
        incremental_stat_append(thread_controller->request_timing_stats, dt);
        
        // Scrub the record, push back to the unused list:
        
        if ( last_record ) {
            last_record->link = in_progress_list->link;
        } else {
            thread_controller->in_progress_records = in_progress_list->link;
        }
        __logging_thread_control_reset_job_data_record(thread_controller, in_progress_list, true);
        
        // Update counts:
        thread_controller->n_in_progress--;
        DEBUG("THREAD %p:  UPDATED QUEUE ON FINI:  n_queued = %u, n_in_progress = %u, n_bulk = %u", calling_thread->the_thread, thread_controller->n_queued, thread_controller->n_in_progress, thread_controller->n_bulk);
    } else {
        DEBUG("THREAD %p:  JOB DATA RECORD NOT FOUND?!?!", calling_thread->the_thread);
    }
}

/*!
 * @function __logging_thread_control_pushback_job_data
 *
 * Shift the job_data record in the thread_controller queue out of the
 * in-progress state and back into a queued state.
 *
 * The total request processing time (from the time the record was assigned to
 * a thread until now) is added to the request time statistics.
 *
 * Pre-condition:  the queue_mutex is locked by the caller.
 */
void
__logging_thread_control_pushback_job_data(
    logging_thread_control_t    *thread_controller,
    logging_thread_t            *calling_thread,
    job_data_t                  *job_data
)
{
    // On entry the queue_mutex is held by a thread, so we DO NOT attempt to do anything
    // with locking inside this function!
    job_data_record_t         *in_progress_list = thread_controller->in_progress_records;
    job_data_record_t         *last_record = NULL;
    
    while ( in_progress_list ) {
        if ( (calling_thread == in_progress_list->assigned_thread) && (job_data == &in_progress_list->job_data) ) break;
        last_record = in_progress_list;
        in_progress_list = in_progress_list->link;
    }
    // Found it?
    if ( in_progress_list ) {
        time_t                  now = time(NULL);
        double                  dt;
        
        DEBUG("THREAD %p:  FOUND JOB DATA RECORD TO PUSHBACK", calling_thread->the_thread);
        
        // Update the request time stats:
        dt = now - in_progress_list->assigned_time;
        DEBUG("THREAD %p:  %ld - %ld = ∆t(request) = %lg", calling_thread->the_thread, now, in_progress_list->assigned_time, dt);
        incremental_stat_append(thread_controller->request_timing_stats, dt);
        
        // Scrub the assignment fields and push to the end of the queue:
        if ( last_record ) {
            last_record->link = in_progress_list->link;
        } else {
            thread_controller->in_progress_records = in_progress_list->link;
        }
        in_progress_list->assigned_time = 0;
        in_progress_list->assigned_thread = NULL;
        in_progress_list->queued_time = time(NULL);
        in_progress_list->link = NULL;
        
        if ( thread_controller->queued_tail ) {
            thread_controller->queued_tail->link = in_progress_list;
            thread_controller->queued_tail = in_progress_list;
        } else {
            thread_controller->queued_records = thread_controller->queued_tail = in_progress_list;
        }
        
        // Update counts:
        thread_controller->n_queued++;
        thread_controller->n_in_progress--;
        DEBUG("THREAD %p:  UPDATED QUEUE ON PUSHBACK:  n_queued = %u, n_in_progress = %u, n_bulk = %u", calling_thread->the_thread, thread_controller->n_queued, thread_controller->n_in_progress, thread_controller->n_bulk);
        
        // Try to get a thread awakened to handle the next record...
        pthread_cond_signal(&thread_controller->queue_cond);
    } else {
        DEBUG("THREAD %p:  JOB DATA RECORD NOT FOUND?!?!", calling_thread->the_thread);
    }
}

/*!
 * @typedef logging_thread_http_response_context
 *
 * HTTP response parsing context, accumulates parsed JSON in parsed_object.
 */
typedef struct {
    json_object     *parsed_object;
    json_tokener    *parser;
} logging_thread_http_response_context;

/*!
 * @function __logging_thread_http_response_callback
 *
 * CURL callback function to process the HTTP response body, producing a JSON
 * object across one or more calls.
 */
size_t
__logging_thread_http_response_callback(
    void        *content,
    size_t      block_size,
    size_t      block_count,
    void        *context
)
{
    logging_thread_http_response_context    *CONTEXT = (logging_thread_http_response_context*)context;
    size_t                                  content_len = block_size * block_count;
    
    char    *p = content;
    size_t  p_len = content_len;
    
    CONTEXT->parsed_object = json_tokener_parse_ex(CONTEXT->parser, content, content_len);
    switch ( json_tokener_get_error(CONTEXT->parser) ) {
    
        case json_tokener_continue:
        case json_tokener_success:
            return content_len;
        
    }
    return 0;
}

/*!
 * @function __logging_thread_http_body_callback
 *
 * CURL callback function to send the HTTP body.
 */
size_t
__logging_thread_http_body_callback(
    void        *buffer,
    size_t      block_size,
    size_t      block_count,
    void        *context
)
{
    char        **CONTEXT = (char**)context;
    size_t      out_bytes_read = 0;
    
    if ( **CONTEXT != '\0' ) {
        out_bytes_read = strlen(*CONTEXT);
    
        if ( out_bytes_read > block_size * block_count ) {
            // Partial read:
            out_bytes_read = block_size * block_count;
        }
        memcpy(buffer, *CONTEXT, out_bytes_read);
        *CONTEXT += out_bytes_read;
    }
    return out_bytes_read;
}

/*!
 * @function __logging_thread_http_body_from_job_data
 *
 * Generate the JSON body that will be PUT to the REST API to complete the job.  The
 * caller is responsible for free'ing the returned pointer.
 */
char*
__logging_thread_http_body_from_job_data(
    job_data_t      *job_data,
    size_t          *out_body_len
)
{
    static const char*  rdr_type_strs[job_rdr_max] = {
                                "cpu",
                                "gpu"
                            };
    static const char*  json_formats[job_data_variant_max] = {
                                "{\"job_id\":%1$u,\"owner_uid\":%2$u,\"account\":\"%3$s\",\"amount\":%4$llu,\"resource\":\"%5$s\"}",
                                "{\"job_id\":%1$u,\"owner_uid\":%2$u,\"account\":\"%3$s\",\"status\":\"completed\",\"amount\":%4$llu}"
                            };
    size_t              json_len;
    char                *json_body = NULL;
    
    json_len = snprintf(NULL, 0, json_formats[job_data->job_data_variant],
                    (unsigned int)job_data->job_id,
                    (unsigned int)job_data->owner_uid,
                    job_data->account,
                    (unsigned long long)job_data->billed_su,
                    rdr_type_strs[job_data->rdr_type]
                );
    if ( json_len > 0 ) {
        if ( (json_body = malloc(json_len + 1)) ) {
            *out_body_len = snprintf(json_body, json_len + 1, json_formats[job_data->job_data_variant],
                    (unsigned int)job_data->job_id,
                    (unsigned int)job_data->owner_uid,
                    job_data->account,
                    (unsigned long long)job_data->billed_su,
                    rdr_type_strs[job_data->rdr_type]
                );
        }
    }
    return json_body;
}
/*!
 * @function __send_predebit_data
 *
 * Handle the HTTP REST API transaction for a job pre-debit record.
 *
 */
bool
__send_predebit_data(
    logging_thread_t    *thread,
    job_data_t          *data_to_log
)
{
    bool                was_logged = false;
    char                *munge_header_value = NULL;
    size_t              munge_header_len;
    munge_err_t         munge_err;
    
    //
    // Setup the REST API URL for the job to which we're responding:
    //
    LOGGING_THREAD_CONTROL_DECL_PREDEBIT_URL_VAR(rest_url, thread->parent, data_to_log->job_id);

    // Update the lifespan fields for this thread:
    thread->last_request_time = time(NULL);
    thread->n_requests++;
    
    // Munge-encode the REST API URL:
    if ( (munge_err = munge_encode(&munge_header_value, thread->munge_context, rest_url, strlen(rest_url))) != EMUNGE_SUCCESS ) {
        ERROR("THREAD %p: unable to MUNGE-encode REST API URL (err = %d)", thread->the_thread, munge_err);
        goto early_exit;
    }
    if ( munge_header_value ) {
        size_t                                  munge_header_len = logging_thread_rest_munge_header_name_len + 2 + strlen(munge_header_value) + 1;
        char                                    munge_header[munge_header_len], *http_response = NULL;
        char                                    *json_post = NULL;
        logging_thread_http_response_context    parsing_context;
    
        snprintf(munge_header, munge_header_len, "%s: %s", logging_thread_rest_munge_header_name, munge_header_value);
            
        // Setup the JSON parsing context:
        parsing_context.parser = json_tokener_new();
        if ( parsing_context.parser ) {
            size_t                              json_body_len = 0;
            char                                *json_body = __logging_thread_http_body_from_job_data(data_to_log, &json_body_len);
            
            if ( json_body ) {
                struct curl_slist               *http_headers = NULL;
                CURLcode                        http_err;
        
                DEBUG("THREAD %p: job_data@%p JSON[%s] url = %s", thread->the_thread, data_to_log, json_body, rest_url);
                
                parsing_context.parsed_object = NULL;
            
                // Setup the CURL handle for the request:
                curl_easy_setopt(thread->curl_handle, CURLOPT_URL, rest_url);
                curl_easy_setopt(thread->curl_handle, CURLOPT_POST, 1L);
                curl_easy_setopt(thread->curl_handle, CURLOPT_POSTFIELDS, json_body);
                curl_easy_setopt(thread->curl_handle, CURLOPT_POSTFIELDSIZE, (long)json_body_len);
                curl_easy_setopt(thread->curl_handle, CURLOPT_NOSIGNAL, 1L);
                if ( thread->parent->request_timeout > 0 ) curl_easy_setopt(thread->curl_handle, CURLOPT_TIMEOUT, thread->parent->request_timeout);
                if ( thread->parent->connection_timeout > 0 ) curl_easy_setopt(thread->curl_handle, CURLOPT_CONNECTTIMEOUT, thread->parent->connection_timeout);
                http_headers = curl_slist_append(http_headers, "Expect:");
                http_headers = curl_slist_append(http_headers, "Content-Type: application/json");
                http_headers = curl_slist_append(http_headers, munge_header);
                curl_easy_setopt(thread->curl_handle, CURLOPT_HTTPHEADER, http_headers);
                curl_easy_setopt(thread->curl_handle, CURLOPT_WRITEFUNCTION, __logging_thread_http_response_callback);
                curl_easy_setopt(thread->curl_handle, CURLOPT_WRITEDATA, (void*)&parsing_context);
                
                // Okay, preform the HTTP request:
                DEBUG("THREAD %p: curl_easy_perform(\"%s\") ...", thread->the_thread, rest_url);
                http_err = curl_easy_perform(thread->curl_handle);
                DEBUG("THREAD %p: ... curl_easy_perform(\"%s\") = %d", thread->the_thread, rest_url, http_err);
                
                if ( http_err == CURLE_OK ) {
                    // Is the response JSON object present?
                    if ( parsing_context.parsed_object ) {
                        if ( json_object_get_type(parsing_context.parsed_object) == json_type_object ) {
                            // Does the 'err_msg' key exist?
                            struct json_object  *err_msg_obj = NULL;
                        
                            if ( json_object_object_get_ex(parsing_context.parsed_object, "err_msg", &err_msg_obj) && err_msg_obj ) {
                                struct json_object  *err_code_obj = NULL;
                            
                                json_object_object_get_ex(parsing_context.parsed_object, "err_code", &err_code_obj);
                                if ( err_code_obj ) {
                                    ERROR("THREAD %p:  Failed to pre-debit job %u:  %s (err_code = %d)",
                                            thread->the_thread, data_to_log->job_id, json_object_get_string(err_msg_obj), json_object_get_int(err_code_obj)
                                        );
                                } else {
                                    ERROR("THREAD %p:  Failed to pre-debit job %u:  %s",
                                            thread->the_thread, data_to_log->job_id, json_object_get_string(err_msg_obj)
                                        );
                                }
#ifndef LOGGING_THREAD_TESTING
                                /*
                                 * Any error from the REST API should be interpreted as implying the job
                                 * cannot run; return ESLURM_ACCOUNTING_POLICY to PREP framework.
                                 */
                                if ( __logging_thread_prep_callbacks[logging_thread_prep_callback_type_prolog] ) {
                                    __logging_thread_prep_callbacks[logging_thread_prep_callback_type_prolog](ESLURM_ACCOUNTING_POLICY, data_to_log->job_id);
                                }

#   ifdef LOGGING_THREAD_LOG_TO_JOB_STDERR
                                // Try to log to the job's std_err:
                                if ( data_to_log->std_err ) {
                                    FILE        *std_err_fptr = NULL;
                                    
                                    switch ( data_to_log->open_mode ) {
                                        case OPEN_MODE_APPEND:
                                            std_err_fptr = fopen(data_to_log->std_err, "a");
                                            break;
                                        case OPEN_MODE_TRUNCATE:
                                            std_err_fptr = fopen(data_to_log->std_err, "w");
                                            break;
                                        default: {
                                            slurm_conf_t *conf = slurm_conf_lock();
                                            if (conf->job_file_append)
                                                std_err_fptr = fopen(data_to_log->std_err, "a");
                                            else
                                                std_err_fptr = fopen(data_to_log->std_err, "w");
                                            slurm_conf_unlock();
                                            break;
                                        }
                                    }
                                    if ( std_err_fptr ) {
                                        fprintf(std_err_fptr, "\n** Slurm Prolog Error: %s\n\n", json_object_get_string(err_msg_obj));
                                        fclose(std_err_fptr);
                                    } else {
                                        ERROR("failed to open %s (errno = %d)", data_to_log->std_err, errno);
                                    }
                                }
#   endif
#endif
                                was_logged = true;
                            } else if ( json_object_object_get_ex(parsing_context.parsed_object, "activity_id", &err_msg_obj) && err_msg_obj ) {
#ifndef LOGGING_THREAD_TESTING
                                /*
                                 * New object returned by the REST API -- success!
                                 */
                                if ( __logging_thread_prep_callbacks[logging_thread_prep_callback_type_prolog] ) {
                                    __logging_thread_prep_callbacks[logging_thread_prep_callback_type_prolog](SLURM_SUCCESS, data_to_log->job_id);
                                }
#endif
                                was_logged = true;
                            } else {
                                ERROR("THREAD %p:  Failed to pre-debit job %u: invalid JSON returned by REST API", thread->the_thread, data_to_log->job_id);
                            }
                        } else {
                            ERROR("THREAD %p:  Failed to pre-debit job %u: malformed JSON returned by REST API", thread->the_thread, data_to_log->job_id);
                        }
                        while ( json_object_put(parsing_context.parsed_object) != 1 );
                    } else {
                        ERROR("THREAD %p:  Failed to pre-debit job %u: no data returned by REST API", thread->the_thread, data_to_log->job_id);
                    }
                }     else {
                    ERROR("THREAD %p:  Failed to pre-debit job %u: CURL returned error %d", thread->the_thread, data_to_log->job_id, http_err);
                }        
                curl_slist_free_all(http_headers);
                free((void*)json_body);
            }
            // Drop the JSON parser:
            json_tokener_free(parsing_context.parser);
            
            // Reset the CURL handle:
            curl_easy_reset(thread->curl_handle);
        } else {
            ERROR("THREAD %p: unable to allocate JSON parsing context", thread->the_thread);
        }
        free((void*)munge_header_value);
    }
early_exit:

    return was_logged;
}


/*!
 * @function __send_completion_data
 *
 * Handle the HTTP REST API transaction for a job completion record.
 *
 */
bool
__send_completion_data(
    logging_thread_t    *thread,
    job_data_t          *data_to_log
)
{
    bool                was_logged = false;
    char                *munge_header_value = NULL;
    size_t              munge_header_len;
    munge_err_t         munge_err;
    
    //
    // Setup the REST API URL for the job to which we're responding:
    //
    LOGGING_THREAD_CONTROL_DECL_COMPLETION_URL_VAR(rest_url, thread->parent, data_to_log->job_id);

    // Update the lifespan fields for this thread:
    thread->last_request_time = time(NULL);
    thread->n_requests++;
    
    // Munge-encode the REST API URL:
    if ( (munge_err = munge_encode(&munge_header_value, thread->munge_context, rest_url, strlen(rest_url))) != EMUNGE_SUCCESS ) {
        ERROR("THREAD %p: unable to MUNGE-encode REST API URL (err = %d)", thread->the_thread, munge_err);
        goto early_exit;
    }
    if ( munge_header_value ) {
        size_t                                  munge_header_len = logging_thread_rest_munge_header_name_len + 2 + strlen(munge_header_value) + 1;
        char                                    munge_header[munge_header_len], *http_response = NULL;
        logging_thread_http_response_context    parsing_context;
    
        snprintf(munge_header, munge_header_len, "%s: %s", logging_thread_rest_munge_header_name, munge_header_value);
        
        DEBUG("THREAD %p: job_data@%p { .job_id = %u, .account = \"%s\", .owner_uid = %d, .billed_su = %llu } url = %s",
                thread->the_thread, data_to_log,
                data_to_log->job_id, data_to_log->account, data_to_log->owner_uid, data_to_log->billed_su,
                rest_url
            );
            
        // Setup the JSON parsing context:
        parsing_context.parser = json_tokener_new();
        if ( parsing_context.parser ) {
            size_t                              json_body_len = 0;
            char                                *json_body = __logging_thread_http_body_from_job_data(data_to_log, &json_body_len);
            
            if ( json_body ) {
                struct curl_slist               *http_headers = NULL;
                char                            *http_body = json_body;
                CURLcode                        http_err;
                
                parsing_context.parsed_object = NULL;
            
                // Setup the CURL handle for the request:
                curl_easy_setopt(thread->curl_handle, CURLOPT_URL, rest_url);
                curl_easy_setopt(thread->curl_handle, CURLOPT_UPLOAD, 1L);
                curl_easy_setopt(thread->curl_handle, CURLOPT_READFUNCTION, __logging_thread_http_body_callback);
                curl_easy_setopt(thread->curl_handle, CURLOPT_READDATA, (void*)&http_body);
                curl_easy_setopt(thread->curl_handle, CURLOPT_INFILESIZE_LARGE, (curl_off_t)json_body_len);
                curl_easy_setopt(thread->curl_handle, CURLOPT_NOSIGNAL, 1L);
                if ( thread->parent->request_timeout > 0 ) curl_easy_setopt(thread->curl_handle, CURLOPT_TIMEOUT, thread->parent->request_timeout);
                if ( thread->parent->connection_timeout > 0 ) curl_easy_setopt(thread->curl_handle, CURLOPT_CONNECTTIMEOUT, thread->parent->connection_timeout);
                http_headers = curl_slist_append(http_headers, "Content-Type: application/json");
                http_headers = curl_slist_append(http_headers, munge_header);
                curl_easy_setopt(thread->curl_handle, CURLOPT_HTTPHEADER, http_headers);
                curl_easy_setopt(thread->curl_handle, CURLOPT_WRITEFUNCTION, __logging_thread_http_response_callback);
                curl_easy_setopt(thread->curl_handle, CURLOPT_WRITEDATA, (void*)&parsing_context);
                
                // Okay, preform the HTTP request:
                DEBUG("THREAD %p: curl_easy_perform(\"%s\") ...", thread->the_thread, rest_url);
                http_err = curl_easy_perform(thread->curl_handle);
                DEBUG("THREAD %p: ... curl_easy_perform(\"%s\") = %d", thread->the_thread, rest_url, http_err);
                
                if ( http_err == CURLE_OK ) {
                    // Is the response JSON object present?
                    if ( parsing_context.parsed_object ) {
                        if ( json_object_get_type(parsing_context.parsed_object) == json_type_object ) {
                            // Does the 'err_msg' key exist?
                            struct json_object  *err_msg_obj = NULL;
                        
                            if ( json_object_object_get_ex(parsing_context.parsed_object, "err_msg", &err_msg_obj) && err_msg_obj ) {
                                struct json_object  *err_code_obj = NULL;
                            
                                json_object_object_get_ex(parsing_context.parsed_object, "err_code", &err_code_obj);
                                if ( err_code_obj ) {
                                    if ( json_object_get_int(err_code_obj) == rest_api_err_code_unknown_job_id ) {
#ifndef LOGGING_THREAD_TESTING
                                        if ( __logging_thread_prep_callbacks[logging_thread_prep_callback_type_epilog] ) {
                                            __logging_thread_prep_callbacks[logging_thread_prep_callback_type_epilog](ESLURM_INVALID_JOB_ID, data_to_log->job_id);
                                        }
#endif
                                        was_logged = true;
                                    }
                                    ERROR("THREAD %p:  Failed to complete job %u:  %s (err_code = %d)",
                                            thread->the_thread, data_to_log->job_id, json_object_get_string(err_msg_obj), json_object_get_int(err_code_obj)
                                        );
                                } else {
                                    ERROR("THREAD %p:  Failed to complete job %u:  %s",
                                            thread->the_thread, data_to_log->job_id, json_object_get_string(err_msg_obj)
                                        );
                                }
                            } else {
#ifndef LOGGING_THREAD_TESTING
                                /*
                                 * Any error from the REST API should be interpreted as implying the job
                                 * cannot run; return ESLURM_ACCOUNTING_POLICY to PREP framework.
                                 */
                                if ( __logging_thread_prep_callbacks[logging_thread_prep_callback_type_epilog] ) {
                                    __logging_thread_prep_callbacks[logging_thread_prep_callback_type_epilog](SLURM_SUCCESS, data_to_log->job_id);
                                }
#endif
                                was_logged = true;
                            }
                        } else {
                            ERROR("THREAD %p:  Failed to complete job %u: invalid JSON returned by REST API", thread->the_thread, data_to_log->job_id);
                        }
                        while ( json_object_put(parsing_context.parsed_object) != 1 );
                    } else {
                        ERROR("THREAD %p:  Failed to complete job %u: no data returned by REST API", thread->the_thread, data_to_log->job_id);
                    }
                }     else {
                    ERROR("THREAD %p:  Failed to complete job %u: CURL returned error %d", thread->the_thread, data_to_log->job_id, http_err);
                }        
                curl_slist_free_all(http_headers);
                free((void*)json_body);
            }
            // Drop the JSON parser:
            json_tokener_free(parsing_context.parser);
            
            // Reset the CURL handle:
            curl_easy_reset(thread->curl_handle);
        } else {
            ERROR("THREAD %p: unable to allocate JSON parsing context", thread->the_thread);
        }
        free((void*)munge_header_value);
    }
early_exit:

    return was_logged;
}


/*!
 * @function __logging_thread_entrypoint
 *
 * Execution entry point for a thread that will process job data records from a
 * thread_controller_t queue.
 *
 */
void*
__logging_thread_entrypoint(
    void                *context
)
{
    logging_thread_t    *THREAD = (logging_thread_t*)context;
    int                 rc;
    bool                is_running = true;
    
    DEBUG("ENTERED THREAD %p", THREAD->the_thread);
            
    //  Allocate a CURL easy-access handle:
    THREAD->curl_handle = curl_easy_init();
    if ( ! THREAD->curl_handle ) {
        ERROR("THREAD %p: Unable to create CURL easy-access handle, must exit", THREAD->the_thread);
        goto early_exit;
    }
    
    // Create a munge context for encoding the URL for the MungeUrlHash header:
    THREAD->munge_context = munge_ctx_create();
    if ( ! THREAD->munge_context ) {
        ERROR("THREAD %p: Unable to create MUNGE encoding context, must exit", THREAD->the_thread);
        goto early_exit;
    }
    
    while ( is_running ) {
        // Time for this thread to terminate?  Wrap in a mutex lock since
        // the controller thread could be trying to update the should_exit
        // flag at any point in time:
        DEBUG("THREAD %p:  WAITING ON THREAD LOCK...", THREAD->the_thread);
        rc = pthread_mutex_lock(&THREAD->thread_mutex);
        DEBUG("THREAD %p:  MUTEX_LOCK(THREAD) = %d", THREAD->the_thread, rc);
        is_running = ! THREAD->should_exit;
        // Drop the lock on the thread state:
        DEBUG("THREAD %p:  DROP THREAD LOCK...", THREAD->the_thread);
        rc = pthread_mutex_unlock(&THREAD->thread_mutex);
        
        if ( is_running ) {
            job_data_t   *data_to_log;
            
            // Lock the parent controller queue mutex:
            DEBUG("THREAD %p:  WAITING ON PARENT QUEUE LOCK...", THREAD->the_thread);
            rc = pthread_mutex_lock(&THREAD->parent->queue_mutex);
            DEBUG("THREAD %p:  MUTEX_LOCK(QUEUE) = %d", THREAD->the_thread, rc);
            
            // Check if the parent controller has a record for us to process:
            data_to_log = __logging_thread_control_pop_job_data(THREAD->parent, THREAD);
            DEBUG("THREAD %p:  DATA TO LOG = %p", THREAD->the_thread, data_to_log);
            if ( ! data_to_log ) {
                // No record available right now, so wait on the queue condition variable
                // and unlock the queue mutex:
                DEBUG("THREAD %p:  WAITING ON PARENT QUEUE COND...", THREAD->the_thread);
                rc = pthread_cond_wait(&THREAD->parent->queue_cond, &THREAD->parent->queue_mutex);
                DEBUG("THREAD %p:  COND_WAIT(QUEUE) = %d", THREAD->the_thread, rc);
                
                // On exit from pthread_cond_wait(), the queue mutex is again
                // locked by this thread.
                
                // Lock the thread state again and update our copy of the
                // thread's running status:
                DEBUG("THREAD %p:  WAITING ON THREAD LOCK...", THREAD->the_thread);
                rc = pthread_mutex_lock(&THREAD->thread_mutex);
                DEBUG("THREAD %p:  MUTEX_LOCK(THREAD) = %d", THREAD->the_thread, rc);
                is_running = ! THREAD->should_exit;
                // Drop the lock on the thread state:
                DEBUG("THREAD %p:  DROP THREAD LOCK...", THREAD->the_thread);
                rc = pthread_mutex_unlock(&THREAD->thread_mutex);
                if ( is_running ) {
                    // Try to grab the data that's ready to be logged:
                    data_to_log = __logging_thread_control_pop_job_data(THREAD->parent, THREAD);
                    DEBUG("THREAD %p:  SECOND TRY, DATA TO LOG = %p", THREAD->the_thread, data_to_log);
                }
            }
            
            // Drop the queue mutex so that other threads can grab records
            // to log:
            rc = pthread_mutex_unlock(&THREAD->parent->queue_mutex);
            DEBUG("THREAD %p:  MUTEX_UNLOCK(QUEUE) = %d", THREAD->the_thread, rc);
            
            if ( data_to_log ) {
                bool            was_logged = true;
                
                switch ( data_to_log->job_data_variant ) {
                
                    case job_data_variant_predebit:
                        was_logged = __send_predebit_data(THREAD, data_to_log);
                        break;
                    
                    case job_data_variant_completion:
                        was_logged = __send_completion_data(THREAD, data_to_log);
                        break;
                        
                }
                
                // Reacquire the queue mutex to finish-up this record:
                DEBUG("THREAD %p:  WAITING ON PARENT QUEUE LOCK...", THREAD->the_thread);
                rc = pthread_mutex_lock(&THREAD->parent->queue_mutex);
                DEBUG("THREAD %p:  MUTEX_LOCK(QUEUE) = %d", THREAD->the_thread, rc);
                
                if ( was_logged ) {
                    // Finalize the record:
                    DEBUG("THREAD %p:  REMOVING RECORD", THREAD->the_thread);
                    __logging_thread_control_fini_job_data(THREAD->parent, THREAD, data_to_log);
                } else {
                    // Push back to the queue:
                    DEBUG("THREAD %p:  PUSHING RECORD BACK IN QUEUE", THREAD->the_thread);
                    __logging_thread_control_pushback_job_data(THREAD->parent, THREAD, data_to_log);
                }
                
                // Drop the lock on the queue_mutex:
                rc = pthread_mutex_unlock(&THREAD->parent->queue_mutex);
                DEBUG("THREAD %p:  MUTEX_UNLOCK(QUEUE) = %d", THREAD->the_thread, rc);
            }
        }
    }

early_exit:

    // Dispose of the CURL handle:
    if ( THREAD->curl_handle ) {
        curl_easy_cleanup(THREAD->curl_handle);
        THREAD->curl_handle = NULL;
    }
    
    // Dispose of the MUNGE context:
    if ( THREAD->munge_context ) {
        munge_ctx_destroy(THREAD->munge_context);
        THREAD->munge_context = NULL;
    }
    
    DEBUG("EXITING THREAD %p", THREAD->the_thread);
    return NULL;
}

/*!
 * @function __logging_thread_create
 *
 * Allocate and initialize a new logging_thread_t instance.  The pthread
 * is not created and started by this function.
 *
 * Returns NULL if the instance could not be allocated or initialized.
 */ 
logging_thread_t*
__logging_thread_create(void)
{
    logging_thread_t        *new_thread = calloc(1, sizeof(logging_thread_t));
    
    if ( new_thread ) {
        int                 rc;
        
        new_thread->start_time = time(NULL);
        rc = pthread_mutex_init(&new_thread->thread_mutex, NULL);
        if ( rc != 0 ) {
            free((void*)new_thread);
            return NULL;
        }
    }
    return new_thread;
}

/*!
 * @function __logging_thread_fini
 *
 * Deallocate a logging_thread_t instance.
 */
void
__logging_thread_fini(
    logging_thread_t        *the_thread
)
{
    int                     rc;
    
    rc = pthread_mutex_destroy(&the_thread->thread_mutex);
    rc = pthread_detach(the_thread->the_thread);
    free((void*)the_thread);
}

/*!
 * @var __logging_thread_has_called_curl_global_init
 *
 * Global that indicates whether or not this code has called the CURL global
 * initialization.
 */
static bool __logging_thread_has_called_curl_global_init = false;

/*
 * Prototype declaration of the maintenance thread's entry point function:
 */
void* __logging_maintenance_thread_entrypoint(void *context);

/*!
 * @enum Logging thread extrenally-configurable options
 *
 * Enumeration of the text-based options that can be configured when a
 * logging_thread_controller_t is allocated.
 *
 * @const logging_thread_option_connection_timeout
 *      Timeout (in seconds) for initial CURL TCP connect for a API transaction;
 *      default LOGGING_THREAD_CONNECT_TIMEOUT
 * @const logging_thread_option_request_timeout
 *      Timeout (in seconds) for the completion of a CURL API transaction (once
 *      connected); default LOGGING_THREAD_REQUEST_TIMEOUT
 * @const logging_thread_option_min_threads
 *      The minimum number of logging threads that should be present to process
 *      the job data queue; default LOGGING_THREAD_MIN_THREADS
 * @const logging_thread_option_max_threads
 *      The maximum number of logging threads that may be used to process
 *      the job data queue; default LOGGING_THREAD_MAX_THREADS
 * @const logging_thread_option_thread_lifespan_max_requests
 *      A logging thread should be terminated once it has processed this many
 *      job data records; default LOGGING_THREAD_MAX_REQUESTS_PER_THREAD
 * @const logging_thread_option_thread_lifespan_max_time
 *      A logging thread should be terminated after it has been online for
 *      this amount of time (in seconds); default
 *      LOGGING_THREAD_MAX_TIME_PER_THREAD
 * @const logging_thread_option_queue_wait_threshold
 *      Once the statistical average wait time in the queue exceeds this value
 *      (in seconds) additional threads will be spawned (if possible); default
 *      LOGGING_THREAD_QUEUE_WAIT_THRESHOLD
 * @const logging_thread_option_maintenance_interval
 *      The scheduled interval (in seconds) that the maintenance thread will wake
 *      and cleanup threads that have reached their lifespan and spawn new threads
 *      if necessary; default LOGGING_THREAD_MAINTENANCE_INTERVAL
 * @const logging_thread_option_timedjoin_timeout
 *      The number of seconds pthread_timedjoin_np() is allowed to block; default
 *      LOGGING_THREAD_TIMEDJOIN_TIMEOUT
 * @const logging_thread_option_timedjoin_retry
 *      The number of times a pthread_timedjoin_np() that times out can be retried
 *      on subsequent maintenance firings; default LOGGING_THREAD_TIMEDJOIN_RETRY
 * @const logging_thread_option_rest_api_base_url
 *      The base URL for HADM REST API requests; default
 *      LOGGING_THREAD_REST_API_BASE_URL
 */
enum {
    logging_thread_option_connection_timeout = 0,
    logging_thread_option_request_timeout,
    logging_thread_option_min_threads,
    logging_thread_option_max_threads,
    logging_thread_option_thread_lifespan_max_requests,
    logging_thread_option_thread_lifespan_max_time,
    logging_thread_option_queue_wait_threshold,
    logging_thread_option_maintenance_interval,
    logging_thread_option_timedjoin_timeout,
    logging_thread_option_timedjoin_retry,
    logging_thread_option_rest_api_base_url,
    
    logging_thread_option_max
};

/*!
 * @typedef logging_thread_option_t
 *
 * C string + string length bundled together; for the logging_thread option
 * strings.
 */
typedef struct {
    const char  *option_str;
    size_t      option_str_len;
} logging_thread_option_t;

/*!
 * @defined LOGGING_THREAD_OPTION_WRAP
 *
 * Generate a logging_thread_option_t struct to hold a given string constant, S.
 */
#define LOGGING_THREAD_OPTION_WRAP(S)   { .option_str = S, .option_str_len = (sizeof(S) - 1) }

/*!
 * @var __logging_thread_options
 *
 * The table of textual logging thread controller options recognized by this
 * API.
 */
logging_thread_option_t __logging_thread_options[logging_thread_option_max] = {
                LOGGING_THREAD_OPTION_WRAP("timeout.connect"),
                LOGGING_THREAD_OPTION_WRAP("timeout.request"),
                LOGGING_THREAD_OPTION_WRAP("threads.min"),
                LOGGING_THREAD_OPTION_WRAP("threads.max"),
                LOGGING_THREAD_OPTION_WRAP("lifespan.max_requests"),
                LOGGING_THREAD_OPTION_WRAP("lifespan.max_time"),
                LOGGING_THREAD_OPTION_WRAP("interval.wait_threshold"),
                LOGGING_THREAD_OPTION_WRAP("interval.maintenance"),
                LOGGING_THREAD_OPTION_WRAP("timedjoin.timeout"),
                LOGGING_THREAD_OPTION_WRAP("timedjoin.retry"),
                LOGGING_THREAD_OPTION_WRAP("rest.base_url")
            };

/*!
 * @function __logging_thread_option_parse_long_int
 *
 * Attempt to locate the given logging thread option (option_id) in
 * the options string.  If present, parse and return the integer value
 * indicated.
 *
 * If options is empty/NULL, the requested option is not present, or
 * the value cannot be parsed as an integer, the default_value is
 * returned.
 */
long
__logging_thread_option_parse_long_int(
    const char      *options,
    int             option_id,
    long            default_value
)
{
    long            result = default_value;
    
    if ( options ) {
        char        *base_ptr = strstr(options, __logging_thread_options[option_id].option_str);
        
        if ( base_ptr && (strlen(base_ptr) > __logging_thread_options[option_id].option_str_len) ) {
            base_ptr += __logging_thread_options[option_id].option_str_len;
            if ( *base_ptr == '=' ) {
                char    *end_ptr = NULL;
                long    parsed_value = strtol(++base_ptr, &end_ptr, 0);
            
                DEBUG("THREAD CONTROLLER OPTION %s = %ld", __logging_thread_options[option_id].option_str, parsed_value);
                if ( (end_ptr > base_ptr) && (parsed_value >= 0) ) result = parsed_value;
            }
        }
    }
    return result;
}

/*!
 * @function __logging_thread_option_parse_cstring
 *
 * Attempt to locate the given logging thread option (option_id) in
 * the options string.  If present, isolate a C string and return
 * it -- the caller is responsible for free'ing it.
 *
 * If options is empty/NULL or the requested option is not present the
 * default_value is returned.
 */
const char*
__logging_thread_option_parse_cstring(
    const char      *options,
    int             option_id,
    const char      *default_value
)
{
    const char      *result = NULL;
    
    if ( options ) {
        char        *base_ptr = strstr(options, __logging_thread_options[option_id].option_str);
        
        if ( base_ptr && (strlen(base_ptr) > __logging_thread_options[option_id].option_str_len) ) {
            base_ptr += __logging_thread_options[option_id].option_str_len;
            if ( *base_ptr == '=' ) {
                char    *start;
                
                // Quote-delimited?
                base_ptr++;
                if ( (*base_ptr == '"') || (*base_ptr == '\'') ) {
                    char    delim = *base_ptr++;
                    
                    start = base_ptr;
                    while ( *base_ptr && (*base_ptr != delim) ) base_ptr++;
                } else {
                    start = base_ptr;
                    while ( base_ptr && ! isspace(*base_ptr) ) base_ptr++;
                }
                
                if ( base_ptr > start ) {
                    char    *parsed_value = malloc(base_ptr - start + 1);
                    if ( parsed_value ) {
                        memcpy(parsed_value, start, base_ptr - start);
                        parsed_value[base_ptr - start] = '\0';
                        DEBUG("THREAD CONTROLLER OPTION %s = %s", __logging_thread_options[option_id].option_str, parsed_value);
                        result = parsed_value;
                    }
                }
            }
        }
    }
    if ( result == NULL && default_value ) result = strdup(default_value);
    return result;
}

/*!
 * @function __logging_thread_control_alloc_job_data_record
 *
 * Allocate a job data queue record -- either from the unused,
 * already-allocated list attached to the thread_controller OR
 * by means of calloc().
 *
 * Returns NULL if a record cannot be allocated.  The record
 * returned is initialzied to all zero bytes.
 */
job_data_record_t*
__logging_thread_control_alloc_job_data_record(
    logging_thread_control_t    *thread_controller
)
{
    job_data_record_t     *new_record = NULL;
    
    // Allocate a record:
    if ( thread_controller->unused_job_data_records ) {
        new_record = thread_controller->unused_job_data_records;
        thread_controller->unused_job_data_records = new_record->link;
        memset(new_record, 0, sizeof(*new_record));
    } else {
        new_record = (job_data_record_t*)calloc(1, sizeof(job_data_record_t));
    }
    return new_record;
}

/*!
 * @function __logging_thread_control_copy_job_data_record
 *
 * Allocate a job data queue record -- either from the unused,
 * already-allocated list attached to the thread_controller OR
 * by means of calloc() and initialize with the contents of
 * the other_job_data record.
 */
job_data_record_t*
__logging_thread_control_copy_job_data_record(
    logging_thread_control_t    *thread_controller,
    job_data_t                  *other_job_data
)
{
    job_data_record_t     *new_record = __logging_thread_control_alloc_job_data_record(thread_controller);
    
    // Initialize:
    if ( new_record ) {
        new_record->job_data = *other_job_data;
        if ( other_job_data->std_err ) {
            new_record->job_data.std_err = strdup(other_job_data->std_err);
        }
    }
    return new_record;
}

void
__logging_thread_control_reset_job_data_record(
    logging_thread_control_t    *thread_controller,
    job_data_record_t           *record_to_zero,
    bool                        should_return_to_unused
)
{
    // Drop the std_err string:
    if ( record_to_zero->job_data.std_err ) free((void*)record_to_zero->job_data.std_err);
    
    if ( should_return_to_unused ) {
        // Zero-out the entire data structure:
        memset(record_to_zero, 0, sizeof(job_data_record_t));
        record_to_zero->link = thread_controller->unused_job_data_records;
        thread_controller->unused_job_data_records = record_to_zero;
    } else {
        free((void*)record_to_zero);
    }
}

//

logging_thread_control_t*
logging_thread_control_init(
    const char      *options
)
{
    logging_thread_control_t    *new_controller;
        
    if ( ! __logging_thread_has_called_curl_global_init ) {
        if ( curl_global_init(CURL_GLOBAL_ALL) != 0 ) return NULL;
        __logging_thread_has_called_curl_global_init = true;
    }
    
    new_controller = calloc(1, sizeof(logging_thread_control_t));
    if ( new_controller ) {
        int                     rc;
        
        new_controller->start_time              = time(NULL);
        
        // Initialize default parameters:
        new_controller->connection_timeout      = __logging_thread_option_parse_long_int(options, logging_thread_option_connection_timeout, LOGGING_THREAD_CONNECT_TIMEOUT);
        new_controller->request_timeout         = __logging_thread_option_parse_long_int(options, logging_thread_option_request_timeout, LOGGING_THREAD_REQUEST_TIMEOUT);
        new_controller->queue_wait_threshold    = __logging_thread_option_parse_long_int(options, logging_thread_option_queue_wait_threshold, LOGGING_THREAD_QUEUE_WAIT_THRESHOLD);
        new_controller->min_threads             = __logging_thread_option_parse_long_int(options, logging_thread_option_min_threads, LOGGING_THREAD_MIN_THREADS);
        new_controller->max_threads             = __logging_thread_option_parse_long_int(options, logging_thread_option_max_threads, LOGGING_THREAD_MAX_THREADS);
        new_controller->thread_lifespan
                            .max_requests       = __logging_thread_option_parse_long_int(options, logging_thread_option_thread_lifespan_max_requests, LOGGING_THREAD_MAX_REQUESTS_PER_THREAD);
        new_controller->thread_lifespan
                            .max_time           = __logging_thread_option_parse_long_int(options, logging_thread_option_thread_lifespan_max_time, LOGGING_THREAD_MAX_TIME_PER_THREAD);
        new_controller->timedjoin_timeout       = __logging_thread_option_parse_long_int(options, logging_thread_option_timedjoin_timeout, LOGGING_THREAD_TIMEDJOIN_TIMEOUT);
        new_controller->timedjoin_retry         = __logging_thread_option_parse_long_int(options, logging_thread_option_timedjoin_retry, LOGGING_THREAD_TIMEDJOIN_RETRY);
        
        // Initialize mutex/conditions:
        rc = pthread_mutex_init(&new_controller->threads_mutex, NULL);
        if ( rc != 0 ) {
            free((void*)new_controller);
            return NULL;
        }
        rc = pthread_mutex_init(&new_controller->queue_mutex, NULL);
        if ( rc != 0 ) {
            free((void*)new_controller);
            return NULL;
        }
        rc = pthread_cond_init(&new_controller->queue_cond, NULL);
        if ( rc != 0 ) {
            free((void*)new_controller);
            return NULL;
        }
        
        // Initialize maintenace thread:
        new_controller->maintenance_interval = __logging_thread_option_parse_long_int(options, logging_thread_option_maintenance_interval, LOGGING_THREAD_MAINTENANCE_INTERVAL);
        rc = pthread_cond_init(&new_controller->maintenance_wakeup, NULL);
        if ( rc != 0 ) {
            free((void*)new_controller);
            return NULL;
        }
        rc = pthread_create(&new_controller->maintenance_thread, NULL, __logging_maintenance_thread_entrypoint, (void*)new_controller);
        
        // Initialize statistics:
        incremental_stat_init(new_controller->queue_timing_stats);
        incremental_stat_init(new_controller->request_timing_stats);
        
        // Get the REST API base URL:
        new_controller->rest_api_base_url = __logging_thread_option_parse_cstring(options, logging_thread_option_rest_api_base_url, LOGGING_THREAD_REST_API_BASE_URL);
        new_controller->rest_api_base_url_len = strlen(new_controller->rest_api_base_url);
        
        // Ensure there's no '/' characters at the end of the URL, we'll add that
        // ourselves:
        while ( new_controller->rest_api_base_url_len && (new_controller->rest_api_base_url[new_controller->rest_api_base_url_len - 1] == '/') ) {
            *((char*)new_controller->rest_api_base_url + --new_controller->rest_api_base_url_len) = '\0';
        }
        
        // Dump some debug info:
        INFO("THREAD_CONTROLLER@%p:  connection_timeout = %ld", new_controller, (long)new_controller->connection_timeout);
        INFO("THREAD_CONTROLLER@%p:  request_timeout = %ld", new_controller, (long)new_controller->request_timeout);
        INFO("THREAD_CONTROLLER@%p:  queue_wait_threshold = %ld", new_controller, (long)new_controller->queue_wait_threshold);
        INFO("THREAD_CONTROLLER@%p:  min_threads = %ld", new_controller, (long)new_controller->min_threads);
        INFO("THREAD_CONTROLLER@%p:  max_threads = %ld", new_controller, (long)new_controller->max_threads);
        INFO("THREAD_CONTROLLER@%p:  thread_lifespan.max_requests = %ld", new_controller, (long)new_controller->thread_lifespan.max_requests);
        INFO("THREAD_CONTROLLER@%p:  thread_lifespan.max_time = %ld", new_controller, (long)new_controller->thread_lifespan.max_time);
        INFO("THREAD_CONTROLLER@%p:  maintenance_interval = %ld", new_controller, (long)new_controller->maintenance_interval);
        INFO("THREAD_CONTROLLER@%p:  timedjoin_timeout = %ld", new_controller, (long)new_controller->timedjoin_timeout);
        INFO("THREAD_CONTROLLER@%p:  timedjoin_retry = %ld", new_controller, (long)new_controller->timedjoin_retry);
        INFO("THREAD_CONTROLLER@%p:  rest_api_base_url = %s", new_controller, new_controller->rest_api_base_url);
    }
    return new_controller;
}

/*!
 * @function __logging_thread_control_thread_join
 *
 * Helper function that attempts to wakeup a logging thread that has
 * been set to exit and complete its termination.  Returns:
 *
 *     logging_thread_join_success    thread was joined successfully
 *     logging_thread_join_defer      join(s) timed out; try again later
 *     logging_thread_join_failure    join failed
 */
int
__logging_thread_control_thread_join(
    logging_thread_control_t    *thread_controller,
    logging_thread_t            *the_thread,
    unsigned                    retry_count
)
{
    bool                        should_retry = (retry_count > 0);
    int                         rc, out_rc = logging_thread_join_failure, try = 1;

    // Don't include the first pass in the retry count:
    retry_count++;
    do {
#ifdef HAVE_PTHREAD_TIMEDJOIN_NP
        struct timespec         join_timeout = { .tv_sec = time(NULL) + thread_controller->timedjoin_timeout, .tv_nsec = 0 };
#endif
        // Wake sleeping threads so they'll notice they're supposed
        // to exit:
        rc = pthread_mutex_lock(&thread_controller->queue_mutex);
        DEBUG("** QUEUE MUTEX LOCK = %d **", rc);
        rc = pthread_cond_broadcast(&thread_controller->queue_cond);
        DEBUG("** QUEUE COND WAKEUP = %d **", rc);
        rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
        DEBUG("** QUEUE MUTEX UNLOCK = %d **", rc);

#ifdef HAVE_PTHREAD_TIMEDJOIN_NP
        DEBUG("** JOINING THREAD %p TRY %d TIMEOUT %ld **", the_thread->the_thread, try++, (long)join_timeout.tv_sec);
        rc = pthread_timedjoin_np(the_thread->the_thread, NULL, &join_timeout);
#else
        DEBUG("** JOINING THREAD %p TRY %d **", the_thread->the_thread, try++);
        rc = pthread_join(the_thread->the_thread, NULL);
#endif
        DEBUG("** THREAD JOIN RC = %d **", rc);
        switch ( rc ) {
            case ETIMEDOUT:
                out_rc = logging_thread_join_defer;
                break;
            case 0:
                out_rc = logging_thread_join_success;
                break;
            default:
                out_rc = logging_thread_join_failure;
                break;
        }
        retry_count--;
    } while ( (out_rc != logging_thread_join_success) && (retry_count > 0) );

    // Indicate another deferral's having happened:
    if ( out_rc == logging_thread_join_defer ) the_thread->join_defer_count++;

    return out_rc;
}

//

bool
logging_thread_control_fini(
    logging_thread_control_t    *thread_controller
)
{
    bool                        all_okay = true;
    int                         rc;
    logging_thread_t            *threads, *last_thread;
    job_data_record_t           *job_data_records;
    
    // Start shutting-down the threads:
    rc = pthread_mutex_lock(&thread_controller->threads_mutex);
    
    // Mark the maintenance thread for exit and join it:
    thread_controller->should_maintenance_exit = true;
    DEBUG("** SIGNALING MAINTENANCE THREAD **");
    rc = pthread_mutex_unlock(&thread_controller->threads_mutex);
    rc = pthread_cond_signal(&thread_controller->maintenance_wakeup);
    DEBUG("** JOIN MAINTENANCE THREAD **");
    rc = pthread_join(thread_controller->maintenance_thread, NULL);
    if ( rc != 0 ) {
        DEBUG("** CANCEL MAINTENANCE THREAD **");
        pthread_cancel(thread_controller->maintenance_thread);
    }
    rc = pthread_mutex_lock(&thread_controller->threads_mutex);
    
    // Walk the threads and mark them for exit:
    threads = thread_controller->threads;
    while ( threads ) {
        // Lock the thread status mutex:
        rc = pthread_mutex_lock(&threads->thread_mutex);
    
        // Check if lifespan has been exceeded:
        threads->should_exit = true;
        
        // Unlock the thread status mutex:
        rc = pthread_mutex_unlock(&threads->thread_mutex);
        
        threads = threads->link;
    }

retry:
    
    // Walk through and try to join all threads:
    threads = thread_controller->threads;
    last_thread = NULL;
    while ( threads ) {
#ifdef HAVE_PTHREAD_TIMEDJOIN_NP
        struct timespec         join_timeout = { .tv_sec = time(NULL) + (135 * 5), .tv_nsec = 0 };
#endif
        // Wake all that are sleeping:
        rc = pthread_mutex_lock(&thread_controller->queue_mutex);
        DEBUG("** QUEUE MUTEX LOCK = %d **", rc);
        rc = pthread_cond_broadcast(&thread_controller->queue_cond);
        DEBUG("** QUEUE COND WAKEUP = %d **", rc);
        rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
        DEBUG("** QUEUE MUTEX UNLOCK = %d **", rc);
        
#ifdef HAVE_PTHREAD_TIMEDJOIN_NP    
        rc = pthread_timedjoin_np(threads->the_thread, NULL, &join_timeout);
#else
        rc = pthread_join(threads->the_thread, NULL);
#endif
        if ( rc == 0 ) {
            logging_thread_t    *next_thread = threads->link;
            
            // Thread is done, reap it:
            if ( last_thread ) {
                last_thread->link = threads->link;
            } else {
                thread_controller->threads = threads->link;
            }
            // Dispose of the thread:
            __logging_thread_fini(threads);
            
            // Decrement thread count:
            thread_controller->n_threads--;
            
            threads = next_thread;
        } else {
            last_thread = threads;
            threads = threads->link;
        }
    }
    if ( thread_controller->n_threads > 0 ) goto retry;
    
    // Dispose of all job data records we're holding:
    job_data_records = thread_controller->queued_records;
    while ( job_data_records ) {
        job_data_record_t     *next = job_data_records->link;
        
        __logging_thread_control_reset_job_data_record(thread_controller, job_data_records, false);
        job_data_records = next;
    }
    job_data_records = thread_controller->in_progress_records;
    while ( job_data_records ) {
        job_data_record_t     *next = job_data_records->link;
        
        __logging_thread_control_reset_job_data_record(thread_controller, job_data_records, false);
        job_data_records = next;
    }
    job_data_records = thread_controller->unused_job_data_records;
    while ( job_data_records ) {
        job_data_record_t     *next = job_data_records->link;
        
        __logging_thread_control_reset_job_data_record(thread_controller, job_data_records, false);
        job_data_records = next;
    }
    
    // Dispose of all pthread entities:
    rc = pthread_mutex_destroy(&thread_controller->threads_mutex);
    rc = pthread_mutex_destroy(&thread_controller->queue_mutex);
    rc = pthread_cond_destroy(&thread_controller->maintenance_wakeup);
    rc = pthread_cond_destroy(&thread_controller->queue_cond);
    
    // Drop that URL we allocated:
    free((void*)thread_controller->rest_api_base_url);
    
    // Alrighty, we've reclaimed everything -- drop the thread_controller:
    free((void*)thread_controller);
    
    return all_okay;
}

//

bool
logging_thread_control_push_job_data(
    logging_thread_control_t    *thread_controller,
    job_data_t           *job_data
)
{
    return logging_thread_control_push_job_data_bulk(thread_controller, job_data, true);
}

bool
logging_thread_control_push_job_data_bulk(
    logging_thread_control_t    *thread_controller,
    job_data_t                  *job_data,
    bool                        is_last_record
)
{
    logging_thread_t            *threads;
    bool                        all_okay = false;
    int                         rc;
    
    // Lock the queue mutex:
    rc = pthread_mutex_lock(&thread_controller->queue_mutex);
    DEBUG("MUTEX_LOCK(QUEUE) = %d", rc);
    
    if ( thread_controller->is_queue_disabled ) {
        // Unlock the queue mutex:
        rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
        DEBUG("MUTEX_UNLOCK(QUEUE) = %d", rc);
        return false;
    }
    
    if ( job_data ) {
        job_data_record_t     *new_record = __logging_thread_control_copy_job_data_record(thread_controller, job_data);
        
        if ( new_record ) {
            new_record->queued_time = time(NULL);
            if ( thread_controller->queued_tail ) {
                thread_controller->queued_tail->link = new_record;
                thread_controller->queued_tail = new_record;
            } else {
                thread_controller->queued_records = thread_controller->queued_tail = new_record;
            }
        
            // Update counts:
            thread_controller->n_queued++;
            thread_controller->n_bulk++;
            
#ifdef LOGGING_THREAD_DEBUG
            {
                int                 i = 0;
                job_data_record_t *qhead = thread_controller->queued_records;
                while ( qhead ) { i++; qhead = qhead->link; }
                DEBUG("QUEUE CONTROL:  n_queued = %u, n_in_progress = %u, n_bulk = %u | resolved = %d",
                    thread_controller->n_queued, thread_controller->n_in_progress, thread_controller->n_bulk, i
                );
            }
#endif
            // All good!
            all_okay = true;
        }
    } else {
        all_okay = true;
    }
    if ( is_last_record ) {
        DEBUG("QUEUE CONTROL:  signal x %d", thread_controller->n_bulk);
        while ( thread_controller->n_bulk > 0 ) {
            // Signal the condition to wake at least one thread:
            rc = pthread_cond_signal(&thread_controller->queue_cond);
            thread_controller->n_bulk--;
        }
    }
    
    // Unlock the queue mutex:
    rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
    DEBUG("MUTEX_UNLOCK(QUEUE) = %d", rc);
    
    return all_okay;
}

//

bool
logging_thread_control_check_threads(
    logging_thread_control_t    *thread_controller
)
{
    bool                        all_okay = true;
    int                         spawn_count, rc;
    
    DEBUG("|- TRYING TO GET LOCK ON THREADS LIST...");
    // Lock the thread list mutex so we can examine the threads:
    rc = pthread_mutex_lock(&thread_controller->threads_mutex);
    
    // We don't want to evict any threads if the average request time has
    // exceeded the timout limit — we're probably waiting on the database REST API
    // to come back online.  Otherwise, shutdown any threads that have outlived
    // their programmed lifespan.
    DEBUG("|- CURRENT AVG REQ TIME %lg <= REQ MAX TIMEOUT %ld", thread_controller->request_timing_stats.m_current, thread_controller->request_timeout);
    if ( (thread_controller->n_threads > 0) && (thread_controller->request_timing_stats.m_current <= thread_controller->request_timeout) ) {
        logging_thread_t    *threads, *last_thread;
    
        DEBUG("|- CHECKING FOR AGED-OUT THREADS IN LIST OF %d THREADS", thread_controller->n_threads);
        
        threads = thread_controller->threads;
        last_thread = NULL;
        while ( threads ) {
            logging_thread_t    *next_thread = threads->link;
            
            // Lock the thread status mutex:
            DEBUG("|- |- LOCKING THREAD %p", threads->the_thread);
            rc = pthread_mutex_lock(&threads->thread_mutex);
        
            // Check if lifespan has been exceeded:
            if ( logging_thread_is_lifespan_surpassed(threads, thread_controller->thread_lifespan.max_time, thread_controller->thread_lifespan.max_requests)) {
                int    join_status;

                DEBUG("|- |- |- SIGNALLING THREAD %p FOR EXIT...", threads->the_thread);
                threads->should_exit = true;
                
                // Drop the lock and wait for the thread to exit:
                DEBUG("|- |- |- UNLOCKING THREAD %p", threads->the_thread);
                rc = pthread_mutex_unlock(&threads->thread_mutex);
                
                join_status = __logging_thread_control_thread_join(thread_controller, threads, 0);
                DEBUG("|- |- |- JOINING THREAD %p => %d", threads->the_thread, join_status);
                switch ( join_status ) {
                    case logging_thread_join_failure:
                        ERROR("!! FAILED TO COMPLETE JOIN OF THREAD %p", threads->the_thread);
                        exit(1);
                    case logging_thread_join_success:
                        DEBUG("|- |- |- REMOVING THREAD %p", threads->the_thread);
                        // Remove the thread from the list:
                        if ( last_thread ) {
                            last_thread->link = threads->link;
                        } else {
                            thread_controller->threads = threads->link;
                        }
                        // Deallocate it:
                        __logging_thread_fini(threads);
                        threads = NULL;
                        thread_controller->n_threads--;
                        break;
                    case logging_thread_join_defer:
                        if ( threads->join_defer_count > thread_controller->timedjoin_retry ) {
                            ERROR("!! FAILED TO COMPLETE JOIN OF THREAD %p AFTER %d DEFERRALS", threads->the_thread, threads->join_defer_count);
                            exit(1);
                        }
                        break;
                }
            } else {
                // Unlock the thread status mutex:
                DEBUG("|- |- UNLOCKING THREAD %p", threads->the_thread);
                rc = pthread_mutex_unlock(&threads->thread_mutex);
            }
            if ( threads ) last_thread = threads;
            threads = next_thread;
        }
    }
    
    // Figure out how many threads to spawn:
    spawn_count = 0;
    
    // If we don't have enough threads, spawn new ones now:
    if ( thread_controller->n_threads < thread_controller->min_threads ) {
        spawn_count = thread_controller->min_threads - thread_controller->n_threads;
    }
    
    // Do we need some additional threads based on queue time?
    rc = pthread_mutex_lock(&thread_controller->queue_mutex);
    if ( (thread_controller->n_queued > 0) && (thread_controller->request_timing_stats.m_current <= thread_controller->request_timeout) ) {
        if ( thread_controller->queue_timing_stats.m_current > thread_controller->queue_wait_threshold ) {
            // Reset our stats, we don't want old state that triggered an increased thread
            // count to dominate the stats:
            incremental_stat_init(thread_controller->queue_timing_stats);
            spawn_count++;
        }
    }
    rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
    
    // As long as we're not going to spawn too many threads...
    if ( thread_controller->n_threads + spawn_count > thread_controller->max_threads ) spawn_count = thread_controller->max_threads - thread_controller->n_threads;
    
    if ( spawn_count > 0 ) {
        logging_thread_t*       new_thread;
        
        DEBUG("|- SPAWNING %d NEW THREADS...", spawn_count);
        while ( spawn_count-- ) {
            new_thread = __logging_thread_create();
            if ( ! new_thread ) break;
            new_thread->parent = thread_controller;
            new_thread->link = thread_controller->threads;
            thread_controller->threads = new_thread;
            thread_controller->n_threads++;
            
            // It's initialized, go ahead and spawn the actual thread:
            rc = pthread_create(&new_thread->the_thread, NULL, __logging_thread_entrypoint, (void*)new_thread);
        }
    }
    
    // Drop the lock on the threads list:
    DEBUG("|- DROPPING LOCK ON THREADS LIST...");
    rc = pthread_mutex_unlock(&thread_controller->threads_mutex);

    return all_okay;
}

//

size_t
__logging_thread_io_read(
    int                 fd,
    void                *buffer,
    size_t              buffer_len,
    bool                *is_eof
)
{
    while ( buffer_len > 0 ) {
        ssize_t         bytes_read = read(fd, buffer, buffer_len);
        
        if ( bytes_read == 0 ) {
            *is_eof = true;
            break;
        }
        if ( (bytes_read < 0) && (errno != EINTR) ) break;
        buffer_len -= bytes_read;
        buffer += bytes_read;
    }
    return buffer_len;
}

//

#define __LOGGING_THREAD_IO_READ_SIMPLE(E) \
            if ( __logging_thread_io_read(src_fd, &(E), sizeof(E), &should_delete) > 0 ) { \
                is_reading = false; \
                ERROR("failure to read from %s (errno = %d)", src_file_path, errno); \
                break; \
            } else { \
                total_bytes_read += sizeof(E); \
            }

#define __LOGGING_THREAD_IO_READ_BUFFER(E, L) \
            if ( __logging_thread_io_read(src_fd, &(L), sizeof(L), &should_delete) > 0 ) { \
                is_reading = false; \
                ERROR("failure to read from %s (errno = %d)", src_file_path, errno); \
                break; \
            } else { \
                total_bytes_read += sizeof(L); \
                (E) = malloc((size_t)(L)); \
                if ( (E) ) { \
                    if ( __logging_thread_io_read(src_fd, (void*)(E), (size_t)(L), &should_delete) > 0 ) { \
                        is_reading = false; \
                        ERROR("failure to read from %s (errno = %d)", src_file_path, errno); \
                        break; \
                    } else { \
                        total_bytes_read += (L); \
                    } \
                } else { \
                    is_reading = false; \
                    ERROR("failure to allocate buffer for read from %s (errno = %d)", src_file_path, errno); \
                    break; \
                } \
            }

bool
logging_thread_control_unserialize_queue_from_file(
    logging_thread_control_t    *thread_controller,
    const char                  *src_file_path
)
{
    bool                        all_okay = false;
    int                         rc;
    struct stat                 src_finfo;

    // Does the file exist?
    rc = stat(src_file_path, &src_finfo);
    if ( rc == 0 ) {
        // Open for reading:
        int                     src_fd = open(src_file_path, O_RDONLY);
        
        if ( src_fd >= 0 ) {
            bool                is_reading = true, should_delete = false;
            job_data_t          new_data;
            size_t              total_bytes_read = 0;
            
            // Get the lock on the queue:
            rc = pthread_mutex_lock(&thread_controller->queue_mutex);
            DEBUG("MUTEX_LOCK(QUEUE) = %d", rc);
            
            while ( is_reading ) {         
                memset(&new_data, 0, sizeof(new_data));
                while ( 1 ) {
                    size_t          std_err_len;
                
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.job_data_variant);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.job_id);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.account);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.owner_uid);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.billed_su);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.rdr_type);
                    __LOGGING_THREAD_IO_READ_SIMPLE(new_data.open_mode);
                    __LOGGING_THREAD_IO_READ_BUFFER(new_data.std_err, std_err_len);
                    break;
                }
                if ( is_reading ) {
                    // We have a new record to add:
                    job_data_record_t     *new_record = __logging_thread_control_alloc_job_data_record(thread_controller);
                    
                    if ( new_record ) {
                        new_record->queued_time = time(NULL);
                        new_record->job_data = new_data;
                        if ( thread_controller->queued_tail ) {
                            thread_controller->queued_tail->link = new_record;
                            thread_controller->queued_tail = new_record;
                        } else {
                            thread_controller->queued_records = thread_controller->queued_tail = new_record;
                        }
        
                        // Update counts:
                        thread_controller->n_queued++;
                        thread_controller->n_bulk++;
                    } else {
                        is_reading = false;
                        ERROR("FAILED TO ALLOCATE JOB DATA RECORD {jobid=%lu,owner_uid=%u,billed_su=%llu,account='%s'} AT %s:%llu",
                                (unsigned long)new_data.job_id,
                                (unsigned)new_data.owner_uid,
                                (unsigned long long)new_data.billed_su,
                                new_data.account,
                                src_file_path, (unsigned long long)total_bytes_read
                            );
                    }
                }
            }
            close(src_fd);
            
            if ( should_delete ) unlink(src_file_path);
            
            INFO("QUEUE CONTROL:  added %d records from %s", thread_controller->n_bulk, src_file_path);
            while ( thread_controller->n_bulk > 0 ) {
                // Signal the condition to wake at least one thread:
                rc = pthread_cond_signal(&thread_controller->queue_cond);
                thread_controller->n_bulk--;
            }

            // Unlock the queue mutex:
            rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
            DEBUG("MUTEX_UNLOCK(QUEUE) = %d", rc);
        } else {
            ERROR("error opening queue file %s (errno = %d)", src_file_path, errno);
        }
    }
    else if ( errno == ENOENT ) {
        all_okay = true;
        INFO("queue file %s not present", src_file_path);
    }
    else {
        ERROR("error checking existence of queue file %s (errno = %d)", src_file_path, errno);
    }
    return all_okay;
}

//

size_t
__logging_thread_io_write(
    int                 fd,
    const void          *buffer,
    size_t              buffer_len
)
{
    while ( buffer_len > 0 ) {
        ssize_t         bytes_written = write(fd, buffer, buffer_len);
                
        if ( (bytes_written < 0) && (errno != EINTR) ) break;
        buffer_len -= bytes_written;
        buffer += bytes_written;
    }
    return buffer_len;
}

//

#define __LOGGING_THREAD_IO_WRITE_SIMPLE(E) \
            if ( __logging_thread_io_write(dst_fd, &(E), sizeof(E)) > 0 ) { \
                output_records_to_error = true; \
                ERROR("failure to write to %s (errno = %d)", dst_file_path, errno); \
                break; \
            }

#define __LOGGING_THREAD_IO_WRITE_BUFFER(E, L) \
            if ( __logging_thread_io_write(dst_fd, &(L), sizeof(L)) > 0 ) { \
                output_records_to_error = true; \
                ERROR("failure to write to %s (errno = %d)", dst_file_path, errno); \
                break; \
            } \
            if ( __logging_thread_io_write(dst_fd, (E), (L)) > 0 ) { \
                output_records_to_error = true; \
                ERROR("failure to write to %s (errno = %d)", dst_file_path, errno); \
                break; \
            }

bool
logging_thread_control_serialize_queue_to_file(
    logging_thread_control_t    *thread_controller,
    const char                  *dst_file_path
)
{
    off_t                       last_offset;
    bool                        all_okay = true, output_records_to_error = false;
    int                         rc, dst_fd;
    
    // Get the lock on the queue:
    while ( 1 ) {
        rc = pthread_mutex_lock(&thread_controller->queue_mutex);
        DEBUG("MUTEX_LOCK(QUEUE) = %d", rc);
        
        // Don't accept additional records!
        thread_controller->is_queue_disabled = true;
        
        all_okay = true;
        
        if ( thread_controller->n_bulk > 0 ) {
            // Shift already-added bulk records into the queue:
            all_okay = logging_thread_control_push_job_data_bulk(thread_controller, NULL, true);
        }
        if ( all_okay ) {
            if ( thread_controller->n_in_progress > 0 ) {
                // Unlock the queue mutex and sleep for a few seconds:
                rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
                DEBUG("MUTEX_UNLOCK(QUEUE) = %d", rc);
            
                sleep(1);
            } else {
                // We're ready to serialize the queue now!
                break;
            }
        }
    }
    
    // When exiting the loop above we're holding the lock on the queue.
    // Open the destination file:
	dst_fd = open(dst_file_path, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
    if ( dst_fd >= 0 ) {
        job_data_record_t       *job_data_record = thread_controller->queued_records;
        unsigned int            n_written = 0;
        
        // Grab the starting point:
        last_offset = lseek(dst_fd, (off_t)0, SEEK_CUR);
        
        // Walk the queue, serializing each record:
        while ( job_data_record ) {
            job_data_record_t   *next_record = job_data_record->link;
            
            while ( 1 ) {
                size_t          std_err_len = job_data_record->job_data.std_err ? strlen(job_data_record->job_data.std_err) : 0;
                
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.job_data_variant);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.job_id);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.account);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.owner_uid);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.billed_su);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.rdr_type);
                __LOGGING_THREAD_IO_WRITE_SIMPLE(job_data_record->job_data.open_mode);
                __LOGGING_THREAD_IO_WRITE_BUFFER(job_data_record->job_data.std_err, std_err_len);
                break;
            }
            
            // If we exit the inner loop with bytes left unwritten, then there's
            // a problem afoot; truncate the file at the end of the last-written
            // record so there's no partial records:
            if ( output_records_to_error ) {
                rc = ftruncate(dst_fd, last_offset);
                break;
            }
            
            // Note the current offset in case we have to roll back on a failed
            // record write:
            last_offset = lseek(dst_fd, (off_t)0, SEEK_CUR);
            
            // Remove the record we just wrote:
            thread_controller->queued_records = next_record;
            __logging_thread_control_reset_job_data_record(thread_controller, job_data_record, false);
            
            n_written++;
            job_data_record = next_record;
        }
        
        // Close the file:
        close(dst_fd);
        
        INFO("WROTE %u JOB DATA RECORDS TO %s", n_written, dst_file_path);
        
        // If we had an error during write and there are records left then let's write
        // 'em to the error log:
        if ( output_records_to_error ) {
            while ( job_data_record ) {
                job_data_record_t *next_record = job_data_record->link;

                ERROR("!! UNHANDLED JOB DATA RECORD {jobid=%lu,owner_uid=%u,billed_su=%llu,account='%s'}",
                        (unsigned long)job_data_record->job_data.job_id,
                        (unsigned)job_data_record->job_data.owner_uid,
                        (unsigned long long)job_data_record->job_data.billed_su,
                        job_data_record->job_data.account
                    );
                __logging_thread_control_reset_job_data_record(thread_controller, job_data_record, false);
                job_data_record = next_record;
            }
        }
        
        // Ensure the queue is absolutely empty:
        thread_controller->n_queued = 0;
        thread_controller->queued_records = thread_controller->queued_tail = NULL;
        
    } else {
        ERROR("unable to open queue save file %s (errno = %d)", dst_file_path, errno);
        all_okay = false;
        thread_controller->is_queue_disabled = true;
    }
    rc = pthread_mutex_unlock(&thread_controller->queue_mutex);
    DEBUG("MUTEX_UNLOCK(QUEUE) = %d", rc);
    return all_okay;
}

/*!
 * @function __logging_maintenance_thread_entrypoint
 *
 * Execution entry point for a thread_controller instance's maintenance thread.  The thread
 * sleeps for an interval then wakes to check whether it's still supposed to be executing.
 * If so, it calls the thread cleanup function, logging_thread_control_check_threads() to
 * terminate/spawn logging threads.
 */
void*
__logging_maintenance_thread_entrypoint(
    void                *context
)
{
    logging_thread_control_t    *controller = (logging_thread_control_t*)context;
    int                         rc;
    bool                        is_running = true;
    
    
    
    while ( is_running ) {
        // Still running?
        rc = pthread_mutex_lock(&controller->threads_mutex);
        is_running = ! controller->should_maintenance_exit;
        rc = pthread_mutex_unlock(&controller->threads_mutex);
        
        if ( is_running ) {
            struct timespec     cond_timeout;
            
            logging_thread_control_check_threads(controller);
            
            rc = pthread_mutex_lock(&controller->threads_mutex);
            cond_timeout.tv_nsec = 0;
            cond_timeout.tv_sec = time(NULL) + controller->maintenance_interval;
            DEBUG("MAINTENANCE THREAD SLEEP");
            rc = pthread_cond_timedwait(&controller->maintenance_wakeup, &controller->threads_mutex, &cond_timeout);
            DEBUG("MAINTENANCE THREAD AWAKE (rc = %d)", rc);
        
            // Still running?
            is_running = ! controller->should_maintenance_exit;
        }
        rc = pthread_mutex_unlock(&controller->threads_mutex);
    }
    return NULL;
}
    
//

#ifdef LOGGING_THREAD_TESTING

#include <signal.h>

static bool is_running = true;

void
__handle_signal(
    int     sig_info
)
{
    DEBUG(".......signal %d caught........", sig_info);
    is_running = false;
}

//

int
main(
    int             argc,
    char * const    argv[]
)
{
    const char                  *thread_control_opts = NULL;
    logging_thread_control_t    *thread_controller = NULL;
    const char*                 accounts[] = { "group1", "group2", "group3", "group4", "group5" };
    int                         accounts_len = sizeof(accounts) / sizeof(const char*);
    
    
    if ( argc > 1 ) {
        thread_control_opts = argv[1];
    }
    thread_controller = logging_thread_control_init(thread_control_opts);
    
    if ( thread_controller ) {
        signal(SIGINT, __handle_signal);
        logging_thread_control_unserialize_queue_from_file(thread_controller, "unhandled_records.qdata");
        while ( is_running ) {
            if ( (getchar() == '\n') && is_running ) {
                double  f = ((double)rand() - (double)INT_MIN) / (((double)INT_MAX - (double)INT_MIN));
                int     i = ceil(8.0 * f);
                
                DEBUG("f = %f, i = %d", f, i);
                while ( i ) {
                    job_data_t   new_data = {
                                            .job_data_variant = rand() % job_data_variant_max,
                                            .job_id = rand(),
                                            .owner_uid = rand(),
                                            .billed_su = rand(),
                                            .rdr_type = rand() % job_rdr_max
                                        };
                    int                 account_idx = floor((double)accounts_len * (double)rand() * (2.0 / ((double)INT_MAX - (double)INT_MIN)));
                    
                    strcpy(new_data.account, accounts[account_idx]);
                    logging_thread_control_push_job_data_bulk(thread_controller, &new_data, (i == 1));
                    i--;
                }
                printf("==> %4d threads | queue time: %lf ± %lf s | request time: %lf ± %lf s\n",
                        thread_controller->n_threads,
                        thread_controller->queue_timing_stats.m_current, sqrt(thread_controller->queue_timing_stats.s_sqrd_current),
                        thread_controller->request_timing_stats.m_current, sqrt(thread_controller->request_timing_stats.s_sqrd_current));
            }
            DEBUG("NEXT ITERATION");
        }
        DEBUG("EXITED RUNLOOP");
        logging_thread_control_serialize_queue_to_file(thread_controller, "unhandled_records.qdata");
        DEBUG("TERMINATING THREAD CONTROLLER");
        logging_thread_control_fini(thread_controller);
    }    
    return 0;
}

#endif /* LOGGING_THREAD_TESTING */
