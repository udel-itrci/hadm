IF(NOT JSON_C_ROOT)
    SET(JSON_C_ROOT "/usr" CACHE PATH "Installation path of json-c library")
ENDIF()
FIND_FILE(
        JSON_C_HEADER
        NAMES json-c/json.h
        PATHS ${JSON_C_ROOT} ENV JSON_C_ROOT
        PATH_SUFFIXES include
        DOC "The json-c header file"
    )
IF(JSON_C_HEADER)
    # The actual base include dir is two levels back:
    GET_FILENAME_COMPONENT(JSON_C_INCLUDE_DIR ${JSON_C_HEADER} DIRECTORY)
    GET_FILENAME_COMPONENT(JSON_C_INCLUDE_DIR ${JSON_C_INCLUDE_DIR} DIRECTORY)
    
    FIND_LIBRARY(
            JSON_C_LIBRARY
            NAMES json-c
            PATHS ${JSON_C_ROOT} ENV JSON_C_ROOT
            PATH_SUFFIXES lib lib64
            DOC "The json-c library"
        )
ENDIF()

# handle the QUIETLY and REQUIRED arguments and set JSON_C_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
        JSON_C
        REQUIRED_VARS JSON_C_LIBRARY JSON_C_INCLUDE_DIR
    )

IF(JSON_C_FOUND)
    SET(JSON_C_LIBRARIES ${JSON_C_LIBRARY})
    SET(JSON_C_INCLUDE_DIRS ${JSON_C_INCLUDE_DIR})
ENDIF()