
#include "logging_thread.h"

#include "slurm/slurm.h"
#include "slurm/slurm_errno.h"
#include "src/slurmctld/slurmctld.h"
#include "src/slurmctld/state_save.h"
#include "src/common/xstring.h"

#include "src/common/prep.h"

/*
 * These variables are required by the generic plugin interface.  If they
 * are not found in the plugin, the plugin loader will ignore it.
 *
 * plugin_name - a string giving a human-readable description of the
 * plugin.  There is no maximum length, but the symbol must refer to
 * a valid string.
 *
 * plugin_type - a string suggesting t#include <time.h>he type of the plugin or its
 * applicability to a particular form of data or method of data handling.
 * If the low-level plugin API is used, the contents of this string are
 * unimportant and may be anything.  Slurm uses the higher-level plugin
 * interface which requires this string to be of the form
 *
 *      <application>/<method>
 *
 * where <application> is a description of the intended application of
 * the plugin (e.g., "auth" for Slurm authentication) and <method> is a
 * description of how this plugin satisfies that application.  Slurm will
 * only load authentication plugins if the plugin_type string has a prefix
 * of "auth/".
 *
 * plugin_version - an unsigned 32-bit integer containing the Slurm version
 * (major.minor.micro combined into a single number).
 */

const char plugin_name[] = "HADM PrEp plugin";
const char plugin_type[] = "prep/hadm";
const uint32_t plugin_version = SLURM_VERSION_NUMBER;

const char serialized_queue_file[] = "hadm_logging_queue.bin";

static logging_thread_control_t     *logging_thread_controller = NULL;

//

typedef struct {
    const char      *partition_name;
    job_rdr_t       partition_rdr_type;
} partition_to_rdr_t;
partition_to_rdr_t                  partition_to_rdr_map[] = {
                                        { "standard",       job_rdr_cpu },
                                        { "large-mem",      job_rdr_cpu },
                                        { "xlarge-mem",     job_rdr_cpu },
                                        { "extended-mem",   job_rdr_cpu },
                                        //
                                        { "gpu-t4",         job_rdr_gpu },
                                        { "gpu-v100",       job_rdr_gpu },
                                        { "gpu_mi50",       job_rdr_gpu },
                                        { "gpu_mi100",      job_rdr_gpu },
                                        //
                                        { NULL,             0           }
                                    };

static job_rdr_t
__partition_to_rdr_lookup(
    const char  *partition_name
)
{
    unsigned int    i = 0;
    
    while ( partition_to_rdr_map[i].partition_name ) {
        if ( strcasecmp(partition_name, partition_to_rdr_map[i].partition_name) == 0 ) return partition_to_rdr_map[i].partition_rdr_type;
        i++;
    }
    return job_rdr_unknown;
}

//

static bool                         have_prolog_slurmctld = false;
static bool                         have_epilog_slurmctld = false;

extern void
prep_p_register_callbacks(
    prep_callbacks_t    *callbacks
)
{
    // Register the callbacks:
    if ( callbacks->prolog_slurmctld ) {
        logging_thread_register_prep_callback(logging_thread_prep_callback_type_prolog, callbacks->prolog_slurmctld);
        have_prolog_slurmctld = true;
    }
    if ( callbacks->epilog_slurmctld ) {
        logging_thread_register_prep_callback(logging_thread_prep_callback_type_epilog, callbacks->epilog_slurmctld);
        have_epilog_slurmctld= true;
    }
}


/*
 * The following base-2 to base-10 fast-logarithm digit-counting function is adapted from
 *
 *     https://stackoverflow.com/questions/25892665/performance-of-log10-function-returning-an-int
 *
 * to determine how many digits an integer value possesses.  We use the resulting value <N> to
 * determine how many additional digits to the right of the decimal to retain in calculating SU
 * usage from TRES counts and wall time.
 */
unsigned int
__baseTwoDigits(
    unsigned long long  x
)
{
    return x ? 64 - __builtin_clzll(x) : 0;
}

static unsigned int __baseTenDigits(uint64_t x) {
    static const uint8_t guess[65] = {
         0,  0,  0,  0,  1,  1,  1,  2,  2,  2,
         3,  3,  3,  3,  4,  4,  4,  5,  5,  5,
         6,  6,  6,  6,  7,  7,  7,  8,  8,  8,
         9,  9,  9,  9, 10, 10, 10, 11, 11, 11,
        12, 12, 12, 12, 13, 13, 13, 14, 14, 14,
        15, 15, 15, 15, 16, 16, 16, 17, 17, 17,
        18, 18, 18, 18, 19
    };
    static const uint64_t tenToThe[20] = {
                0ULL,  /* Yes,that's NOT 10^0, but we need to account for 0's being a single digit! */
                10ULL,
                100ULL,
                1000ULL,
                10000ULL,
                100000ULL,
                1000000ULL,
                10000000ULL,
                100000000ULL,
                1000000000ULL,
                10000000000ULL,
                100000000000ULL,
                1000000000000ULL,
                10000000000000ULL,
                100000000000000ULL,
                1000000000000000ULL,
                10000000000000000ULL,
                100000000000000000ULL,
                1000000000000000000ULL,
                10000000000000000000ULL
    };
    unsigned int digits = guess[__baseTwoDigits((unsigned long long)x)];
    return digits + (x >= tenToThe[digits]);
}


extern int prep_p_prolog(job_env_t *job_env, slurm_cred_t *cred)
{
    return SLURM_SUCCESS;
}

extern int prep_p_epilog(job_env_t *job_env, slurm_cred_t *cred)
{
    return SLURM_SUCCESS;
}


extern int
prep_p_prolog_slurmctld(
    job_record_t        *job_ptr,
    bool                *async
)
{
    job_data_t          the_job_data = {
                            .job_data_variant = job_data_variant_predebit,
                            .job_id = job_ptr->job_id,
                            .owner_uid = job_ptr->user_id,
                            //
                            .std_err = NULL,
                            .open_mode = job_ptr->details->open_mode
                        };
    double              delta_t;
    double              su_raw, su_raw_roundup;
    unsigned int        n_digits;
    const char          *std_err_path = NULL;
    
    // Lookup the RDR type for the partition; if the partition is not one with
    // which we associate an RDR, then don't bother logging anything:
    the_job_data.rdr_type = __partition_to_rdr_lookup(job_ptr->part_ptr->name);
    if ( the_job_data.rdr_type == job_rdr_unknown ) {
        *async = false;
        return SLURM_SUCCESS;
    }
    
    // Jobs that run forever can't be handled...but we should never see any,
    // anyway:
    if ( job_ptr->time_limit == INFINITE ) {
        *async = false;
        return SLURM_SUCCESS;
    }
    if ( job_ptr->time_limit == NO_VAL ) {
        delta_t = job_ptr->part_ptr->default_time;
    } else {
        delta_t = job_ptr->time_limit;
    }
    
    // Does the job have a known GraceTime coming from the job QOS,
    // partition QOS, or partition itself?
    if ( job_ptr->qos_ptr && (job_ptr->qos_ptr->grace_time > 0) ) {
        delta_t += job_ptr->qos_ptr->grace_time;
    }
    else if ( job_ptr->part_ptr->qos_ptr && (job_ptr->part_ptr->qos_ptr->grace_time > 0) ) {
        delta_t += job_ptr->part_ptr->qos_ptr->grace_time;
    }
    else if ( job_ptr->part_ptr->grace_time > 0 ) {
        delta_t += job_ptr->part_ptr->grace_time;
    }
    
    if ( ! have_prolog_slurmctld ) {
        *async = false;
        error("%s: async PREP functionality is required by this plugin", plugin_type);
        return SLURM_ERROR;
    }
    
    // Fill-in the account:
    strncpy(the_job_data.account, job_ptr->account, JOB_DATA_RECORD_ACCOUNT_CAPACITY);

#ifdef ENABLE_CONSTRUCTED_HADM_STDERR
    // Construct the standard error path:
    the_job_data.std_err = std_err_path = xstrdup_printf("%s/hadm-accounting-%u.err", job_ptr->details->work_dir, job_ptr->job_id);
    info("std_err_path = %s", std_err_path);
#endif /* ENABLE_CONSTRUCTED_HADM_STDERR */
    
    // By now resources have been allocated to the job, so we should know the
    // billable TRES:
    su_raw = job_ptr->billable_tres * delta_t;
    
    // Try to retain only 10 significant digits; this tends to help eliminate round-up
    // error in the ceil() call:
    n_digits = __baseTenDigits((uint64_t)su_raw);
    if ( n_digits <= 10 ) {
        unsigned int    i_digits;
        
        i_digits = n_digits; while ( i_digits ) su_raw *= 10.0, i_digits--;
        su_raw = floor(su_raw);
        i_digits = n_digits; while ( i_digits ) su_raw *= 0.1, i_digits--;
    }
    the_job_data.billed_su = ceil(su_raw);
    
    // Log the record:
    if ( ! logging_thread_control_push_job_data(logging_thread_controller, &the_job_data) ) {
        error("%s: unhandled pre-debit {jobid=%lu,owner_uid=%u,billed_su=%llu,account='%s',rdr=%d}",
                plugin_type,
                (unsigned long)job_ptr->job_id,
                (unsigned)job_ptr->user_id,
                (unsigned long long)the_job_data.billed_su,
                job_ptr->account,
                (int)the_job_data.rdr_type
            );
        if ( std_err_path ) xfree(std_err_path);
        *async = false;
        return SLURM_ERROR;
    }
    /* The call to logging_thread_control_push_job_data() will have duplicated
     * the job record we constructed here -- including the std_err field.  So we
     * can get rid of our copy of that path:
     */
    if ( std_err_path ) xfree(std_err_path);
    *async = true;
    return SLURM_SUCCESS;
}


extern int
prep_p_epilog_slurmctld(
    job_record_t        *job_ptr,
    bool                *async
)
{
    job_data_t          the_job_data = {
                            .job_data_variant = job_data_variant_completion,
                            .job_id = job_ptr->job_id,
                            .owner_uid = job_ptr->user_id,
                            //
                            .std_err = NULL,
                            .open_mode = job_ptr->details->open_mode
                        };
    double              delta_t = job_ptr->end_time - job_ptr->start_time - job_ptr->tot_sus_time;
    double              su_raw = job_ptr->billable_tres * (delta_t / 60), su_raw_roundup;
    unsigned int        n_digits;
    const char          *std_err_path = NULL;
    
    // Lookup the RDR type for the partition; if the partition is not one with
    // which we associate an RDR, then don't bother logging anything:
    the_job_data.rdr_type = __partition_to_rdr_lookup(job_ptr->part_ptr->name);
    if ( the_job_data.rdr_type == job_rdr_unknown ) {
        *async = false;
        return SLURM_SUCCESS;
    }
    
    if ( ! have_epilog_slurmctld ) {
        *async = false;
        error("%s: async PREP functionality is required by this plugin", plugin_type);
        return SLURM_ERROR;
    }
    
    // Fill-in the account:
    strncpy(the_job_data.account, job_ptr->account, JOB_DATA_RECORD_ACCOUNT_CAPACITY);
    
    // Construct the standard error path:
    the_job_data.std_err = std_err_path = xstrdup_printf("%s/hadm-accounting-%u.err", job_ptr->details->work_dir, job_ptr->job_id);
    
    // Should we actually bill for this job?
    switch ( job_ptr->job_state & JOB_STATE_BASE ) {
        
        case JOB_NODE_FAIL:
        case JOB_BOOT_FAIL: {
            the_job_data.billed_su = 0;
            break;
        }
        
        default: {
            // Try to retain only 10 significant digits; this tends to help eliminate round-up
            // error in the ceil() call:
            n_digits = __baseTenDigits((uint64_t)su_raw);
            if ( n_digits <= 10 ) {
                unsigned int    i_digits;
        
                i_digits = n_digits; while ( i_digits ) su_raw *= 10.0, i_digits--;
                su_raw = floor(su_raw);
                i_digits = n_digits; while ( i_digits ) su_raw *= 0.1, i_digits--;
            }
            the_job_data.billed_su = ceil(su_raw);
    
            // If the delta-t was non-zero, then a zero billed SU is not allowed:
            if ( (the_job_data.billed_su == 0) && (delta_t > 0.0) ) {
                the_job_data.billed_su = 1;
            }
            break;
        }
    
    }
    
    // Log the record:
    if ( ! logging_thread_control_push_job_data(logging_thread_controller, &the_job_data) ) {
        error("%s: unhandled completion {jobid=%lu,owner_uid=%u,billed_su=%llu,account='%s',rdr=%d}",
                plugin_type,
                (unsigned long)job_ptr->job_id,
                (unsigned)job_ptr->user_id,
                (unsigned long long)the_job_data.billed_su,
                job_ptr->account,
                (int)the_job_data.rdr_type
            );
        xfree(std_err_path);
        *async = false;
        return SLURM_ERROR;
    }
    
    xfree(std_err_path);
    *async = true;
    return SLURM_SUCCESS;
}


extern void
prep_p_required(
    prep_call_type_t    type,
    bool                *required
)
{
    *required = false;
    switch ( type ) {
        case PREP_PROLOG_SLURMCTLD:
        case PREP_EPILOG_SLURMCTLD:
            if ( running_in_slurmctld() ) *required = true;
            break;
    }
}

/*
 * This is a MASSIVE HACK -- how do we know if we're being loaded in
 * slurmctld versus slurmd?  We check glibc's internal string constant
 * for the name of the executable, of course:
 */
extern const char *__progname;
/*
 * init() is called when the plugin is loaded, before any other functions
 * are called. Put global initialization here.
 */
extern int init(void)
{
    int                 rc = SLURM_SUCCESS;

    if ( strstr(__progname, "slurmctld") ) {
        char            *serialized_queue_path = NULL;
    
        logging_thread_controller = logging_thread_control_init(slurm_conf.prep_params);
        if ( ! logging_thread_controller ) {
            error("%s: unable to initialize logging queue controller", plugin_type);
            return SLURM_ERROR;
        }
    
        // Reconstitute any saved completion records:
        xstrfmtcat(serialized_queue_path, "%s/%s", slurm_conf.state_save_location, serialized_queue_file);
        if ( ! logging_thread_control_unserialize_queue_from_file(logging_thread_controller, serialized_queue_path) ) {
            error("%s: failure while loading saved queued completion records into active queue", plugin_type);
            rc = SLURM_ERROR;
        }
        xfree(serialized_queue_path);
    }
    return rc;
}

/*
 * fini() is called when the plugin is unloaded, before any other functions
 * are called. Put global shutdown here.
 */
extern int fini(void)
{
    int                 rc = SLURM_SUCCESS;

    if ( strstr(__progname, "slurmctld") ) {
        info("%s: shutting down HADM PrEp plugin", plugin_type);
        if ( logging_thread_controller ) {
            char        *serialized_queue_path = NULL;
        
            // Write any unhandled completion records:
            xstrfmtcat(serialized_queue_path, "%s/%s", slurm_conf.state_save_location, serialized_queue_file);
            if ( ! logging_thread_control_serialize_queue_from_file(logging_thread_controller, serialized_queue_path) ) {
                error("%s: failure while writing unhandled completion records to %s", plugin_type, serialized_queue_file);
                rc = SLURM_ERROR;
            }
            xfree(serialized_queue_path);
        
            if ( ! logging_thread_control_fini(logging_thread_controller) ) rc = SLURM_ERROR;
            logging_thread_controller = NULL;
        }
    }
    return rc;
}
