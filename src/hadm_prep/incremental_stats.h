/*
 * incremental_stats
 * Maintain incremental statistics for a measurement.
 */

#ifndef __INCREMENTAL_STATS_H__
#define __INCREMENTAL_STATS_H__

#include <stdio.h>
#include <math.h>

/*!
 * @typedef incremental_stat_t
 * @discussion
 *     Data structure holding the mean and squared standard deviation
 *     for a measurement.  First- and second-derivative estimate fields
 *     are also maintained to describe the change in the two statistics
 *     as readings are added.
 */
typedef struct incremental_stat {
    double              m_current, dm, d2m;
    double              s_sqrd_current, ds_sqrd, d2s_sqrd;
    unsigned long long  n;
} incremental_stat_t;

/*!
 * @defined incremental_stat_init
 * @discussion
 *     Initialize the fields of an incremental_stat_t data structure, A,
 *     to represent an empty set of measurements.
 */
#define incremental_stat_init(A) \
            { (A).m_current = (A).s_sqrd_current = 0.0; (A).n = 0ULL; \
              (A).dm = (A).d2m = (A).ds_sqrd = (A).d2s_sqrd = 0.0; \
            }

/*!
 * @defined incremental_stat_append
 * @discussion
 *     Add a measurement, V, to the set represented by an incremental_stat_t
 *     data structure, A.
 *
 *         1 measurement:       m_current updated
 *         2 measurements:      m_current, s_sqrd_current, dm, ds_sqrd updated
 *        >2 measurements:      m_current, s_sqrd_current, dm, ds_sqrd, d2m,
 *                              d2s_sqrd updated
 */
#define incremental_stat_append(A, V) \
            { \
                double    m_next, s_sqrd_next = 0.0; \
                (A).n++; \
                switch ( (A).n ) { \
                    case 1: {\
                        m_next = (V); \
                        break; \
                    } \
                    default: { \
                        double    term1, term2; \
                        term1 = (A).s_sqrd_current * (double)((A).n - 2) / (double)((A).n - 1); \
                        term2 = ((V) - (A).m_current); term2 = term2 * term2 / (double)(A).n; \
                        s_sqrd_next = term1 + term2; \
                        m_next = (A).m_current + ((V) - (A).m_current) / (double)(((A).n)); \
                        break; \
                    } \
                } \
                if ( (A).n >= 2 ) { \
                    double  dm_next = m_next - (A).m_current; \
                    double  ds_sqrd_next = s_sqrd_next - (A).s_sqrd_current; \
                    if ( (A).n >= 3 ) { \
                        (A).d2m = dm_next - (A).dm; \
                        (A).d2s_sqrd = ds_sqrd_next - (A).ds_sqrd; \
                    } \
                    (A).dm = dm_next; \
                    (A).ds_sqrd = ds_sqrd_next; \
                } \
                (A).s_sqrd_current = s_sqrd_next; \
                (A).m_current = m_next; \
            }

/*!
 * @defined incremental_stat_fprintf
 * @discussion
 *     Display the information in an incremental_stat_t data structure, A, to
 *     the stdio stream, FP.  No newline is written to FP.
 */
#define incremental_stat_fprintf(FP, A) \
            fprintf( \
                (FP), \
                "%5llu %16.6lf±%-.6lf %16.6lf %16.6lf %16.6lf %16.6lf", \
                (A).n, (A).m_current, sqrt((A).s_sqrd_current), (A).dm, (A).d2m, (A).ds_sqrd, (A).d2s_sqrd \
            )

#endif /* __INCREMENTAL_STATS_H__ */
