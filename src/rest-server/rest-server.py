#
# hermes-allocations-datamgr
# DARWIN project resource allocations data manager API
#
# A REST API used to manage:
#
#     - XSEDE and local projects (a.k.a. workgroups or accounts)
#     - Resource allocations granted to projects
#         - Valid time range
#         - RDR type
#     - Billing activity against allocations
#         - Credits:  addition of funds
#         - Pre-debits:  subtraction of funds due to pending/running jobs
#         - Debits:  subtraction of funds, including completed jobs
#
import os
import pwd
import grp
import psycopg2
import psycopg2.extras
from psycopg2 import sql
from flask import Flask, Response, request, url_for, flash, redirect, jsonify, g as request_globals
from flask.json import JSONEncoder
from decimal import Decimal

HADM_PREFIX = os.getenv('HADM_PREFIX', '')

class hermes_errors:

    err_generic             = -1
    err_database	        = -2
    
    err_invalid_auth        = -100
    err_missing_auth        = -101
    err_not_authorized      = -102
    
    err_invalid_data        = -200
    err_incomplete_data     = -201
    err_unknown_job_id      = -202
    err_no_allocation_avail = -203 
    


#
# Access checks:
#
def is_superuser(request_uid):
    """Users with superuser privilege have access to all entities and can create new objects.  The list of valid uids is built once per-session and cached."""
    if not hasattr(request_globals, 'superusers'):
        request_globals.superusers =(
                    pwd.getpwnam('root').pw_uid,
                    pwd.getpwnam('_slurmadm').pw_uid,
                    pwd.getpwnam('frey').pw_uid,
                    pwd.getpwnam('pdw').pw_uid,
                )
    return request_uid in request_globals.superusers
    
def is_admin(request_uid):
    """Users with admin privilege have at least read-only access to all entities (jobs, projects, allocations).  The list of valid uids is built once per-session and cached."""
    if is_superuser(request_uid):
        return True
    if not hasattr(request_globals, 'admin_users'):
        request_globals.admin_users =(
                    pwd.getpwnam('anita').pw_uid,
                    pwd.getpwnam('mkyle').pw_uid,
                    pwd.getpwnam('jnhuffma').pw_uid,
                )
    return request_uid in request_globals.admin_users
    
#
# Uid and Gid parsing and handling:
#
def getpw_for_user_id(user_id):
    """Returns the pwd record for a given uid number of user name."""
    try:
        return pwd.getpwuid(int(user_id))
    except:
        pass
    try:
        return pwd.getpwnam(user_id)
    except Exception as E:
        raise E
        
def parse_uid_number(user_id):
    """Turn a uid number or user name into the appropriate textual user name.  Raises an exception if the user does not exist.  Primarily used in conjunction with gids_for_uid() since it requires the user name, not the uid number."""
    try:
        return int(user_id)
    except:
        pass
    try:
        user_info = pwd.getpwnam(user_id)
        return user_info.pw_uid
    except Exception as E:
        raise E
        
def parse_uid_name(user_id):
    """Turn a uid number or user name into the appropriate integral uid name.  Raises an exception if the user does not exist."""
    try:
        user_info = pwd.getpwuid(int(user_id))
    except:
        try:
            user_info = pwd.getpwnam(user_id)
        except Exception as E:
            raise E
    return user_info.pw_name
    
def parse_gid_number(group_id):
    """Turn a gid number or group name into the appropriate integral gid number.  Raises an exception if the group does not exist."""
    try:
        return int(group_id)
    except:
        pass
    try:
        group_info = grp.getgrnam(group_id)
        return group_info.gr_gid
    except Exception as E:
        raise E

def filter_workgroup_gids(gids):
    """Filter a list of integral gid numbers to remove any that are not workgroup-oriented."""
    return [gid for gid in gids if gid > 1000]

def gids_for_uid(request_uid, request_gid):
    """Return an iterable containing all auxilliary group ids for a given user."""
    return filter_workgroup_gids(os.getgrouplist(parse_uid_name(request_uid), request_gid))


#
# JSON error response:
#
def error_response(err_msg, status=400, err_code=-1):
    """Return a Flask response tuple wrapping the given error message/code."""
    error_obj = { 'err_code': err_code, 'err_msg': err_msg }
    return (jsonify(error_obj), status, {'mimetype': 'application/json'})


#
# We need our own JSON encoder that will turn Decimal values (credits/debits and
# balances) into native Python integers.
#
class ExtendedJSONEncoder(JSONEncoder):
    """Postgres returns the balance fields as Decimal values, but JSON doesn't know to what it should encode them.  Since all balances are integers, we simply cast those values to integer."""
    def default(self, obj):
        if isinstance(obj, Decimal):
            return int(obj)
        return super(ExtendedJSONEncoder, self).default(obj)



#
# Our application:
#
app = Flask('hermes_allocations_datamgr')
app.json_encoder = ExtendedJSONEncoder

#
# Database connection(s) are handled by a class wrapper that
# automatically connects and has simplified query member
# functions.
#
class AppDatabase(object):

    _conn = None
    _cursor = None
    
    @staticmethod
    def read_password_file():
        """Read the database password from a file in the working directory."""
        try:
            with open(os.path.join(HADM_PREFIX, 'etc/hermes_allocations_datamgr.passwd')) as passwd_file:
                return passwd_file.read().strip()
        except Exception as E:
            raise RuntimeError('Password file not present in working directory ({:s})'.format(str(E)))

    @classmethod
    def db_connection(cls):
        """Open a connection to the database."""
        db_url = 'postgresql://allocations:{:s}@hadm-db/allocations'.format(cls.read_password_file())
        return psycopg2.connect(dsn=db_url)
    
    @staticmethod
    def database_in_context():
        """Use a unique database in each Flask application context so that transaction blocks are handled predictably and properly."""
        if not hasattr(request_globals, 'database'):
            request_globals.database = AppDatabase()
        return request_globals.database
        
    def __init__(self):
        """Initialize a new instance of the class with its own database connection and cursor."""
        self._conn = AppDatabase.db_connection()
        self._cursor = self._conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    
    def __del__(self):
        """Destructor for an instance of the class; close the cursor and database connection."""
        if self._conn:
            if self._cursor:
                del self._cursor
            del self._conn
    
    def reset_cursor(self):
        """Discard the current database cursor and create a new one."""
        del self._cursor
        self._cursor = self._conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    
    def query(self, query_str, query_arguments, should_commit=False):
        """Perform a query, returning all tuples produced.  If should_commit is True, then the parent connection for this instance's cursor will have its commit method called to commit the current transaction."""
        self._cursor.execute(query_str, query_arguments)
        results = self._cursor.fetchall()
        if should_commit:
            self._conn.commit()
        return results
    
    def query_as_boolean(self, query_str, query_arguments, should_commit=False):
        """Perform a query that does not return any tuples.  If should_commit is True, then the parent connection for this instance's cursor will have its commit method called to commit the current transaction."""
        self._cursor.execute(query_str, query_arguments)
        if should_commit:
            self._conn.commit()
        return True
    
    def commit(self):
        """Explicitly ask the instance's parent connection to commit the current transaction."""
        self._conn.commit()
    
    def rollback(self):
        """Explicitly ask the instance's parent connection to rollback the current transaction."""
        self._conn.rollback()
            

@app.before_request
def decode_munge_hash():
    """Given a MUNGE-ified URL hash from the HTTP headers and the URL it's supposed to encode, decode and verify the veracity of those data.  If successful, the decoded pymunge tuple is returned.  If the header doesn't exist False is returned.

Various exceptions may be thrown, as well, if pymunge cannot decode the header value or the encoded URL doesn't match the requested URL."""
    global  munge_credentials
    
    #
    # Check for a munge header in the request:
    #
    request_globals.munge_credentials = None
    munge_hash = request.headers.get('MungeUrlHash')
    if munge_hash is not None:
        from pymunge import decode as munge_decode
        
        decoded_hash = munge_decode(munge_hash.encode('ASCII'))
            
        # We decoded the hash, make sure the URL is correct:
        if request.url.encode('ASCII') == decoded_hash[0]:
            # Is this an admin or superuser it's possible s/he is posing
            # the request as a different user:
            run_as = request.headers.get('X-HADM-RunAs')
            if run_as is not None and len(run_as) > 0:
                # Validate that it's actually a real user:
                try:
                    run_as_uid_number = parse_uid_number(run_as)
                except:
                    return error_response('ERROR:  run as target user is invalid')
                    
                if is_superuser(decoded_hash[1]):
                    pass
                elif is_admin(decoded_hash[1]):
                    if is_superuser(run_as_uid_number):
                        return error_response('ERROR:  right, like we\'re going to let you run as a superuser')
                else:
                    return error_response('ERROR:  requesting user lacks privileges to run as')
                
                    
                # Substitute the target user id and baseline group:
                decoded_hash = (decoded_hash[0], run_as_uid_number, parse_gid_number('everyone'))
                
            request_globals.munge_credentials = decoded_hash
            return None
            
        return error_response('ERROR:  invalid MUNGE hash in request headers', err_code=hermes_errors.err_invalid_auth)
    #
    # We require a MUNGE header for authnz:
    #
    return error_response('ERROR:  missing MUNGE header', status=401, err_code=hermes_errors.err_missing_auth)


@app.route('/project', methods=['GET', 'POST'])
def handle_project():
    if request.method == 'POST':
        #
        # Creation of a project requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
            
            #
            # Validate the incoming data structure:
            #
            if not isinstance(input_json, dict):
                return error_response('ERROR:  Invalid project data structure', err_code=hermes_errors.err_invalid_data)
            
            if not all(k in input_json for k in ('project_id', 'gid', 'gname', 'account')):
                return error_response('ERROR:  Incomplete project data structure', err_code=hermes_errors.err_incomplete_data)
            
            query_result = AppDatabase.database_in_context().query('INSERT INTO projects (project_id, gid, gname, account) VALUES (%s, %s, %s, %s) RETURNING project_id', [int(input_json['project_id']), int(input_json['gid']), str(input_json['gname']), str(input_json['account'])], should_commit=True)
            if len(query_result) == 1:
                # The result will be the added proejct_id:
                query_result = AppDatabase.database_in_context().query('SELECT * FROM projects WHERE project_id = %s', [query_result[0]['project_id']])
                return jsonify(query_result[0])
        except Exception as E:
            return error_response('ERROR:  Unable to add project: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
        
    elif request.method == 'GET':
        #
        # Build the list of predicates for the query:
        #
        query_str = 'SELECT * FROM projects'
        predicate_str = ''
        query_arguments = []
    
        target_gids = set()
        valid_gids = []
    
        if is_admin(request_globals.munge_credentials[1]):
            #
            # Users in the admin role are allowed to specify uids to search against.  We need
            # to generate the aggregate set of gids to which each uid is a member and select
            # projects owned by those gids as our partial predicate:
            #
            if 'uid' in request.args:
                for uid in request.args.get('uid').split(','):
                    user_info = getpw_for_user_id(uid)
                    target_gids.update(gids_for_uid(user_info.pw_uid, user_info.pw_gid))
        else:
            #
            # Regular users restricted to ONLY those projects to which they are a member:
            #
            valid_gids = gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2])
    
        if 'gid' in request.args:
            #
            # Augment the list of target project gids with those in this list:
            #
            try:
                target_gids.update(filter_workgroup_gids([parse_gid_number(gid) for gid in request.args.get('gid').split(',')]))
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target gids: {:s}'.format(request.args.get('gid')))
                
        if len(target_gids) > 0:
            #
            # With all target gids now parsed and tallied, add the query predicate and arguments:
            #
            predicate_str = ' gid IN (' + '%s' + ',%s'*(len(target_gids)-1) + ')'
            query_arguments.extend(target_gids)
        
        if 'account' in request.args:
            #
            # Add a predicate that limits projects against a set of account names:
            #
            target_accounts = request.args.get('account').split(',')
            if len(target_accounts) > 0:
                predicate_str = predicate_str + (' OR' if len(predicate_str) > 0 else '') + ' account IN (%s' + ',%s'*(len(target_accounts)-1) + ')'
                query_arguments.extend(target_accounts)
            
        if 'project_id' in request.args:
            #
            # Add a predicate that limits by project_id:
            #
            try:
                target_project_ids = [int(v) for v in request.args.get('project_id').split(',')]
                if len(target_project_ids) > 0:
                    predicate_str = predicate_str + (' OR' if len(predicate_str) > 0 else '') + ' project_id IN (%s' + ',%s'*(len(target_project_ids)-1) + ')'
                    query_arguments.extend(target_project_ids)
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of project_ids: {:s}'.format(request.args.get('project_id')))
                
        #
        # If there are a limited number of valid gids for the requesting user, restrict based on those at the tail end
        # as an AND clause:
        #
        if len(valid_gids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND gid IN (%s' + ',%s'*(len(valid_gids)-1) + ')'
                query_arguments.extend(valid_gids)
            else:
                predicate_str = ' gid IN (%s' + ',%s'*(len(valid_gids)-1) + ')'
                query_arguments.extend(valid_gids)
    
        #
        # Generate the full query string and execute it:
        #
        query_str = query_str + (' WHERE' + predicate_str if len(predicate_str) > 0 else '')
        try:
            output_json = jsonify(AppDatabase.database_in_context().query(query_str, query_arguments))
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation lookup query: {:s}'.format(str(E)))
        
        return output_json
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))
    

@app.route('/project/<int:project_id>', methods=['GET'])
def handle_project_by_id(project_id):
    if request.method == 'GET':
        #
        # Validate access to the requested project; first we have to lookup the
        # record:
        #
        try:
            query_result = AppDatabase.database_in_context().query('SELECT * FROM projects_with_allocations WHERE project_id = %s', [project_id])
            if len(query_result) > 0:
                #
                # Is the user allowed to ask for this?
                #
                if is_admin(request_globals.munge_credentials[1]) or query_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                    return jsonify(query_result)
                return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            return error_response('ERROR:  no such project')
        except Exception as E:
            return error_response('ERROR:  Failed while performing project lookup query: {:s}'.format(str(E)))
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))
        

@app.route('/alloc', methods=['GET', 'POST'])
def handle_allocation():
    if request.method == 'POST':
        #
        # Creation of an allocation requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
            #
            # Validate the incoming data structure:
            #
            if not isinstance(input_json, dict):
                return error_response('ERROR:  Invalid allocation data structure', err_code=hermes_errors.err_invalid_data)
            
            if not all(k in input_json for k in ('category', 'resource', 'start_date', 'end_date')):
                return error_response('ERROR:  Incomplete project data structure', err_code=hermes_errors.err_incomplete_data)
            
            query_str = 'INSERT INTO allocations (project_id, category, resource, start_date, end_date) VALUES '
            query_arguments = []
            
            # Any one of these keys is acceptable:
            if not any(k in input_json for k in ('gid', 'gname', 'account')):
                if not 'project_id' in input_json:
                    return error_response('ERROR:  No parent project identifier in project data structure', err_code=hermes_errors.err_incomplete_data)
                
                # Solely based on project id, that's easy:
                query_str = query_str + ' (%s, %s, %s, %s, %s)'
                query_arguments.extend((int(input_json['project_id']), input_json['category'], input_json['resource'], input_json['start_date'], input_json['end_date']))
            
            else:
                # We'll need to lookup the project_id from the projects table:
                predicate = ''
                if 'project_id' in input_json:
                    predicate = ' project_id = %s'
                    query_arguments.append(int(input_json['project_id']))
                if 'gid' in input_json:
                    predicate = predicate + (' AND' if len(predicate) > 0 else '') + ' gid = %s'
                    query_arguments.append(int(input_json['gid']))
                if 'gname' in input_json:
                    predicate = predicate + (' AND' if len(predicate) > 0 else '') + ' gname = %s'
                    query_arguments.append(input_json['gname'])
                if 'account' in input_json:
                    predicate = predicate + (' AND' if len(predicate) > 0 else '') + ' account = %s'
                    query_arguments.append(input_json['account'])
                    
                query_str = query_str + ' ((SELECT project_id FROM projects WHERE' + predicate + '), %s, %s, %s, %s)'
                query_arguments.extend((input_json['category'], input_json['resource'], input_json['start_date'], input_json['end_date']))
            
            query_str = query_str + ' RETURNING allocation_id'
            query_result = AppDatabase.database_in_context().query(query_str, query_arguments, should_commit=True)
            if len(query_result) == 1:
                # The result will be the added allocatin_id:
                query_result = AppDatabase.database_in_context().query('SELECT * FROM allocations WHERE allocation_id = %s', [query_result[0]['allocation_id']])
                return jsonify(query_result[0])
        except Exception as E:
            return error_response('ERROR:  Unable to add allocation: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    elif request.method == 'GET':
        #
        # Build the list of predicates for the query:
        #
        query_str = 'SELECT * FROM allocations'
        predicate_str = ''
        query_arguments = []
    
        target_gids = set()
        target_accounts = set()
        valid_gids = []
        if is_admin(request_globals.munge_credentials[1]):
            #
            # Users in the admin role are allowed to specify uids to search against.  We need
            # to generate the aggregate set of gids to which each uid is a member and select
            # projects owned by those gids as our partial predicate:
            #
            if 'uid' in request.args:
                for uid in request.args.get('uid').split(','):
                    try:
                        user_info = getpw_for_user_id(uid)
                        target_gids.update(gids_for_uid(user_info.pw_uid, user_info.pw_gid))
                    except Exception as E:
                        return error_response('ERROR:  Invalid uid value: {:s}'.format(uid))
        else:
            #
            # Regular users restricted to ONLY those projects to which they are a member.  Generate
            # the set of gids to which the requesting uid is a member and we'll limit the results to
            # ONLY projects for those gids:
            #
            valid_gids = gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2])
        
        if 'gid' in request.args:
            #
            # Augment the list of target project gids with those in this list:
            #
            try:
                target_gids.update(filter_workgroup_gids([parse_gid_number(gid) for gid in request.args.get('gid').split(',')]))
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target gids: {:s}'.format(request.args.get('gid')))

        if 'account' in request.args:
            #
            # Add a predicate that limits projects against a set of account names
            #
            target_accounts.update(request.args.get('account').split(','))
            
        #
        # For allocations, the project table predicates require a sub-query against the projects
        # table:
        #
        if len(target_gids) + len(target_accounts):
            predicate_str = ' project_id IN (SELECT project_id FROM projects WHERE'
            if len(target_gids) > 0:
                predicate_str = predicate_str + ' gid IN (%s' + ',%s'*(len(target_gids)-1) + ')'
                query_arguments.extend(target_gids)
            if len(target_accounts) > 0:
                predicate_str = predicate_str + (' OR' if len(target_gids) > 0 else '') + ' account IN (' + '%s' + ',%s'*(len(target_accounts)-1) + ')'
                query_arguments.extend(target_accounts)
            predicate_str = predicate_str + ')'
                
        #
        # Add a predicate that limits by project_id:
        #
        if 'project_id' in request.args:
            try:
                target_project_ids = [int(v) for v in request.args.get('project_id').split(',')]
                if len(target_project_ids) > 0:
                    predicate_str = predicate_str + (' OR' if len(predicate_str) > 0 else '') + ' project_id IN (%s' + ',%s'*(len(target_project_ids)-1) + ')'
                    query_arguments.extend(target_project_ids)
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target project_ids: {:s}'.format(request.args.get('project_id')))
                  
        #
        # Add a predicate that limits by allocation_id:
        #
        if 'allocation_id' in request.args:
            try:
                target_allocation_ids = [int(v) for v in request.args.get('allocation_id').split(',')]
                if len(target_allocation_ids) > 0:
                    predicate_str = predicate_str + (' OR' if len(predicate_str) > 0 else '') + ' allocation_id IN (%s' + ',%s'*(len(target_allocation_ids)-1) + ')'
                    query_arguments.extend(target_allocation_ids)
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target allocation_ids: {:s}'.format(request.args.get('allocation_id')))
    
        #
        # If there are a limited number of valid gids for the requesting user, restrict based on those at the tail end
        # as an AND clause:
        #
        if len(valid_gids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND project_id IN (SELECT project_id FROM projects WHERE gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
            else:
                predicate_str = ' project_id IN (SELECT project_id FROM projects WHERE gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
        
        #
        # Generate the full query string:
        #
        query_str = query_str + (' WHERE' + predicate_str if len(predicate_str) > 0 else '')
        try:
            output_json = jsonify(AppDatabase.database_in_context().query(query_str, query_arguments))
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
        return output_json
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/alloc/<int:allocation_id>', methods=['GET'])
def handle_allocation_by_id(allocation_id):
    if request.method == 'GET':
        #
        # Validate access to the requested project:
        #
        try:
            lookup_result = AppDatabase.database_in_context().query('SELECT gid FROM projects WHERE project_id IN (SELECT project_id FROM allocations WHERE allocation_id = %s)', [allocation_id])
            if len(lookup_result) == 1:
                #
                # Is the user allowed to ask for this?
                #
                if not is_admin(request_globals.munge_credentials[1]) and not lookup_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                    return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            else:
                return error_response('ERROR:  no such allocation')
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        lookup_result = lookup_result[0]
        
        #
        # Determine what sort of "report" we want to generate:
        #
        report_type = request.args.get('report', default='aggregate')
        try:
            query_str = { 'aggregate': 'SELECT * FROM allocations_with_balances WHERE allocation_id = %s',
                          'detail': 'SELECT * FROM allocation_details WHERE allocation_id = %s'
                        }[report_type]
            return_type = { 'aggregate': 'single', 'detail': 'array' }[report_type]
        except:
            return error_response('ERROR:  Invalid report type: {:s}'.format(report_type))
        
        try:
            query_result = AppDatabase.database_in_context().query(query_str, [allocation_id])
            if len(query_result) > 0:
                if return_type == 'single':
                    query_result = query_result[0]
                return jsonify(query_result)
            return jsonify([])
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/alloc/<int:allocation_id>/credit', methods=['GET', 'POST'])
def handle_allocation_by_id_credit(allocation_id):
    if request.method == 'POST':
        #
        # Addition of an allocation credit requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
            if not isinstance(input_json, dict):
                amount = int(input_json)
                comments = None
            else:
                amount = int(input_json['amount'])
                comments = input_json.get('comments', None)
            
            if comments is not None:
                query_result = AppDatabase.database_in_context().query('INSERT INTO credits (allocation_id, amount, comments) VALUES (%s, %s, %s) RETURNING activity_id', [allocation_id, amount, comments], should_commit=True)
            else:
                query_result = AppDatabase.database_in_context().query('INSERT INTO credits (allocation_id, amount) VALUES (%s, %s) RETURNING activity_id', [allocation_id, amount], should_commit=True)
            if len(query_result) == 1:
                # The result will be the added account activity id:
                query_result = AppDatabase.database_in_context().query('SELECT * FROM credits WHERE activity_id = %s', [query_result[0]['activity_id']])
                return jsonify(query_result[0])
        except Exception as E:
            return error_response('ERROR:  Unable to add credit: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
    elif request.method == 'GET':
        try:
            report_type = request.args.get('report', default='aggregate')
            if report_type == 'aggregate':
                query_result = AppDatabase.database_in_context().query('SELECT A.credit, P.gid FROM allocations_with_balances AS A INNER JOIN projects AS P ON (P.project_id = A.project_id) WHERE A.allocation_id = %s', [allocation_id])
                if len(query_result) == 1:
                    #
                    # Is the user allowed to ask for this?
                    #
                    if is_admin(request_globals.munge_credentials[1]) or query_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                        return jsonify(query_result[0].pop('credit', 0))
                    return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            elif report_type == 'detail':
                query_result = AppDatabase.database_in_context().query('SELECT P.gid, D.* FROM allocation_details AS D INNER JOIN projects AS P ON (P.project_id = (SELECT project_id FROM allocations WHERE allocation_id = D.allocation_id)) WHERE D.allocation_id = %s AND D.activity_kind = %s', [allocation_id, 'credit'])
                if len(query_result) > 0:
                    if is_admin(request_globals.munge_credentials[1]) or query_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                        return jsonify(query_result);
                    return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            else:
                return error_response('ERROR:  invalid report type: {:s}'.format(report_type))
                
            return error_response('ERROR:  no such allocation')
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation credit lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/alloc/<int:allocation_id>/debit', methods=['GET', 'POST'])
def handle_allocation_by_id_debit(allocation_id):
    if request.method == 'POST':
        #
        # Addition of an allocation debit requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
            if not isinstance(input_json, dict):
                amount = int(input_json)
                comments = None
            else:
                amount = int(input_json['amount'])
                comments = input_json.get('comments', None)
            
            if comments is not None:
                query_result = AppDatabase.database_in_context().query('INSERT INTO debits (allocation_id, amount, comments) VALUES (%s, %s, %s) RETURNING activity_id', [allocation_id, amount, comments], should_commit=True)
            else:
                query_result = AppDatabase.database_in_context().query('INSERT INTO debits (allocation_id, amount) VALUES (%s, %s) RETURNING activity_id', [allocation_id, amount], should_commit=True)
            if len(query_result) == 1:
                # The result will be the added account activity id:
                query_result = AppDatabase.database_in_context().query('SELECT * FROM debits WHERE activity_id = %s', [query_result[0]['activity_id']])
                return jsonify(query_result[0])
        except Exception as E:
            return error_response('ERROR:  Unable to add debit: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
    elif request.method == 'GET':
        try:
            report_type = request.args.get('report', default='aggregate')
            if report_type == 'aggregate':
                query_result = AppDatabase.database_in_context().query('SELECT A.debit, P.gid FROM allocations_with_balances AS A INNER JOIN projects AS P ON (P.project_id = A.project_id) WHERE A.allocation_id = %s', [allocation_id])
                if len(query_result) == 1:
                    #
                    # Is the user allowed to ask for this?
                    #
                    if is_admin(request_globals.munge_credentials[1]) or query_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                        return jsonify(query_result[0].pop('debit', 0))
                    return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            elif report_type == 'detail':
                query_result = AppDatabase.database_in_context().query('SELECT P.gid, D.* FROM allocation_details AS D INNER JOIN projects AS P ON (P.project_id = (SELECT project_id FROM allocations WHERE allocation_id = D.allocation_id)) WHERE D.allocation_id = %s AND D.activity_kind = %s', [allocation_id, 'debit'])
                if len(query_result) > 0:
                    if is_admin(request_globals.munge_credentials[1]) or query_result[0]['gid'] in gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2]):
                        return jsonify(query_result);
                    return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            else:
                return error_response('ERROR:  invalid report type: {:s}'.format(report_type))
                
            return error_response('ERROR:  no such allocation')
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation debit lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/alloc/<int:allocation_id>/activity', methods=['GET'])
def handle_allocation_by_id_activity(allocation_id):
    if request.method == 'GET':
        #
        # This interface is for superusers only:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
        
        try:
            # Which tables do we want to include?
            activity_kinds = set(['credits', 'debits', 'predebits', 'failures'])
            value = request.args.get('activity_kind', default=None)
            if value is not None:
                activity_kinds = activity_kinds.intersection(value.split(','))
            if len(activity_kinds) == 0:
                return jsonify([])
            
            # Each table will possibly have a predicate to filter tuples:
            query_filter_str = ''
            query_filter_args = []
            
            # We allow filtering by dates, too:
            start_date = request.args.get('start_date', default=None)
            end_date = request.args.get('end_date', default=None)
            if start_date:
                if end_date:
                    query_filter_str = '(creation_date >= %s::TIMESTAMP WITH TIME ZONE AND creation_date <= %s::TIMESTAMP WITH TIME ZONE) OR (modification_date >= %s::TIMESTAMP WITH TIME ZONE AND modification_date <= %s::TIMESTAMP WITH TIME ZONE)'
                    query_filter_args = [start_date, start_date, end_date, end_date]
                else:
                    query_filter_str = 'creation_date >= %s::TIMESTAMP WITH TIME ZONE OR modification_date >= %s::TIMESTAMP WITH TIME ZONE'
                    query_filter_args = [start_date, start_date]
            elif end_date:
                query_filter_str = 'creation_date <= %s::TIMESTAMP WITH TIME ZONE OR modification_date <= %s::TIMESTAMP WITH TIME ZONE'
                query_filter_args = [end_date, end_date]
            
            # Every sub-query needs to restrict to the allocation id, too:
            if query_filter_str:
                query_filter_str = '(' + query_filter_str + ') AND allocation_id = %s'
            else:
                query_filter_str = 'allocation_id = %s'
            query_filter_args.append(allocation_id)
            
            # Now generate the query:
            query_str = ''
            query_args = []
            for activity_kind in activity_kinds:
                sub_query_str = 'SELECT row_to_json(tuple)::text AS json_record FROM (SELECT \'' + activity_kind + '\' AS activity_kind,* FROM ' + activity_kind
                if query_filter_str:
                    sub_query_str = sub_query_str + ' WHERE (' + query_filter_str + ') ) AS tuple'
                    query_args.extend(query_filter_args)
                    
                if query_str:
                    query_str = query_str + ' UNION (' + sub_query_str + ')'
                else:
                    query_str = sub_query_str
            
            # Attempt to perform the query and return the results:
            query_result = AppDatabase.database_in_context().query(query_str, query_args)
            return Response('[' + ','.join([q['json_record'] for q in query_result]) + ']', mimetype='application/json')
        except Exception as E:
            return error_response('ERROR:  Failed while performing allocation activity lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))



@app.route('/job', methods=['GET', 'POST'])
def handle_job():
    if request.method == 'POST':
        #
        # Creation of a job pre-debit requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
            
            #
            # Validate the incoming data structure:
            #
            if not isinstance(input_json, dict):
                return error_response('ERROR:  Invalid project data structure', err_code=hermes_errors.err_invalid_data)
            
            if not all(k in input_json for k in ('job_id', 'owner_uid', 'amount')):
                return error_response('ERROR:  Incomplete job data structure', err_code=hermes_errors.err_incomplete_data)
        except Exception as E:
            return error_response('ERROR:  Invalid project data structure: {:s}'.format(str(E)), err_code=hermes_errors.err_invalid_data)
            
        try:
            if 'allocation_id' in input_json:
                query_arguments = [int(input_json['allocation_id'])]
                query_str = 'SELECT submission_check_by_allocation(%s, %s, %s, %s) AS activity_id'
                failure_str = 'INSERT INTO failures (allocation_id, job_id, owner_uid, amount, comments) VALUES (%s, %s, %s, %s, %s) RETURNING activity_id'
            elif 'account' in input_json and 'resource' in input_json:
                # Resolve to an allocation id:
                alloc_id_result = AppDatabase.database_in_context().query('SELECT get_current_allocation_for_account(%s, %s) AS allocation_id', [input_json['account'], input_json['resource']])
                if len(alloc_id_result) > 0 and alloc_id_result[0]['allocation_id']:
                    query_arguments = [int(alloc_id_result[0]['allocation_id'])]
                    query_str = 'SELECT submission_check_by_allocation(%s, %s, %s, %s) AS activity_id'
                    failure_str = 'INSERT INTO failures (allocation_id, job_id, owner_uid, amount, comments) VALUES (%s, %s, %s, %s, %s) RETURNING activity_id'
                else:
                    return error_response('ERROR:  No allocation available', err_code=hermes_errors.err_no_allocation_avail)
            else:
                return error_response('ERROR:  Incomplete job data structure', err_code=hermes_errors.err_incomplete_data)
                
            query_arguments.extend((int(input_json['job_id']), parse_uid_number(input_json['owner_uid']), int(input_json['amount'])))
            
            query_result = AppDatabase.database_in_context().query(query_str, query_arguments, should_commit=True)
            if len(query_result) == 1:
                # The result will be the added account activity id:
                query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE activity_id = %s', [query_result[0]['activity_id']])
                return jsonify(query_result[0])
        
        except psycopg2.errors.RaiseException as E:
            output_lines = str(E).splitlines()
            output_str = output_lines.pop(0)
            database_str = str(output_str)
            for output_line in output_lines:
                if output_line.startswith('HINT:'):
                    output_str = output_str + ' (' + output_line + ')'
            # Log this to the database:
            query_arguments.append(database_str)
            AppDatabase.database_in_context().rollback()
            AppDatabase.database_in_context().query(failure_str, query_arguments, should_commit=True)
            return error_response('ERROR:  {:s}'.format(output_str), err_code=hermes_errors.err_database)
        except Exception as E:
            return error_response('ERROR:  Invalid job data structure: {:s}'.format(str(E)), err_code=hermes_errors.err_invalid_data)
        
    if request.method == 'GET':
        #
        # Build the list of predicates for the query:
        #
        query_str = 'SELECT * FROM predebits'
        predicate_str = ''
        query_arguments = []
    
        target_uids = set()
        target_gids = set()
        target_accounts = set()
        target_project_ids = set()
        target_allocation_ids = set()
        target_statuses = set()
        valid_gids = []
        if is_admin(request_globals.munge_credentials[1]):
            #
            # Users in the admin role are allowed to specify uids to search against.  We need
            # to generate the aggregate set of gids to which each uid is a member and select
            # projects owned by those gids as our partial predicate:
            #
            if 'owner_uid' in request.args:
                try:
                    target_uids.update([parse_uid_number(v) for v in request.args.get('owner_uid').split(',')])
                except Exception as E:
                    return error_response('ERROR:  Invalid owner_uid value: {:s}'.format(request.args.get('owner_uid')))
        else:
            #
            # Regular users restricted to ONLY those projects to which they are a member.  Generate
            # the set of gids to which the requesting uid is a member and we'll limit the results to
            # ONLY projects for those gids.  Also make sure we limit to just the user's uid, too:
            #
            valid_gids = gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2])
            target_uids.add(int(request_globals.munge_credentials[1]))
        
        if 'gid' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of gid numbers/names:
            #
            try:
                target_gids.update(filter_workgroup_gids([parse_gid_number(gid) for gid in request.args.get('gid').split(',')]))
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target gids: {:s}'.format(str(E)))

        if 'account' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of account names:
            #
            target_accounts.update(request.args.get('account').split(','))
        
        if 'project_id' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of project_ids:
            #
            try:
                target_project_ids.update([int(v) for v in request.args.get('project_id').split(',')])
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target project_ids: {:s}'.format(str(E)))
            
        if 'allocation_id' in request.args:
            #
            # Sub-query predicate that limits allocations to a set of allocation_ids:
            #
            try:
                target_allocation_ids.update([int(v) for v in request.args.get('allocation_id').split(',')])
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target allocation_ids: {:s}'.format(str(E)))
        
        if 'status' in request.args:
            #
            # Sub-query predicate that limits status of the tuple:
            #
            try:
                target_statuses.update(request.args.get('status').split(','))
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target statuses: {:s}'.format(str(E)))
                
        #
        # For predebits, the allocation/project table predicates require sub-queries.  The sub-queries
        # produce allocation_ids, which we'll UNION with any target allocation_ids provided for the sake
        # of simplicity in the predicates:
        #
        if len(target_gids) + len(target_accounts) + len(target_project_ids):
            predicate_str = ' allocation_id IN (SELECT A.allocation_id FROM allocations AS A INNER JOIN projects AS P ON (A.project_id = P.project_id) WHERE'
            if len(target_gids) > 0:
                predicate_str = predicate_str + ' P.gid IN (%s' + ',%s'*(len(target_gids)-1) + ')'
                query_arguments.extend(target_gids)
            if len(target_accounts) > 0:
                predicate_str = predicate_str + (' OR' if len(target_gids) > 0 else '') + ' P.account IN (' + '%s' + ',%s'*(len(target_accounts)-1) + ')'
                query_arguments.extend(target_accounts)
            if len(target_project_ids) > 0:
                predicate_str = predicate_str + (' OR' if len(target_gids) + len(target_accounts) > 0 else '') + ' A.project_id IN (' + '%s' + ',%s'*(len(target_project_ids)-1) + ')'
                query_arguments.extend(target_project_ids)
            predicate_str = predicate_str + ')'
            
            if len(target_allocation_ids) > 0:
                predicate_str = predicate_str + ' UNION (%s' + ',%s'*(len(target_allocation_ids)-1) + ')'
                query_arguments.extend(target_allocation_ids)
        
        elif len(target_allocation_ids) > 0:
            #
            # No other allocation_id-generating sub-queries, so just tack-on the allocation_id
            # predicate by itself:
            #
            predicate_str = predicate_str + ' allocation_id IN (%s' + ',%s'*(len(target_allocation_ids)-1) + ')'
            query_arguments.extend(target_allocation_ids)
    
        #
        # If there are a limited number of valid gids for the requesting user, restrict based on those at the tail end
        # as an AND clause:
        #
        if len(valid_gids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND allocation_id IN (SELECT a.allocation_id FROM allocations AS a INNER JOIN projects AS p ON (a.project_id = p.project_id) WHERE p.gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
            else:
                predicate_str = 'allocation_id IN (SELECT a.allocation_id FROM allocations AS a INNER JOIN projects AS p ON (a.project_id = p.project_id) WHERE p.gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
        
        #
        # If there are a limited number of uids to search, add that at the end as an AND clause:
        #
        if len(target_uids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND owner_uid IN (%s' + ',%s'*(len(target_uids)-1) + ')'
                query_arguments.extend(target_uids)
            else:
                predicate_str = 'owner_uid IN (%s' + ',%s'*(len(target_uids)-1) + ')'
                query_arguments.extend(target_uids)
        
        #
        # Filter by status?
        #
        if len(target_statuses) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND status IN (%s' + ',%s'*(len(target_statuses)-1) + ')'
                query_arguments.extend(target_statuses)
            else:
                predicate_str = 'status IN (%s' + ',%s'*(len(target_statuses)-1) + ')'
                query_arguments.extend(target_statuses)
        
        #
        # Generate the full query string:
        #
        query_str = query_str + (' WHERE ' + predicate_str if len(predicate_str) > 0 else '')
        try:
            output_json = jsonify(AppDatabase.database_in_context().query(query_str, query_arguments))
        except Exception as E:
            return error_response('ERROR:  Failed while performing job lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
        return output_json
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/job/<int:job_id>', methods=['GET', 'PUT'])
def handle_job_by_job_id(job_id):
    if request.method == 'PUT':
        #
        # Update of a job pre-debit requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            
        #
        # Get PUTed data:
        #
        try:
            input_json = request.get_json(force=True)
            
            #
            # The only fields we care about are the status and amount:
            #
            if not isinstance(input_json, dict):
                return error_response('ERROR:  Invalid job data structure', err_code=hermes_errors.err_invalid_data)
            
            if not all(k in input_json for k in ('status', 'amount')):
                return error_response('ERROR:  Incomplete job data structure', err_code=hermes_errors.err_incomplete_data)
            
            #
            # Parse the amount to an integer, ensure it's negative:
            #
            amount = int(input_json['amount'])
            if amount > 0:
                amount = -amount
            
        except Exception as E:
            return error_response('ERROR:  Invalid job data structure: {:s}'.format(str(E)), err_code=hermes_errors.err_invalid_data)
            
        try:
            #
            # Lookup the current state of this job id to ensure an appropriate
            # state change:
            #
            query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE job_id = %s', [job_id])
            if len(query_result) == 1:
                query_str = None
                if query_result[0]['status'] == input_json['status']:
                    if int(query_result[0]['amount']) != amount:
                        # No status change, just update the amount:
                        query_str = 'UPDATE predebits SET amount=%s, modification_date=now() WHERE job_id = %s'
                        query_arguments = [amount, job_id]
                elif int(query_result[0]['amount']) != amount:
                    # No status change, just update the amount:
                    query_str = 'UPDATE predebits SET status=%s, amount=%s, modification_date=now() WHERE job_id = %s'
                    query_arguments = [input_json['status'], amount, job_id]
                else:
                    # No status change, just update the amount:
                    query_str = 'UPDATE predebits SET status=%s, modification_date=now() WHERE job_id = %s'
                    query_arguments = [input_json['status'], job_id]
                
                if query_str is not None:
                    if AppDatabase.database_in_context().query_as_boolean(query_str, query_arguments, should_commit=True):
                        query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE job_id = %s', [job_id])
                return jsonify(query_result[0])
                
            return error_response('ERROR:  no such job', err_code=hermes_errors.err_unknown_job_id)
            
        except Exception as E:
            return error_response('ERROR:  Failed while updating job predebit: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
    if request.method == 'GET':
        #
        # Validate access to the requested job; first we have to lookup the
        # record:
        #
        try:
            query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE job_id = %s', [job_id])
            if len(query_result) == 1:
                #
                # Is the user allowed to ask for this?
                #
                if is_admin(request_globals.munge_credentials[1]) or query_result[0]['owner_uid'] == int(request_globals.munge_credentials[1]):
                    return jsonify(query_result[0])
                return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            return error_response('ERROR:  no such job', err_code=hermes_errors.err_unknown_job_id)
        except Exception as E:
            return error_response('ERROR:  Failed while performing job lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/job/activity-<int:activity_id>', methods=['GET', 'PUT'])
def handle_job_by_activity_id(activity_id):
    if request.method == 'PUT':
        #
        # Update of a job pre-debit requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            
        #
        # Get PUTed data:
        #
        try:
            input_json = request.get_json(force=True)
            
            #
            # The only fields we care about are the status and amount:
            #
            if not isinstance(input_json, dict):
                return error_response('ERROR:  Invalid job data structure', err_code=hermes_errors.err_invalid_data)
            
            if not all(k in input_json for k in ('status', 'amount')):
                return error_response('ERROR:  Incomplete job data structure', err_code=hermes_errors.err_incomplete_data)
            
            #
            # Parse the amount to an integer, ensure it's negative:
            #
            amount = int(input_json['amount'])
            if amount > 0:
                amount = -amount
            
        except Exception as E:
            return error_response('ERROR:  Invalid job data structure: {:s}'.format(str(E)), err_code=hermes_errors.err_invalid_data)
            
        try:
            #
            # Lookup the current state of this job id to ensure an appropriate
            # state change:
            #
            query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE activity_id = %s', [activity_id])
            if len(query_result) == 1:
                query_str = None
                if query_result[0]['status'] == input_json['status']:
                    if int(query_result[0]['amount']) != amount:
                        # No status change, just update the amount:
                        query_str = 'UPDATE predebits SET amount=%s, modification_date=now() WHERE activity_id = %s'
                        query_arguments = [amount, activity_id]
                elif int(query_result[0]['amount']) != amount:
                    # No status change, just update the amount:
                    query_str = 'UPDATE predebits SET status=%s, amount=%s, modification_date=now() WHERE activity_id = %s'
                    query_arguments = [input_json['status'], amount, activity_id]
                else:
                    # No status change, just update the amount:
                    query_str = 'UPDATE predebits SET status=%s, modification_date=now() WHERE activity_id = %s'
                    query_arguments = [input_json['status'], activity_id]
                
                if query_str is not None:
                    if AppDatabase.database_in_context().query_as_boolean(query_str, query_arguments, should_commit=True):
                        query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE activity_id = %s', [activity_id])
                return jsonify(query_result[0])
                
            return error_response('ERROR:  no such job', err_code=hermes_errors.err_unknown_job_id)
            
        except Exception as E:
            return error_response('ERROR:  Failed while updating job predebit: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
            
    if request.method == 'GET':
        #
        # Validate access to the requested job; first we have to lookup the
        # record:
        #
        try:
            query_result = AppDatabase.database_in_context().query('SELECT * FROM predebits WHERE activity_id = %s', [activity_id])
            if len(query_result) == 1:
                #
                # Is the user allowed to ask for this?
                #
                if is_admin(request_globals.munge_credentials[1]) or query_result[0]['owner_uid'] == int(request_globals.munge_credentials[1]):
                    return jsonify(query_result[0])
                return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
            return error_response('ERROR:  no such job', err_code=hermes_errors.err_unknown_job_id)
        except Exception as E:
            return error_response('ERROR:  Failed while performing job lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))
        
        
@app.route('/job/resolve', methods=['POST'])
def handle_job_resolve():
    if request.method == 'POST':
        #
        # Job resolve requires very specific credentials:
        #
        if not is_superuser(request_globals.munge_credentials[1]):
            return error_response('ERROR:  unauthorized access', status=401, err_code=hermes_errors.err_not_authorized)
        
        #
        # Get POSTed data:
        #
        try:
            input_json = request.get_json(force=True)
        
            #
            # Get the list of activity_id values:
            #
            if not isinstance(input_json, list):
                return error_response('ERROR:  JSON object POSTed is not a list')
            
            #
            # Ensure all values are ints:
            #
            input_json = [int(v) for v in input_json]
                
        except Exception as E:
            return error_response('ERROR:  Invalid activity_id array: {:s}'.format(str(E)), err_code=hermes_errors.err_invalid_data)
        
        #
        # Do the query:
        #
        output_list = []
        if len(input_json) > 0: 
            query_str = 'SELECT resolve_array_of_completed_jobs(\'{%s' + ',%s'*(len(input_json) - 1) + '}\'::BIGINT[])'
            try:
                query_result = AppDatabase.database_in_context().query(query_str, input_json, should_commit=True)
                #
                # The result set should be a list of activity_ids that were handled:
                #
                handled_ids = [int(item['resolve_array_of_completed_jobs']) for item in query_result]
                #
                # What wasn't handled?
                #
                output_list = list(set(input_json) - set(handled_ids))
            except Exception as E:
                return error_response('ERROR:  Failed in job-resolve query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        return jsonify(output_list)
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))


@app.route('/failure', methods=['GET'])
def handle_failure():
    if request.method == 'GET':
        #
        # Build the list of predicates for the query:
        #
        query_str = 'SELECT * FROM failures'
        predicate_str = ''
        query_arguments = []
    
        target_uids = set()
        target_gids = set()
        target_accounts = set()
        target_project_ids = set()
        target_allocation_ids = set()
        target_job_ids = set()
        valid_gids = []
        if is_admin(request_globals.munge_credentials[1]):
            #
            # Users in the admin role are allowed to specify uids to search against.  We need
            # to generate the aggregate set of gids to which each uid is a member and select
            # projects owned by those gids as our partial predicate:
            #
            if 'owner_uid' in request.args:
                try:
                    target_uids.update([parse_uid_number(v) for v in request.args.get('owner_uid').split(',')])
                except Exception as E:
                    return error_response('ERROR:  Invalid owner_uid value: {:s}'.format(request.args.get('owner_uid')))
        else:
            #
            # Regular users restricted to ONLY those projects to which they are a member.  Generate
            # the set of gids to which the requesting uid is a member and we'll limit the results to
            # ONLY projects for those gids.  Also make sure we limit to just the user's uid, too:
            #
            valid_gids = gids_for_uid(request_globals.munge_credentials[1], request_globals.munge_credentials[2])
            target_uids.add(int(request_globals.munge_credentials[1]))
        
        if 'gid' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of gid numbers/names:
            #
            try:
                target_gids.update(filter_workgroup_gids([parse_gid_number(gid) for gid in request.args.get('gid').split(',')]))
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target gids: {:s}'.format(str(E)))

        if 'account' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of account names:
            #
            target_accounts.update(request.args.get('account').split(','))
        
        if 'project_id' in request.args:
            #
            # Sub-query predicate that limits allocations for a set of project_ids:
            #
            try:
                target_project_ids.update([int(v) for v in request.args.get('project_id').split(',')])
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target project_ids: {:s}'.format(str(E)))
            
        if 'allocation_id' in request.args:
            #
            # Sub-query predicate that limits allocations to a set of allocation_ids:
            #
            try:
                target_allocation_ids.update([int(v) for v in request.args.get('allocation_id').split(',')])
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of target allocation_ids: {:s}'.format(str(E)))
        
        if 'job_id' in request.args:
            #
            # Sub-query predicate that limits status of the tuple:
            #
            try:
                target_job_ids.update([int(v) for v in request.args.get('job_id').split(',')])
            except Exception as E:
                return error_response('ERROR:  Unable to parse list of job ids: {:s}'.format(str(E)))
                
        #
        # For failures, the allocation/project table predicates require sub-queries.  The sub-queries
        # produce allocation_ids, which we'll UNION with any target allocation_ids provided for the sake
        # of simplicity in the predicates:
        #
        if len(target_gids) + len(target_accounts) + len(target_project_ids):
            predicate_str = ' allocation_id IN (SELECT A.allocation_id FROM allocations AS A INNER JOIN projects AS P ON (A.project_id = P.project_id) WHERE'
            if len(target_gids) > 0:
                predicate_str = predicate_str + ' P.gid IN (%s' + ',%s'*(len(target_gids)-1) + ')'
                query_arguments.extend(target_gids)
            if len(target_accounts) > 0:
                predicate_str = predicate_str + (' OR' if len(target_gids) > 0 else '') + ' P.account IN (' + '%s' + ',%s'*(len(target_accounts)-1) + ')'
                query_arguments.extend(target_accounts)
            if len(target_project_ids) > 0:
                predicate_str = predicate_str + (' OR' if len(target_gids) + len(target_accounts) > 0 else '') + ' A.project_id IN (' + '%s' + ',%s'*(len(target_project_ids)-1) + ')'
                query_arguments.extend(target_project_ids)
            predicate_str = predicate_str + ')'
            
            if len(target_allocation_ids) > 0:
                predicate_str = predicate_str + ' UNION (%s' + ',%s'*(len(target_allocation_ids)-1) + ')'
                query_arguments.extend(target_allocation_ids)
        
        elif len(target_allocation_ids) > 0:
            #
            # No other allocation_id-generating sub-queries, so just tack-on the allocation_id
            # predicate by itself:
            #
            predicate_str = predicate_str + ' allocation_id IN (%s' + ',%s'*(len(target_allocation_ids)-1) + ')'
            query_arguments.extend(target_allocation_ids)
    
        #
        # If there are a limited number of valid gids for the requesting user, restrict based on those at the tail end
        # as an AND clause:
        #
        if len(valid_gids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND allocation_id IN (SELECT a.allocation_id FROM allocations AS a INNER JOIN projects AS p ON (a.project_id = p.project_id) WHERE p.gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
            else:
                predicate_str = 'allocation_id IN (SELECT a.allocation_id FROM allocations AS a INNER JOIN projects AS p ON (a.project_id = p.project_id) WHERE p.gid IN (%s' + ',%s'*(len(valid_gids)-1) + '))'
                query_arguments.extend(valid_gids)
        
        #
        # If there are a limited number of uids to search, add that at the end as an AND clause:
        #
        if len(target_uids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND owner_uid IN (%s' + ',%s'*(len(target_uids)-1) + ')'
                query_arguments.extend(target_uids)
            else:
                predicate_str = ' owner_uid IN (%s' + ',%s'*(len(target_uids)-1) + ')'
                query_arguments.extend(target_uids)
        
        #
        # Filter by job id?
        #
        if len(target_job_ids) > 0:
            if len(predicate_str) > 0:
                predicate_str = ' (' + predicate_str + ') AND job_id IN (%s' + ',%s'*(len(target_job_ids)-1) + ')'
                query_arguments.extend(target_job_ids)
            else:
                predicate_str = ' job_id IN (%s' + ',%s'*(len(target_job_ids)-1) + ')'
                query_arguments.extend(target_job_ids)
        
        #
        # Generate the full query string:
        #
        query_str = query_str + (' WHERE' + predicate_str if len(predicate_str) > 0 else '')
        try:
            output_json = jsonify(AppDatabase.database_in_context().query(query_str, query_arguments))
        except Exception as E:
            return error_response('ERROR:  Failed while performing failure lookup query: {:s}'.format(str(E)), err_code=hermes_errors.err_database)
        
        return output_json
    
    else:
        return error_response('ERROR:  Invalid request method: {:s}'.format(request.method))



if __name__ == "__main__":
    app.run(debug=True)
